package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ConnectionType;

/**
 * Created by Roman on 03.08.2016.
 */
public interface RemoveConnectionMessenger
{
    void request(Component startComponent, Component endComponent, ConnectionType type);

    void addCallback(RemoveConnectionCallback callback);
    void removeCallback(RemoveConnectionCallback callback);
}

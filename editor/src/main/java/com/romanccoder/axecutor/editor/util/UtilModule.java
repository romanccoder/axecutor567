package com.romanccoder.axecutor.editor.util;

import com.google.inject.AbstractModule;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.core.util.validation.ValidationProvider;
import com.romanccoder.axecutor.editor.main.Editor;

/**
 * Created by Roman on 06.07.2016.
 */
public class UtilModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(TranslationProvider.class).annotatedWith(Editor.class).to(EditorTranslationProvider.class);
        bind(ValidationProvider.class).annotatedWith(Editor.class).to(EditorValidationProvider.class);
        bind(ResourceProvider.class).annotatedWith(Editor.class).to(EditorResourceProvider.class);
    }
}

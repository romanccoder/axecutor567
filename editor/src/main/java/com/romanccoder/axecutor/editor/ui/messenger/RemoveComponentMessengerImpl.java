package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by roman on 9/14/16.
 */
public class RemoveComponentMessengerImpl implements RemoveComponentMessenger
{
    private List<RemoveComponentCallback> callbackList;

    public RemoveComponentMessengerImpl()
    {
        callbackList = new CopyOnWriteArrayList<>();
    }

    @Override
    public void request(Component component)
    {
        for (RemoveComponentCallback callback : callbackList)
            callback.onRemoveComponent(component);
    }

    @Override
    public void addCallback(RemoveComponentCallback callback)
    {
        callbackList.add(callback);
    }

    @Override
    public void removeCallback(RemoveComponentCallback callback)
    {
        callbackList.remove(callback);
    }
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.editor.ui.model.Callback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 23.07.2016.
 */
public class NewConnectionMessengerImpl implements NewConnectionMessenger
{
    private List<NewConnectionCallback> callbackList;

    private Component startComponent;
    private ConnectionType type;
    private Component endComponent;

    public NewConnectionMessengerImpl()
    {
        callbackList = new ArrayList<>();
    }

    @Override
    public void start(Component startComponent, ConnectionType type)
    {
        clear();

        if (startComponent == null || type == null)
            throw new NullPointerException();

        this.startComponent = startComponent;
        this.type = type;

        for (NewConnectionCallback callback : callbackList)
            callback.onStartedNewConnection(startComponent, type);
    }

    @Override
    public boolean hasStarted()
    {
        return startComponent != null && type != null;
    }

    @Override
    public void end()
    {
        if (!hasStarted())
            throw new IllegalStateException("Connection has not been started");

        for (NewConnectionCallback callback : callbackList)
            callback.onNotEndedNewConnection(startComponent, type);

        clear();
    }

    @Override
    public void end(Component endComponent)
    {
        if (endComponent == null)
            throw new NullPointerException();

        if (!hasStarted())
            throw new IllegalStateException("Connection has not been started");

        this.endComponent = endComponent;

        for (NewConnectionCallback callback : callbackList)
            callback.onEndedNewConnection(startComponent, type, endComponent);

        clear();
    }

    @Override
    public void clear()
    {
        startComponent = null;
        type = null;
        endComponent = null;
    }

    @Override
    public void addCallback(NewConnectionCallback callback)
    {
        callbackList.add(callback);
    }

    @Override
    public void removeCallback(NewConnectionCallback callback)
    {
        callbackList.remove(callback);
    }
}

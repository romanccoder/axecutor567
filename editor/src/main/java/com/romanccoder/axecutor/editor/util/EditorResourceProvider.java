package com.romanccoder.axecutor.editor.util;

import com.romanccoder.axecutor.core.util.ResourceProvider;

/**
 * Created by Roman on 06.07.2016.
 */
public class EditorResourceProvider implements ResourceProvider
{
    @Override
    public String getBasePath()
    {
        return "/com/romanccoder/axecutor/editor";
    }
}

package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import javafx.scene.control.Alert;

/**
 * Created by Roman on 09.07.2016.
 */
public class ErrorControllerImpl implements ErrorController
{
    private Exception e;

    private Alert alert;

    @Inject
    @Editor
    private TranslationProvider translationProvider;

    public ErrorControllerImpl()
    {
        alert = new Alert(Alert.AlertType.ERROR);
    }

    @Override
    public void setException(Exception e)
    {
        this.e = e;

        alert.setTitle(translationProvider.get("com.romanccoder.axecutor.editor.ui.ErrorControllerImpl.title"));
        alert.setHeaderText(translationProvider.get("com.romanccoder.axecutor.editor.ui.ErrorControllerImpl.header_text"));
        alert.setContentText(translationProvider.get("com.romanccoder.axecutor.editor.ui.ErrorControllerImpl.context_text"));
    }

    @Override
    public void showDialog()
    {
        alert.showAndWait();
    }
}

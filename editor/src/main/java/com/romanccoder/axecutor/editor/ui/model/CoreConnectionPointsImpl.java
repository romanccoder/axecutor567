package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.ui.Point;

/**
 * Created by roman on 10/3/16.
 */
public class CoreConnectionPointsImpl extends CoreConnectionPoints
{
    private Point outputGreenTopPoint;
    private Point outputGreenLeftPoint;
    private Point outputGreenRightPoint;
    private Point outputGreenBottomPoint;

    public CoreConnectionPointsImpl()
    {
        outputGreenTopPoint = new Point(CoreComponentModel.OUTPUT_GREEN_TOP_POINT);
        outputGreenLeftPoint = new Point(CoreComponentModel.OUTPUT_GREEN_LEFT_POINT);
        outputGreenRightPoint = new Point(CoreComponentModel.OUTPUT_GREEN_RIGHT_POINT);
        outputGreenBottomPoint = new Point(CoreComponentModel.OUTPUT_GREEN_BOTTOM_POINT);
    }

    @Override
    public Point getPoint(ConnectionPointsLocation location)
    {
        switch (location)
        {
            case OUTPUT_GREEN_TOP:
                return outputGreenTopPoint;
            case OUTPUT_GREEN_RIGHT:
                return outputGreenRightPoint;
            case OUTPUT_GREEN_BOTTOM:
                return outputGreenBottomPoint;
            case OUTPUT_GREEN_LEFT:
                return outputGreenLeftPoint;
            default:
                return null;
        }
    }
}

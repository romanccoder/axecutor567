package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Created by Roman on 19.06.2016.
 */
public class MainControllerImpl extends AbstractController implements MainController
{
    @FXML
    private VBox rootBox;

    @FXML
    private TitledPane componentsTitledPane;
    @FXML
    private VBox projectsBox;
    @FXML
    private TitledPane propertiesTitledPane;

    @Inject
    private ControllerMediator mediator;

    private ComponentsListController componentsListController;

    private ProjectsController projectsController;

    private PropertiesController propertiesController;

    @Inject
    public MainControllerImpl(
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider,
            ComponentsListController componentsListController,
            ProjectsController projectsController,
            PropertiesController propertiesController
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        this.componentsListController = componentsListController;
        componentsTitledPane.setContent(componentsListController.getView());

        this.projectsController = projectsController;
        VBox.setVgrow(projectsController.getView(), Priority.ALWAYS);
        projectsBox.getChildren().add(projectsController.getView());

        this.propertiesController = propertiesController;
        propertiesTitledPane.setContent(propertiesController.getView());
    }

    @Override
    public Parent getView()
    {
        return rootBox;
    }

    @FXML
    public void handleCreateProject()
    {
        mediator.showCreateProjectDialog();
    }

    @FXML
    public void handleShowSettings()
    {
        mediator.showSettingsDialog();
    }
}

package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.messenger.ConnectionPointsMessenger;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.model.*;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Created by Roman on 13.08.2016.
 */
public class StandaloneComponentControllerImpl extends AbstractController implements StandaloneComponentController, ConnectionPointsMessenger.ConnectionPointsListener
{
    @FXML
    private AnchorPane rootPane;
    @FXML
    private Rectangle mainRectangle;
    @FXML
    private ImageView iconImageView;
    @FXML
    private Label descriptionLabel;

    private Component component;
    private ComponentsTree tree;
    private ComponentDescriptor descriptor;
    private ComponentsPosition componentsPosition;

    @Inject
    private NewConnectionMessenger newConnectionMessenger;

    @Inject
    private NewConnectionStartPointModel newConnectionStartPointModel;
    @Inject
    private GridPositionModel gridPositionModel;

    private StandaloneConnectionPoints connectionPoints;

    private InputConnectionControllerFactory inputControllerFactory;
    private GreenOutputControllerFactory greenControllerFactory;
    private RedOutputControllerFactory redControllerFactory;

    @Inject
    public StandaloneComponentControllerImpl(
            @Assisted Component component,
            @Assisted ComponentsTree tree,
            @Assisted ComponentsPosition componentsPosition,
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider,
            ConnectionPointsMessenger connectionPointsMessenger,
            StandaloneConnectionPointsFactory standaloneConnectionPointsFactory,
            InputConnectionControllerFactory inputControllerFactory,
            GreenOutputControllerFactory greenControllerFactory,
            RedOutputControllerFactory redControllerFactory
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        setComponent(component);
        setTree(tree);
        setPositions(componentsPosition);

        connectionPointsMessenger.addListener(this);
        this.connectionPoints = standaloneConnectionPointsFactory.create(component, tree, componentsPosition);

        this.inputControllerFactory = inputControllerFactory;
        this.greenControllerFactory = greenControllerFactory;
        this.redControllerFactory = redControllerFactory;

        initUI();
    }

    private void initUI()
    {
        // input rectangles
        InputConnectionController leftInputController = inputControllerFactory.create(component, tree);
        leftInputController.getShape().setLayoutX(StandaloneComponentModel.INPUT_LEFT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        leftInputController.getShape().setLayoutY(StandaloneComponentModel.INPUT_LEFT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);
        InputConnectionController rightInputController = inputControllerFactory.create(component, tree);
        rightInputController.getShape().setLayoutX(StandaloneComponentModel.INPUT_RIGHT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        rightInputController.getShape().setLayoutY(StandaloneComponentModel.INPUT_RIGHT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);

        //green output rectangles
        GreenOutputController leftGreenController = greenControllerFactory.create(component, tree, componentsPosition);
        leftGreenController.getShape().setLayoutX(StandaloneComponentModel.OUTPUT_GREEN_LEFT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        leftGreenController.getShape().setLayoutY(StandaloneComponentModel.OUTPUT_GREEN_LEFT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);
        GreenOutputController rightGreenController = greenControllerFactory.create(component, tree, componentsPosition);
        rightGreenController.getShape().setLayoutX(StandaloneComponentModel.OUTPUT_GREEN_RIGHT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        rightGreenController.getShape().setLayoutY(StandaloneComponentModel.OUTPUT_GREEN_RIGHT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);

        // red output rectangles
        RedOutputController leftRedController = redControllerFactory.create(component, tree, componentsPosition);
        leftRedController.getShape().setLayoutX(StandaloneComponentModel.OUTPUT_RED_LEFT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        leftRedController.getShape().setLayoutY(StandaloneComponentModel.OUTPUT_RED_LEFT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);
        RedOutputController rightRedController = redControllerFactory.create(component, tree, componentsPosition);
        rightRedController.getShape().setLayoutX(StandaloneComponentModel.OUTPUT_RED_RIGHT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        rightRedController.getShape().setLayoutY(StandaloneComponentModel.OUTPUT_RED_RIGHT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);

        // apply input handlers to all rectangles
        leftInputController.applyDragHandlers(mainRectangle);
        leftInputController.applyDragHandlers(leftGreenController.getShape());
        leftInputController.applyDragHandlers(leftRedController.getShape());
        leftInputController.applyDragHandlers(rightGreenController.getShape());
        leftInputController.applyDragHandlers(rightRedController.getShape());
        leftInputController.applyDragHandlers(iconImageView);

        leftInputController.applyMouseHandlers(rootPane);
        rightInputController.applyMouseHandlers(rootPane);
        leftGreenController.applyMouseHandlers(rootPane);
        rightGreenController.applyMouseHandlers(rootPane);
        leftRedController.applyMouseHandlers(rootPane);
        rightRedController.applyMouseHandlers(rootPane);

        rootPane.getChildren().addAll(leftInputController.getShape(), rightInputController.getShape(), leftGreenController.getShape(), rightGreenController.getShape(), leftRedController.getShape(), rightRedController.getShape());
    }

    private void setComponent(Component component)
    {
        this.component = component;
        mainRectangle.setFill(component.getColor());
        mainRectangle.setStroke(component.getColor().darker().darker());
        if (component.getIcon() != null)
            iconImageView.setImage(new Image(component.getIcon().toExternalForm()));
        descriptionLabel.textProperty().bind(component.getProperties().shortDescriptionProperty());
    }

    private void setTree(ComponentsTree tree)
    {
        this.tree = tree;
        this.descriptor = tree.getDescriptor(component);
    }

    private void setPositions(ComponentsPosition componentsPosition)
    {
        this.componentsPosition = componentsPosition;
    }

    @Override
    public Parent getView()
    {
        return rootPane;
    }

    @Override
    public Shape getShape()
    {
        return mainRectangle;
    }

    @Override
    public ConnectionPoints onPointsRequested(Component component)
    {
        if (component == this.component)
            return connectionPoints;
        else
            return null;
    }
}

package com.romanccoder.axecutor.editor.util;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.settings.SettingsService;
import com.romanccoder.axecutor.core.util.TranslationProvider;

import java.util.ResourceBundle;

/**
 * Created by Roman on 06.07.2016.
 */
public class EditorTranslationProvider implements TranslationProvider
{
    private ResourceBundle bundle;

    @Inject
    public EditorTranslationProvider(SettingsService service)
    {
        bundle = ResourceBundle.getBundle("com.romanccoder.axecutor.editor.bundles.Messages", service.getModel().getActiveLocale().getLocale());
    }

    @Override
    public ResourceBundle getBundle()
    {
        return bundle;
    }
}

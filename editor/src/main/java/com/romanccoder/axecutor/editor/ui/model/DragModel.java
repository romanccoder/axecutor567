package com.romanccoder.axecutor.editor.ui.model;

/**
 * Created by Roman on 05.08.2016.
 */
public interface DragModel
{
    void setPoint(double x, double y);

    void setX(double x);
    double getX();

    void setY(double y);
    double getY();
}

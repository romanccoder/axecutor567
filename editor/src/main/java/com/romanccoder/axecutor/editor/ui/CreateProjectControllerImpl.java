package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.Project;
import com.romanccoder.axecutor.core.project.ProjectService;
import com.romanccoder.axecutor.core.project.ProjectServiceFactory;
import com.romanccoder.axecutor.core.util.MaterialTheme;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.core.util.validation.ViewValidationUtil;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.model.ActiveProjectsModel;
import com.romanccoder.axecutor.editor.ui.model.CreateProjectModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Optional;

/**
 * Created by Roman on 07.07.2016.
 */
public class CreateProjectControllerImpl extends AbstractController implements CreateProjectController
{
    @FXML
    private VBox rootBox;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField fullPathTextField;
    @FXML
    private Button changeBasePathButton;

    private TranslationProvider translationProvider;

    private Dialog<ButtonType> dialog;

    private DirectoryChooser directoryChooser;

    private CreateProjectModel model;

    @Inject
    private Provider<ErrorController> errorControllerProvider;

    @Inject
    private Project project;
    @Inject
    private ComponentsTree componentsTree;
    @Inject
    private ComponentsPosition componentsPosition;
    @Inject
    private ProjectServiceFactory projectServiceFactory;
    @Inject
    private ActiveProjectsModel activeProjectsModel;

    @Inject
    public CreateProjectControllerImpl(
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider,
            CreateProjectModel model,
            ViewValidationUtil validationUtil
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        this.translationProvider = translationProvider;

        directoryChooser = new DirectoryChooser();

        this.model = model;
        model.nameProperty().bindBidirectional(nameTextField.textProperty());
        fullPathTextField.textProperty().bindBidirectional(model.fullPathProperty());
        validationUtil.setModel(model);
        validationUtil.setView(getView());
        validationUtil.enable();

        dialog = new Dialog<>();
        MaterialTheme.applyStyle(dialog);
        dialog.setTitle(translationProvider.get("com.romanccoder.axecutor.editor.ui.CreateProjectControllerImpl.title"));
        dialog.setGraphic(new ImageView(resourceProvider.getImage("create_project_64.png").toExternalForm()));
        dialog.setHeaderText(translationProvider.get("com.romanccoder.axecutor.editor.ui.CreateProjectControllerImpl.header_text"));
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(resourceProvider.getImage("create_project_64.png").toString()));
        dialog.getDialogPane().setContent(rootBox);
        dialog.getDialogPane().setPrefHeight(350);
        dialog.getDialogPane().setPrefWidth(500);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        dialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(validationUtil.hasErrorsProperty());
    }
    @Override
    public void showDialog()
    {
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == ButtonType.OK)
        {
            try
            {
                ProjectService projectService = projectServiceFactory.create(new File(model.getFullPath()));
                project.setName(model.getName());
                project.setTree(componentsTree);
                project.setPositions(componentsPosition);
                projectService.setProject(project);
                projectService.save();

                activeProjectsModel.addProject(project);
            }
            catch (Exception e)
            {
                ErrorController errorController = errorControllerProvider.get();
                errorController.setException(e);
                errorController.showDialog();
            }
        }
    }

    @Override
    public Parent getView()
    {
        return rootBox;
    }

    @FXML
    public void handleChangeBasePath(ActionEvent event)
    {
        if (new File(model.getBasePath()).exists())
            directoryChooser.setInitialDirectory(new File(model.getBasePath()));
        File directory = directoryChooser.showDialog(getView().getScene().getWindow());

        if (directory != null)
            model.setBasePath(directory.getAbsolutePath());
    }
}

package com.romanccoder.axecutor.editor.ui.model;

/**
 * Created by roman on 10/1/16.
 */
public interface ConnectionModel
{
    int SHAPE_WIDTH  = 6;
    int SHAPE_HEIGHT = 6;
}

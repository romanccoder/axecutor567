package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.ComponentType;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;

/**
 * Created by Roman on 10.08.2016.
 */
public interface GridPositionModel
{
    int GRID_SPACING = 25;
    int GRID_PADDING = 20;
    int CELL_SIZE    = 110;

    double getHCellGap(ComponentType type);
    double getVCellGap(ComponentType type);

    Point getPoint(GridPosition gridPosition);
    GridPosition getNearestPosition(Point point);
    boolean isPointInGridCell(Point point, GridPosition position);
}

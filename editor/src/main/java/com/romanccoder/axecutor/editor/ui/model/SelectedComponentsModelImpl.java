package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Roman on 18.07.2016.
 */
public class SelectedComponentsModelImpl implements SelectedComponentsModel
{
    private List<Component> selectedList;

    private List<SelectedComponentsListener> listenerList;

    public SelectedComponentsModelImpl()
    {
        selectedList = new ArrayList<>();
        listenerList = new ArrayList<>();
    }

    @Override
    public void setSelected(List<Component> componentsList)
    {
        clearSelected();

        selectedList.addAll(componentsList);

        for (SelectedComponentsListener listener : listenerList)
            listener.onSelect(selectedList);
    }

    @Override
    public void setSelected(Component component)
    {
        setSelected(new ArrayList<>(Collections.singletonList(component)));
    }

    @Override
    public void clearSelected()
    {
        if (!selectedList.isEmpty())
        {
            for (SelectedComponentsListener listener : listenerList)
                listener.onClearSelection(selectedList);

            selectedList.clear();
        }
    }

    @Override
    public List<Component> getSelected()
    {
        return selectedList;
    }

    @Override
    public void addListener(SelectedComponentsListener listener)
    {
        listenerList.add(listener);
    }

    @Override
    public void removeListener(SelectedComponentsListener listener)
    {
        listenerList.remove(listener);
    }
}

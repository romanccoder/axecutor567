package com.romanccoder.axecutor.editor.ui.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;

/**
 * Created by Roman on 18.08.2016.
 */
public class StandaloneConnectionPointsImpl extends StandaloneConnectionPoints
{
    private class TargetChangedListener implements ComponentDescriptor.DescriptorChangedListener
    {
        @Override
        public void onDescriptorChanged(ComponentDescriptor descriptor, ConnectionType type, Component previousComponent)
        {
            checkTarget();
        }
    }

    private class GridPositionChangedListener implements ComponentsPosition.GridPositionListener
    {
        @Override
        public void onPositionChanged(Component component, GridPosition position)
        {
            checkTarget();
        }
    }

    private Component component;
    private ComponentsTree tree;
    private ComponentsPosition componentsPosition;

    private Point inputTopPoint;
    private Point inputRightPoint;
    private Point inputBottomPoint;
    private Point inputLeftPoint;

    private Point outputGreenTopPoint;
    private Point outputGreenRightPoint;
    private Point outputGreenBottomPoint;
    private Point outputGreenLeftPoint;

    private Point outputRedRightPoint;
    private Point outputRedLeftPoint;

    @Inject
    public StandaloneConnectionPointsImpl(@Assisted Component component, @Assisted ComponentsTree tree, @Assisted ComponentsPosition componentsPosition)
    {
        this.component = component;
        this.tree = tree;
        this.componentsPosition = componentsPosition;

        inputTopPoint = new Point(StandaloneComponentModel.INPUT_TOP_POINT);
        inputRightPoint = new Point(StandaloneComponentModel.INPUT_RIGHT_POINT);
        inputBottomPoint = new Point(StandaloneComponentModel.INPUT_BOTTOM_POINT);
        inputLeftPoint = new Point(StandaloneComponentModel.INPUT_LEFT_POINT);

        outputGreenTopPoint = new Point(StandaloneComponentModel.OUTPUT_GREEN_TOP_POINT);
        outputGreenRightPoint = new Point(StandaloneComponentModel.OUTPUT_GREEN_RIGHT_POINT);
        outputGreenBottomPoint = new Point(StandaloneComponentModel.OUTPUT_GREEN_BOTTOM_POINT);
        outputGreenLeftPoint = new Point(StandaloneComponentModel.OUTPUT_GREEN_LEFT_POINT);

        outputRedRightPoint = new Point(StandaloneComponentModel.OUTPUT_RED_RIGHT_POINT);
        outputRedLeftPoint = new Point(StandaloneComponentModel.OUTPUT_RED_LEFT_POINT);

        init();
    }

    private void init()
    {
        TargetChangedListener targetChangedListener = new TargetChangedListener();
        GridPositionChangedListener gridPositionChangedListener = new GridPositionChangedListener();

        ComponentDescriptor.DescriptorChangedListener listener = (descriptor, type, previousTarget) ->
        {
            if (type == ConnectionType.GREEN)
            {
                if (previousTarget != null)
                {
                    tree.getDescriptor(previousTarget).removeListener(targetChangedListener); // remove listener from previous component
                    componentsPosition.removeListener(previousTarget, gridPositionChangedListener);
                    resetTopBottom();
                }

                if (descriptor.hasGreenTarget())
                {
                    tree.getDescriptor(tree.getDescriptor(component).getGreenTarget()).addListener(targetChangedListener); // add listener to new target
                    componentsPosition.addListener(tree.getDescriptor(component).getGreenTarget(), gridPositionChangedListener);
                    checkTarget();
                }
            }
        };
        tree.getDescriptor(component).addListener(listener);

        componentsPosition.addListener(component, (component1, position) -> checkTarget());
    }

    private void resetTopBottom()
    {
        setInputTopPoint(new Point(StandaloneComponentModel.INPUT_TOP_POINT));
        setInputBottomPoint(new Point(StandaloneComponentModel.INPUT_BOTTOM_POINT));
        setOutputGreenTopPoint(new Point(StandaloneComponentModel.OUTPUT_GREEN_TOP_POINT));
        setOutputGreenBottomPoint(new Point(StandaloneComponentModel.OUTPUT_GREEN_BOTTOM_POINT));
    }

    private void checkTarget()
    {
        ComponentDescriptor componentDescriptor = tree.getDescriptor(component);

        if (componentDescriptor.hasGreenTarget() && tree.getDescriptor(componentDescriptor.getGreenTarget()).getGreenTarget() == component)
        {
            GridPosition componentPosition = componentsPosition.getPosition(component);
            GridPosition targetPosition = componentsPosition.getPosition(componentDescriptor.getGreenTarget());

            if (componentPosition.getRow() == targetPosition.getRow() + 1) // target is on top
            {
                setOutputGreenTopPoint(new Point(StandaloneComponentModel.OUTPUT_GREEN_TOP_POINT.getX() - 10, StandaloneComponentModel.OUTPUT_GREEN_TOP_POINT.getY()));
                setInputTopPoint(new Point(StandaloneComponentModel.INPUT_TOP_POINT.getX() + 10, StandaloneComponentModel.INPUT_TOP_POINT.getY()));
            }
            else if (componentPosition.getRow() == targetPosition.getRow() - 1) // target is on bottom
            {
                setOutputGreenBottomPoint(new Point(StandaloneComponentModel.OUTPUT_GREEN_BOTTOM_POINT.getX() + 10, StandaloneComponentModel.OUTPUT_GREEN_BOTTOM_POINT.getY()));
                setInputBottomPoint(new Point(StandaloneComponentModel.INPUT_BOTTOM_POINT.getX() - 10, StandaloneComponentModel.INPUT_BOTTOM_POINT.getY()));
            }
        }
        else
        {
            resetTopBottom();
        }
    }

    private Point getInputTopPoint()
    {
        return inputTopPoint;
    }

    private void setInputTopPoint(Point inputTopPoint)
    {
        Point previousPoint = this.inputTopPoint;
        this.inputTopPoint = inputTopPoint;

        if (!previousPoint.equals(inputTopPoint))
        {
            setChanged();
            notifyObservers();
        }
    }

    private Point getInputRightPoint()
    {
        return inputRightPoint;
    }

    private void setInputRightPoint(Point inputRightPoint)
    {
        this.inputRightPoint = inputRightPoint;
        setChanged();
        notifyObservers();
    }

    private Point getInputBottomPoint()
    {
        return inputBottomPoint;
    }

    private void setInputBottomPoint(Point inputBottomPoint)
    {
        Point previousPoint = this.inputBottomPoint;
        this.inputBottomPoint = inputBottomPoint;

        if (!previousPoint.equals(inputBottomPoint))
        {
            setChanged();
            notifyObservers();
        }
    }

    private Point getInputLeftPoint()
    {
        return inputLeftPoint;
    }

    private void setInputLeftPoint(Point inputLeftPoint)
    {
        this.inputLeftPoint = inputLeftPoint;
        setChanged();
        notifyObservers();
    }

    private Point getOutputGreenTopPoint()
    {
        return outputGreenTopPoint;
    }

    private void setOutputGreenTopPoint(Point outputGreenTopPoint)
    {
        Point previousPoint = this.outputGreenTopPoint;
        this.outputGreenTopPoint = outputGreenTopPoint;

        if (!previousPoint.equals(outputGreenTopPoint))
        {
            setChanged();
            notifyObservers();
        }
    }

    private Point getOutputGreenRightPoint()
    {
        return outputGreenRightPoint;
    }

    private void setOutputGreenRightPoint(Point outputGreenRightPoint)
    {
        this.outputGreenRightPoint = outputGreenRightPoint;
        setChanged();
        notifyObservers();
    }

    private Point getOutputGreenBottomPoint()
    {
        return outputGreenBottomPoint;
    }

    private void setOutputGreenBottomPoint(Point outputGreenBottomPoint)
    {
        Point previousPoint = this.outputGreenBottomPoint;
        this.outputGreenBottomPoint = outputGreenBottomPoint;

        if (!previousPoint.equals(outputGreenBottomPoint))
        {
            setChanged();
            notifyObservers();
        }
    }

    private Point getOutputGreenLeftPoint()
    {
        return outputGreenLeftPoint;
    }

    private void setOutputGreenLeftPoint(Point outputGreenLeftPoint)
    {
        this.outputGreenLeftPoint = outputGreenLeftPoint;
        setChanged();
        notifyObservers();
    }

    private Point getOutputRedRightPoint()
    {
        return outputRedRightPoint;
    }

    private void setOutputRedRightPoint(Point outputRedRightPoint)
    {
        this.outputRedRightPoint = outputRedRightPoint;
        setChanged();
        notifyObservers();
    }

    private Point getOutputRedLeftPoint()
    {
        return outputRedLeftPoint;
    }

    private void setOutputRedLeftPoint(Point outputRedLeftPoint)
    {
        this.outputRedLeftPoint = outputRedLeftPoint;
        setChanged();
        notifyObservers();
    }

    @Override
    public Point getPoint(ConnectionPointsLocation location)
    {
        switch (location)
        {
            case INPUT_TOP:
                return getInputTopPoint();
            case INPUT_RIGHT:
                return getInputRightPoint();
            case INPUT_BOTTOM:
                return getInputBottomPoint();
            case INPUT_LEFT:
                return getInputLeftPoint();
            case OUTPUT_GREEN_TOP:
                return getOutputGreenTopPoint();
            case OUTPUT_GREEN_RIGHT:
                return getOutputGreenRightPoint();
            case OUTPUT_GREEN_BOTTOM:
                return getOutputGreenBottomPoint();
            case OUTPUT_GREEN_LEFT:
                return getOutputGreenLeftPoint();
            case OUTPUT_RED_RIGHT:
                return getOutputRedRightPoint();
            case OUTPUT_RED_LEFT:
                return getOutputRedLeftPoint();
            default:
                return null;
        }
    }
}

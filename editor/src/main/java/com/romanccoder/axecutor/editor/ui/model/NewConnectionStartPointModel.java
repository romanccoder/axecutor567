package com.romanccoder.axecutor.editor.ui.model;

/**
 * Created by Roman on 13.08.2016.
 */
public interface NewConnectionStartPointModel
{
    void setPoint(double x, double y);

    void setX(double x);
    double getX();

    void setY(double y);
    double getY();
}

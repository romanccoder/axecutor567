package com.romanccoder.axecutor.editor.util;

import com.romanccoder.axecutor.core.util.validation.ValidationProvider;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Created by Roman on 06.07.2016.
 */
public class EditorValidationProvider implements ValidationProvider
{
    private ValidatorFactory factory;

    public EditorValidationProvider()
    {
        factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(
                        new ResourceBundleMessageInterpolator(
                                new PlatformResourceBundleLocator("com.romanccoder.axecutor.editor.bundles.Messages")
                        )
                )
                .buildValidatorFactory();
    }

    @Override
    public Validator getValidator()
    {
        return factory.getValidator();
    }

    @Override
    public ValidatorFactory getValidatorFactory()
    {
        return factory;
    }
}

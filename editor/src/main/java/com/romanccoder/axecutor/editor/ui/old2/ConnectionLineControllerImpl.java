package com.romanccoder.axecutor.editor.ui.old2;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.editor.ui.messenger.ComponentPositionMessenger;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;

/**
 * Created by Roman on 04.08.2016.
 */
public class ConnectionLineControllerImpl implements ConnectionLineController
{
    private Path path;

    private boolean setManual;
    private MoveTo startMove;
    private LineTo lineTo;

    private Component startComponent;
    private Point[] startPoints;
    private ConnectionType type;

    private Component endComponent;
    private Point[] endPoints;

    @Inject
    private ComponentPositionMessenger componentPositionMessenger;

    public ConnectionLineControllerImpl()
    {
        path = new Path();
    }

    @Override
    public void setStart(Component component, Point[] points)
    {
        clear();
        setManual = false;

        startComponent = component;
        startPoints = points;
        GridPosition gridPosition = componentPositionMessenger.getPosition(component);

        gridPosition.addObserver((o, arg) -> calculateLine());
    }

    @Override
    public void setStart(double x, double y)
    {
        clear();
        setManual = true;
        startMove = new MoveTo(x, y);
        lineTo = new LineTo();
        path.getElements().addAll(startMove, lineTo);
    }

    @Override
    public void setType(ConnectionType type)
    {
        this.type = type;

        if (type == ConnectionType.GREEN)
            path.setFill(Color.GREEN);
        else if (type == ConnectionType.RED)
            path.setFill(Color.RED);
    }

    @Override
    public void setEnd(Component component, Point[] points)
    {
        endComponent = component;
        endPoints = points;
        GridPosition gridPosition = componentPositionMessenger.getPosition(component);

        calculateLine();
        gridPosition.addObserver((o, arg) -> calculateLine());
    }

    @Override
    public void setEnd(double x, double y)
    {
        if (setManual)
        {
            lineTo.setX(x);
            lineTo.setY(y);
        }
    }

    @Override
    public void clear()
    {
        path.getElements().clear();
    }

    private void calculateLine()
    {
        GridPosition startPosition = componentPositionMessenger.getPosition(startComponent);
        Point startComponentPoint = componentPositionMessenger.getPoint(startComponent);
        GridPosition endPosition = componentPositionMessenger.getPosition(endComponent);
        Point endComponentPoint = componentPositionMessenger.getPoint(endComponent);

        if (startPosition.getColumn() == endPosition.getColumn() && endPosition.getRow() == startPosition.getRow() + 1)
        {
            Point startPoint = new Point(startComponentPoint.getX() + startPoints[2].getX(), startComponentPoint.getY() + startPoints[2].getY());
            Point endPoint = new Point(endComponentPoint.getX() + endPoints[0].getX(), endComponentPoint.getY() + endPoints[0].getY());
            drawLine(startPoint, endPoint);
        }
        else if (startPosition.getColumn() == endPosition.getColumn() && endPosition.getRow() == startPosition.getRow() - 1)
        {
            Point startPoint = new Point(startComponentPoint.getX() + startPoints[0].getX(), startComponentPoint.getY() + startPoints[0].getY());
            Point endPoint = new Point(endComponentPoint.getX() + endPoints[2].getX(), endComponentPoint.getY() + endPoints[2].getY());
            drawLine(startPoint, endPoint);
        }
        else if (startPosition.getColumn() == endPosition.getColumn())
        {
            Point startPoint = new Point(startComponentPoint.getX() + startPoints[3].getX(), startComponentPoint.getY() + startPoints[3].getY());
            Point endPoint = new Point(endComponentPoint.getX() + endPoints[3].getX(), endComponentPoint.getY() + endPoints[3].getY());
            drawCurvedLine(startPoint, endPoint);
        }
    }

    private void drawLine(Point startPoint, Point endPoint)
    {
        clear();

        MoveTo startMove = new MoveTo(startPoint.getX(), startPoint.getY());
        path.getElements().add(startMove);

        LineTo mainLine = new LineTo();
        mainLine.setX(endPoint.getX());
        mainLine.setY(endPoint.getY());
        path.getElements().add(mainLine);

        double lineAngle = Math.atan2(startPoint.getY() - endPoint.getY(), startPoint.getX() - endPoint.getX());

        double arrowX1 = Math.cos(lineAngle + Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getX();
        double arrowY1 = Math.sin(lineAngle + Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getY();

        double arrowX2 = Math.cos(lineAngle - Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getX();
        double arrowY2 = Math.sin(lineAngle - Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getY();

        MoveTo leftArrowMove = new MoveTo(endPoint.getX(), endPoint.getY());
        LineTo leftArrowLine = new LineTo(arrowX1, arrowY1);
        MoveTo rightArrowMove = new MoveTo(endPoint.getX(), endPoint.getY());
        LineTo rightArrowLine = new LineTo(arrowX2, arrowY2);

        path.getElements().addAll(leftArrowMove, leftArrowLine, rightArrowMove, rightArrowLine);
    }

    private void drawCurvedLine(Point startPoint, Point endPoint)
    {
        clear();

        MoveTo startMove = new MoveTo(startPoint.getX(), startPoint.getY());
        path.getElements().add(startMove);

        CubicCurveTo cubicCurveTo = new CubicCurveTo();
        cubicCurveTo.setControlX1(startPoint.getX() - 70);
        cubicCurveTo.setControlY1(startPoint.getY());
        cubicCurveTo.setControlX2(endPoint.getX() - 70);
        cubicCurveTo.setControlY2(endPoint.getY());
        cubicCurveTo.setX(endPoint.getX());
        cubicCurveTo.setY(endPoint.getY());

        path.getElements().add(cubicCurveTo);

        Rotate rotation = new Rotate(ARROW_ANGLE);

        Point2D tan = new Point2D(
                cubicCurveTo.getControlX2() - cubicCurveTo.getX(),
                cubicCurveTo.getControlY2() - cubicCurveTo.getY()
        ).normalize().multiply(ARROW_LENGTH);

        MoveTo move = new MoveTo(cubicCurveTo.getX(), cubicCurveTo.getY());
        Point2D arrowPoint = rotation.transform(tan);

        LineTo arrow1Line = new LineTo(arrowPoint.getX(), arrowPoint.getY());
        arrow1Line.setAbsolute(false);
        rotation.setAngle(-ARROW_ANGLE);

        arrowPoint = rotation.transform(tan);
        LineTo arrow2Line = new LineTo(arrowPoint.getX(), arrowPoint.getY());
        arrow2Line.setAbsolute(false);

        path.getElements().addAll(move, arrow1Line, move, arrow2Line);
    }

    @Override
    public Path getPath()
    {
        return path;
    }

    @Override
    public Parent getView()
    {
        return null;
    }
}

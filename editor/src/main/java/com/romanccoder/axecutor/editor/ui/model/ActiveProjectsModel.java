package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.project.Project;
import javafx.collections.ListChangeListener;
import javafx.collections.SetChangeListener;

import java.util.Observable;

/**
 * Created by Roman on 17.07.2016.
 */
public interface ActiveProjectsModel
{
    void addProject(Project project);
    void removeProject(Project project);

    void addListener(SetChangeListener<Project> listener);
    void removeListener(SetChangeListener<Project> listener);
}

package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.GridPosition;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by roman on 9/7/16.
 */
public class CopyModelImpl implements CopyModel
{
    private List<CopyListener> listenerList;

    public CopyModelImpl()
    {
        listenerList = new CopyOnWriteArrayList<>();
    }

    @Override
    public void copy(ComponentsTree tree, GridPosition position, Component component)
    {
        for (CopyListener listener : listenerList)
            listener.onCopy(tree, position, component);
    }

    @Override
    public void addListener(CopyListener listener)
    {
        listenerList.add(listener);
    }

    @Override
    public void removeListener(CopyListener listener)
    {
        listenerList.remove(listener);
    }
}

package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;

import java.util.List;
import java.util.Observable;

/**
 * Created by Roman on 18.07.2016.
 */
public interface SelectedComponentsModel
{
    void setSelected(List<Component> componentsList);
    void setSelected(Component component);
    void clearSelected();
    List<Component> getSelected();

    void addListener(SelectedComponentsListener listener);
    void removeListener(SelectedComponentsListener listener);
}

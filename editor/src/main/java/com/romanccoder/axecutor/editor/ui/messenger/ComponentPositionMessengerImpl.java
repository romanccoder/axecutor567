package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 08.08.2016.
 */
public class ComponentPositionMessengerImpl implements ComponentPositionMessenger
{
    private List<ComponentPositionListener> listenerList;

    public ComponentPositionMessengerImpl()
    {
        listenerList = new ArrayList<>();
    }

    @Override
    public GridPosition getPosition(Component component)
    {
        for (ComponentPositionListener listener : listenerList)
        {
            GridPosition position = listener.getPosition(component);

            if (position != null)
                return position;
        }

        return null;
    }

    @Override
    public Point getPoint(Component component)
    {
        for (ComponentPositionListener listener : listenerList)
        {
            Point point = listener.getPoint(component);

            if (point != null)
                return point;
        }

        return null;
    }

    @Override
    public void addListener(ComponentPositionListener listener)
    {
        listenerList.add(listener);
    }

    @Override
    public void removeListener(ComponentPositionListener listener)
    {
        listenerList.remove(listener);
    }
}

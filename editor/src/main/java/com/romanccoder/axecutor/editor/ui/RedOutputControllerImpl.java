package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionCallback;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.messenger.RemoveConnectionCallback;
import com.romanccoder.axecutor.editor.ui.messenger.RemoveConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.model.ConnectionModel;
import com.romanccoder.axecutor.editor.ui.model.GridPositionModel;
import com.romanccoder.axecutor.editor.ui.model.NewConnectionStartPointModel;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Created by roman on 9/27/16.
 */
public class RedOutputControllerImpl implements RedOutputController, NewConnectionCallback, RemoveConnectionCallback
{
    private Rectangle rectangle;
    private EventHandler<? super MouseEvent> enteredHandler;
    private EventHandler<? super MouseEvent> exitedHandler;
    private EventHandler<? super DragEvent> dragExitedHandler;
    private EventHandler<? super MouseEvent> dragDetectedHandler;
    private EventHandler<? super DragEvent> dragDoneHandler;

    private EventHandler<? super MouseEvent> rootEnteredHandler;
    private EventHandler<? super MouseEvent> rootExitedHandler;

    private Component component;
    private ComponentDescriptor descriptor;
    private ComponentsTree tree;
    private ComponentsPosition componentsPosition;

    private ResourceProvider resourceProvider;
    private NewConnectionStartPointModel startPointModel;
    private NewConnectionMessenger newConnectionMessenger;
    private GridPositionModel gridPositionModel;

    @Inject
    public RedOutputControllerImpl(
            @Editor ResourceProvider resourceProvider,
            NewConnectionStartPointModel startPointModel,
            NewConnectionMessenger newConnectionMessenger,
            RemoveConnectionMessenger removeConnectionMessenger,
            GridPositionModel gridPositionModel,
            @Assisted Component component,
            @Assisted ComponentsTree tree,
            @Assisted ComponentsPosition componentsPosition
    )
    {
        rectangle = new Rectangle(ConnectionModel.SHAPE_WIDTH, ConnectionModel.SHAPE_HEIGHT);
        rectangle.setRotate(45);
        rectangle.setStroke(Color.RED);
        rectangle.setFill(Color.TRANSPARENT);

        this.resourceProvider = resourceProvider;
        this.startPointModel = startPointModel;
        this.newConnectionMessenger = newConnectionMessenger;
        this.gridPositionModel = gridPositionModel;

        setComponent(component);
        setTree(tree);
        setComponentsPosition(componentsPosition);

        newConnectionMessenger.addCallback(this);
        removeConnectionMessenger.addCallback(this);

        initUI();
    }

    private void initUI()
    {
        if (descriptor.hasRedTarget())
        {
            show();
            highlight();
        }
        else
        {
            hide();
            unHighlight();
        }

        enteredHandler = event ->
        {
            if (!descriptor.hasRedTarget())
                highlight();

            event.consume();
        };

        exitedHandler = event ->
        {
            if (!descriptor.hasRedTarget())
                unHighlight();

            event.consume();
        };

        dragExitedHandler = event ->
        {
            if (!descriptor.hasRedTarget())
                unHighlight();

            event.consume();
        };

        dragDetectedHandler = event ->
        {
            if (!descriptor.hasRedTarget())
            {
                Dragboard db = ((Rectangle)event.getSource()).startDragAndDrop(TransferMode.LINK);
                db.setDragView(new Image(resourceProvider.getImage("connection.png").toExternalForm()));
                ClipboardContent clipboardContent = new ClipboardContent();
                clipboardContent.putString("new connection");
                db.setContent(clipboardContent);
                Point startPoint = gridPositionModel.getPoint(componentsPosition.getPosition(component));

                startPointModel.setX(startPoint.getX() + gridPositionModel.getHCellGap(component.getType()) + ((Rectangle)event.getSource()).getWidth() / 2 + ((Rectangle)event.getSource()).getLayoutX());
                startPointModel.setY(startPoint.getY() + gridPositionModel.getVCellGap(component.getType()) + ((Rectangle)event.getSource()).getHeight() / 2 + ((Rectangle)event.getSource()).getLayoutY());

                newConnectionMessenger.start(component, ConnectionType.RED);

                event.consume();
            }
        };

        dragDoneHandler = event ->
        {
            if (!event.getDragboard().getContent(DataFormat.PLAIN_TEXT).equals(CONNECTION_ENDED))
            {
                newConnectionMessenger.end();
            }
            else
            {
                ((Rectangle) event.getSource()).getStyleClass().remove("red_hover");
            }

            event.consume();
        };

        rectangle.setOnMouseEntered(enteredHandler);
        rectangle.setOnMouseExited(exitedHandler);
        rectangle.setOnDragExited(dragExitedHandler);
        rectangle.setOnDragDetected(dragDetectedHandler);
        rectangle.setOnDragDone(dragDoneHandler);

        rootEnteredHandler = event -> show();
        rootExitedHandler = event ->
        {
            if (!descriptor.hasRedTarget())
                hide();
        };
    }

    @Override
    public Parent getView()
    {
        return null;
    }

    private void setComponent(Component component)
    {
        this.component = component;
    }

    private void setTree(ComponentsTree tree)
    {
        this.descriptor = tree.getDescriptor(component);
        this.tree = tree;
    }

    private void setComponentsPosition(ComponentsPosition componentsPosition)
    {
        this.componentsPosition = componentsPosition;
    }

    private void highlight()
    {
        rectangle.setFill(Color.RED);
    }

    private void unHighlight()
    {
        rectangle.setFill(Color.TRANSPARENT);
    }

    private void show()
    {
        rectangle.setVisible(true);
    }

    private void hide()
    {
        rectangle.setVisible(false);
    }

    @Override
    public void applyMouseHandlers(Node... nodes)
    {
        for (Node node : nodes)
        {
            node.addEventHandler(MouseEvent.MOUSE_ENTERED, rootEnteredHandler);
            node.addEventHandler(MouseEvent.MOUSE_EXITED, rootExitedHandler);
        }
    }

    @Override
    public void onStartedNewConnection(Component startComponent, ConnectionType type)
    {

    }

    @Override
    public void onNotEndedNewConnection(Component startComponent, ConnectionType type)
    {

    }

    @Override
    public void onEndedNewConnection(Component startComponent, ConnectionType type, Component endComponent)
    {
        if (startComponent == component && type == ConnectionType.RED)
        {
            show();
            highlight();
        }
    }

    @Override
    public void onRemoveConnection(Component startComponent, Component endComponent, ConnectionType type)
    {
        if (this.component == startComponent && type == ConnectionType.RED)
        {
            hide();
            unHighlight();
        }
    }

    @Override
    public Shape getShape()
    {
        return rectangle;
    }
}

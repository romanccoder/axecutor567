package com.romanccoder.axecutor.editor.ui.model;

/**
 * Created by Roman on 13.08.2016.
 */
public class NewConnectionStartPointModelImpl implements NewConnectionStartPointModel
{
    private double x;
    private double y;

    @Override
    public void setPoint(double x, double y)
    {
        setX(x);
        setY(y);
    }

    @Override
    public void setX(double x)
    {
        this.x = x;
    }

    @Override
    public double getX()
    {
        return x;
    }

    @Override
    public void setY(double y)
    {
        this.y = y;
    }

    @Override
    public double getY()
    {
        return y;
    }
}

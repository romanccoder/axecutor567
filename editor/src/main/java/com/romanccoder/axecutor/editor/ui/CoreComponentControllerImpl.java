package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.messenger.ConnectionPointsMessenger;
import com.romanccoder.axecutor.editor.ui.model.*;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

/**
 * Created by roman on 9/27/16.
 */
public class CoreComponentControllerImpl extends AbstractController implements CoreComponentController, ConnectionPointsMessenger.ConnectionPointsListener
{
    @FXML
    private AnchorPane rootPane;
    @FXML
    private Circle mainCircle;
    @FXML
    private ImageView iconImageView;
    @FXML
    private Label descriptionLabel;

    private Component component;
    private ComponentsTree tree;
    private ComponentDescriptor descriptor;
    private ComponentsPosition componentsPosition;

    @Inject
    private CoreConnectionPoints connectionPoints;

    private GreenOutputControllerFactory greenControllerFactory;

    @Inject
    public CoreComponentControllerImpl(
            @Assisted Component component,
            @Assisted ComponentsTree tree,
            @Assisted ComponentsPosition componentsPosition,
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider,
            ConnectionPointsMessenger connectionPointsMessenger,
            StandaloneConnectionPointsFactory standaloneConnectionPointsFactory,
            InputConnectionControllerFactory inputControllerFactory,
            GreenOutputControllerFactory greenControllerFactory
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        setComponent(component);
        setTree(tree);
        setPositions(componentsPosition);

        this.greenControllerFactory = greenControllerFactory;

        connectionPointsMessenger.addListener(this);

        initUI();
    }

    private void initUI()
    {
        GreenOutputController leftGreenController = greenControllerFactory.create(component, tree, componentsPosition);
        leftGreenController.getShape().setLayoutX(CoreComponentModel.OUTPUT_GREEN_LEFT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        leftGreenController.getShape().setLayoutY(CoreComponentModel.OUTPUT_GREEN_LEFT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);
        GreenOutputController rightGreenController = greenControllerFactory.create(component, tree, componentsPosition);
        rightGreenController.getShape().setLayoutX(CoreComponentModel.OUTPUT_GREEN_RIGHT_POINT.getX() - ConnectionModel.SHAPE_WIDTH / 2);
        rightGreenController.getShape().setLayoutY(CoreComponentModel.OUTPUT_GREEN_RIGHT_POINT.getY() - ConnectionModel.SHAPE_HEIGHT / 2);

        leftGreenController.applyMouseHandlers(rootPane);
        rightGreenController.applyMouseHandlers(rootPane);

        rootPane.getChildren().addAll(leftGreenController.getShape(), rightGreenController.getShape());
    }

    private void setComponent(Component component)
    {
        this.component = component;
        mainCircle.setFill(component.getColor());
        mainCircle.setStroke(component.getColor().darker().darker());
        if (component.getIcon() != null)
            iconImageView.setImage(new Image(component.getIcon().toExternalForm()));
        descriptionLabel.textProperty().bind(component.getProperties().shortDescriptionProperty());
    }

    private void setTree(ComponentsTree tree)
    {
        this.tree = tree;
        this.descriptor = tree.getDescriptor(component);
    }

    private void setPositions(ComponentsPosition componentsPosition)
    {
        this.componentsPosition = componentsPosition;
    }

    @Override
    public Parent getView()
    {
        return rootPane;
    }

    @Override
    public Shape getShape()
    {
        return mainCircle;
    }

    @Override
    public ConnectionPoints onPointsRequested(Component component)
    {
        if (component == this.component)
            return connectionPoints;
        else
            return null;
    }
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.editor.ui.model.ConnectionPoints;

/**
 * Created by Roman on 13.08.2016.
 */
public interface ConnectionPointsMessenger
{
    interface ConnectionPointsListener
    {
        ConnectionPoints onPointsRequested(Component component);
    }

    ConnectionPoints request(Component component);

    void addListener(ConnectionPointsListener listener);
    void removeListener(ConnectionPointsListener listener);
}

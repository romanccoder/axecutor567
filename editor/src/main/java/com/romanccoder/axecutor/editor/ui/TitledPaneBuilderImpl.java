package com.romanccoder.axecutor.editor.ui;

import javafx.scene.Node;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;

import java.net.URL;

/**
 * Created by Roman on 07.07.2016.
 */
public class TitledPaneBuilderImpl implements TitledPaneBuilder
{
    private TitledPane pane;

    private String label;
    private URL icon;
    private boolean collapsible;
    private boolean expanded;
    private Node content;
    private Object associatedObject;

    public TitledPaneBuilderImpl()
    {
        pane = new TitledPane();
        label = pane.getText();
        collapsible = pane.isCollapsible();
        expanded = pane.isExpanded();
    }

    @Override
    public void setLabel(String label)
    {
        this.label = label;
        pane.setText(label);
    }

    @Override
    public String getLabel()
    {
        return label;
    }

    @Override
    public void setIcon(URL icon)
    {
        this.icon = icon;
        pane.setGraphic(new ImageView(icon.toExternalForm()));
    }

    @Override
    public URL getIcon()
    {
        return icon;
    }

    @Override
    public void setCollapsible(boolean collapsible)
    {
        this.collapsible = collapsible;
        pane.setCollapsible(collapsible);
    }

    @Override
    public boolean isCollapsible()
    {
        return collapsible;
    }

    @Override
    public void setExpanded(boolean expanded)
    {
        this.expanded = expanded;
        pane.setExpanded(expanded);
    }

    @Override
    public boolean isExpanded()
    {
        return expanded;
    }

    @Override
    public void setContent(Node content)
    {
        this.content = content;
        pane.setContent(content);
    }

    @Override
    public Node getContent()
    {
        return content;
    }

    @Override
    public void setAssociatedObject(Object associatedObject)
    {
        this.associatedObject = associatedObject;
        pane.setUserData(associatedObject);
    }

    @Override
    public Object getAssociatedObject()
    {
        return associatedObject;
    }

    @Override
    public TitledPane getPane()
    {
        return pane;
    }
}

package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.editor.ui.ConnectionController;

/**
 * Created by Roman on 18.08.2016.
 */
public interface StandaloneComponentModel
{
    int HEIGHT = 100;
    int WIDTH  = 100;

    int SHAPE_SIZE = 60;

    Point INPUT_TOP_POINT    = new Point(WIDTH / 2, 0);
    Point INPUT_RIGHT_POINT  = new Point(WIDTH - 20, 12);
    Point INPUT_BOTTOM_POINT = new Point(WIDTH / 2, HEIGHT);
    Point INPUT_LEFT_POINT   = new Point(20, 12);

    Point OUTPUT_GREEN_TOP_POINT    = new Point(WIDTH / 2, 0);
    Point OUTPUT_GREEN_RIGHT_POINT  = new Point(WIDTH - 20, SHAPE_SIZE / 2);
    Point OUTPUT_GREEN_BOTTOM_POINT = new Point(WIDTH / 2, HEIGHT);
    Point OUTPUT_GREEN_LEFT_POINT   = new Point(20, SHAPE_SIZE / 2);

    Point OUTPUT_RED_RIGHT_POINT = new Point(WIDTH - 20, 48);
    Point OUTPUT_RED_LEFT_POINT  = new Point(20, 48);
}

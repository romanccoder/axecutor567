package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by Roman on 06.07.2016.
 */
public class ControllerMediatorImpl implements ControllerMediator
{
    @Inject
    private Provider<CreateProjectController> createProjectControllerProvider;

    @Inject
    private Provider<SettingsController> settingsControllerProvider;

    @Override
    public void showCreateProjectDialog()
    {
        createProjectControllerProvider.get().showDialog();
    }

    @Override
    public void showSettingsDialog()
    {
        settingsControllerProvider.get().showDialog();
    }
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by roman on 9/14/16.
 */
public interface RemoveComponentCallback
{
    void onRemoveComponent(Component component);
}

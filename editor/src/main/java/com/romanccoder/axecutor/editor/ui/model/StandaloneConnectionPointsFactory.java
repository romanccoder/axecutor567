package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by Roman on 18.08.2016.
 */
public interface StandaloneConnectionPointsFactory
{
    StandaloneConnectionPoints create(Component component, ComponentsTree tree, ComponentsPosition componentsPosition);
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.editor.ui.model.Callback;

/**
 * Created by Roman on 23.07.2016.
 */
public interface NewConnectionMessenger
{
    void start(Component startComponent, ConnectionType type);
    boolean hasStarted();
    void end();
    void end(Component endComponent);

    void clear();

    void addCallback(NewConnectionCallback callback);
    void removeCallback(NewConnectionCallback callback);
}

package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.ui.Point;

/**
 * Created by Roman on 18.08.2016.
 */
public interface CoreComponentModel
{
    int HEIGHT = 100;
    int WIDTH = 100;

    int SHAPE_SIZE = 60;

    Point OUTPUT_GREEN_TOP_POINT    = new Point(WIDTH / 2, 0);
    Point OUTPUT_GREEN_RIGHT_POINT  = new Point(WIDTH - 20, SHAPE_SIZE / 2);
    Point OUTPUT_GREEN_BOTTOM_POINT = new Point(WIDTH / 2, HEIGHT);
    Point OUTPUT_GREEN_LEFT_POINT   = new Point(20, SHAPE_SIZE / 2);
}

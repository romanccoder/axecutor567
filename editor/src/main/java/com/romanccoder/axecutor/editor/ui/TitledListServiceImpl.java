package com.romanccoder.axecutor.editor.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Created by Roman on 07.07.2016.
 */
public class TitledListServiceImpl extends TitledListService
{
    private List<TitledPaneBuilder> items;

    public TitledListServiceImpl()
    {
        items = new ArrayList<>();
    }

    @Override
    public void add(TitledPaneBuilder model)
    {
        items.add(model);
        setChanged();
        notifyObservers();
    }

    @Override
    public boolean remove(TitledPaneBuilder model)
    {
        if (items.remove(model))
        {
            setChanged();
            notifyObservers();
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public TitledPaneBuilder getByAssociated(Object associatedObject)
    {
        Optional<TitledPaneBuilder> modelOptional = items.stream().filter(item -> item.getAssociatedObject().equals(associatedObject)).findFirst();

        if (!modelOptional.isPresent())
            throw new NoSuchElementException("No such model with associated object: " + associatedObject);

        return modelOptional.get();
    }

    @Override
    public List<TitledPaneBuilder> getAll()
    {
        return items;
    }
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;

import java.util.function.Function;

/**
 * Created by Roman on 08.08.2016.
 */
public interface ComponentPositionMessenger
{
    interface ComponentPositionListener
    {
        GridPosition getPosition(Component component);
        Point getPoint(Component component);
    }

    GridPosition getPosition(Component component);
    Point getPoint(Component component);

    void addListener(ComponentPositionListener listener);
    void removeListener(ComponentPositionListener listener);
}

package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by Roman on 06.07.2016.
 */
public interface ControllerMediator
{
    void showCreateProjectDialog();
    void showSettingsDialog();
}

package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.*;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.messenger.RemoveComponentCallback;
import com.romanccoder.axecutor.editor.ui.messenger.RemoveComponentMessenger;
import com.romanccoder.axecutor.editor.ui.model.*;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.util.List;

/**
 * Created by Roman on 10.08.2016.
 */
public class ComponentWrapperControllerImpl implements ComponentWrapperController, MoveManager.MoveCallback, CopyModel.CopyListener, RemoveComponentCallback, SelectedComponentsListener
{
    private BorderPane pane;

    private ComponentsTree tree;
    private ComponentsPosition componentsPosition;
    
    private Component component;
    private ComponentController componentController;
    private GridPosition gridPosition;

    @Inject
    private StandaloneComponentControllerFactory standaloneControllerFactory;
    @Inject
    private CoreComponentControllerFactory coreControllerFactory;

    private MoveManager moveManager;
    private CopyModel copyModel;
    private RemoveComponentMessenger removeComponentMessenger;

    @Inject
    private Provider<ErrorController> errorControllerProvider;

    @Inject
    private SelectedComponentsModel selectedComponentsModel;

    @Inject
    public ComponentWrapperControllerImpl(
            @Editor ResourceProvider resourceProvider,
            MoveManager moveManager,
            @Assisted ComponentsTree tree,
            @Assisted ComponentsPosition componentsPosition,
            @Assisted GridPosition gridPosition,
            CopyModel copyModel,
            RemoveComponentMessenger removeComponentMessenger,
            SelectedComponentsModel selectedComponentsModel
    )
    {
        pane = new BorderPane();
        pane.getStylesheets().add(resourceProvider.getCSS("components_wrapper.css").toExternalForm());

        this.moveManager = moveManager;
        moveManager.addCallback(this);

        this.tree = tree;
        this.componentsPosition = componentsPosition;
        this.gridPosition = gridPosition;

        this.copyModel = copyModel;
        copyModel.addListener(this);

        this.removeComponentMessenger = removeComponentMessenger;
        removeComponentMessenger.addCallback(this);

        selectedComponentsModel.addListener(this);

        initUI();
    }

    @Override
    public GridPosition getPosition()
    {
        return gridPosition;
    }

    private void initUI()
    {
        // Context menu
        ContextMenu contextMenu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Remove component");
        removeMenuItem.setOnAction(event -> removeComponentMessenger.request(component));
        contextMenu.getItems().add(removeMenuItem);

        // Select or show context menu
        pane.setOnMouseClicked(event ->
        {
            if (component != null && event.isStillSincePress())
            {
                if (event.getButton() == MouseButton.PRIMARY)
                {
                    selectedComponentsModel.setSelected(component);
                }
                if (event.getButton() == MouseButton.SECONDARY)
                {
                    if (selectedComponentsModel.getSelected().size() < 2)
                        selectedComponentsModel.setSelected(component);

                    contextMenu.show(pane, event.getScreenX(), event.getScreenY());
                }

                event.consume();
            }
        });


        pane.setOnDragDetected(event -> // move component
        {
            if (component != null)
            {
                Dragboard db = pane.startDragAndDrop(TransferMode.MOVE);
                db.setDragView(componentController.getView().snapshot(null, null));
                ClipboardContent clipboardContent = new ClipboardContent();
                clipboardContent.putString("move component");
                db.setContent(clipboardContent);
                moveManager.start(tree, component);
                hideComponent();

                event.consume();
            }
        });
        //pane.setOnDragEntered(event -> pane.getStyleClass().add("wrapper_highlight"));
        //pane.setOnDragExited(event -> pane.getStyleClass().remove("wrapper_highlight"));


        pane.setOnDragDone(event ->
        {
            if (!event.getDragboard().getContent(DataFormat.PLAIN_TEXT).equals("dropped"))
            {
                if (moveManager.getCurrent().getStartComponent() == component)
                    moveManager.end();

                //showComponent();
            }
        });
    }

    @Override
    public void setComponent(Component component)
    {
        unselect();

        if (component != null)
        {
            if (component.getType() == ComponentType.STANDALONE)
            {
                componentController = standaloneControllerFactory.create(component, tree, componentsPosition);
            }
            else if (component.getType() == ComponentType.CORE)
            {
                componentController = coreControllerFactory.create(component, tree, componentsPosition);
            }

            this.component = component;

            pane.getChildren().clear();
            pane.setCenter(componentController.getView());
        }
        else
        {
            pane.getChildren().clear();
            this.component = null;
            this.componentController = null;
        }
    }

    @Override
    public Component getComponent()
    {
        return component;
    }

    @Override
    public Parent getView()
    {
        return pane;
    }

    private void showComponent()
    {
        componentController.getView().setVisible(true);
    }

    private void hideComponent()
    {
        componentController.getView().setVisible(false);
    }

    private void select()
    {
        if (component != null)
            componentController.getShape().getStyleClass().add("selected");
    }

    private void unselect()
    {
        if (component != null)
            componentController.getShape().getStyleClass().remove("selected");
    }

    @Override
    public void onMoveStarted(MoveModel model)
    {

    }

    @Override
    public void onMove(MoveModel model)
    {
        if (model.getTree() == tree)
        {
            if (model.getStartComponent() == component)
            {
                setComponent(null);
            }
            else if (model.getEndPosition().equals(gridPosition))
            {
                if (component != null)
                {
                    moveComponentBottom();
                }

                componentsPosition.set(model.getStartComponent(), gridPosition);
                setComponent(model.getStartComponent());
            }
        }
    }

    @Override
    public void onMoveNotEnded(MoveModel model)
    {
        if (model.getStartComponent() == component)
        {
            showComponent();
        }
    }

    @Override
    public void onCopy(ComponentsTree tree, GridPosition position, Component component)
    {
        try
        {
            if (this.tree == tree && this.gridPosition.equals(position))
            {
                tree.addComponent(component);
                componentsPosition.set(component, gridPosition);
                if (this.component != null)
                {
                    moveComponentBottom();
                }
                setComponent(component);
            }
        }
        catch (CoreComponentExistsException e)
        {
            ErrorController errorController = errorControllerProvider.get();
            errorController.setException(e);
            errorController.showDialog();
        }
    }

    private void moveComponentBottom()
    {
        if (component != null)
        {
            moveManager.start(tree, component);
            moveManager.moveTo(new GridPosition(gridPosition.getRow() + 1, gridPosition.getColumn()));
        }
    }

    @Override
    public void onRemoveComponent(Component component)
    {
        if (this.component == component)
            setComponent(null);
    }

    @Override
    public void onClearSelection(List<Component> componentsList)
    {
        if (component != null && componentsList.contains(component))
            unselect();
    }

    @Override
    public void onSelect(List<Component> componentsList)
    {
        if (component != null && componentsList.contains(component))
            select();
    }
}

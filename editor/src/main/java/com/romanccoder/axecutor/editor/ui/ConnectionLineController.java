package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.ui.Point;
import javafx.scene.shape.Path;

/**
 * Created by Roman on 17.08.2016.
 */
public interface ConnectionLineController extends Controller
{
    double ARROW_ANGLE           = 30;
    double ARROW_LENGTH          = 10;
    double SEGMENT_LENGTH        = 4;
    double SEGMENT_GAP           = 2.65;
    double STROKE_WIDTH          = 1.8;
    double STROKE_WIDTH_INCREASE = 2;

    void setStart(double x, double y);
    Point getStartPoint();
    void setEnd(double x, double y);
    void setType(ConnectionType type);
    ConnectionType getType();

    void setComponents(Component startComponent, Component endComponent);
    void clearConnection();
    Component getStartComponent();
    Component getEndComponent();

    Path[] getPaths();

    void clear();
}

package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by Roman on 20.08.2016.
 */
public interface StandaloneComponentControllerFactory
{
    StandaloneComponentController create(Component component, ComponentsTree tree, ComponentsPosition componentsPosition);
}

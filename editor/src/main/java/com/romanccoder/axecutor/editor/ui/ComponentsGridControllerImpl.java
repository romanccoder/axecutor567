package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.*;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.messenger.*;
import com.romanccoder.axecutor.editor.ui.model.*;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Roman on 10.08.2016.
 */
public class ComponentsGridControllerImpl implements ComponentsGridController, MoveManager.MoveCallback, NewConnectionCallback, CopyModel.CopyListener, RemoveConnectionCallback, RemoveComponentCallback
{
    private ScrollPane scrollPane;
    private AnchorPane anchorPane;
    private GridPane gridPane;

    private ComponentsTree tree;
    private ComponentsPosition componentsPosition;

    private int rowsCount;
    private int columnsCount;

    private ComponentWrapperControllerFactory wrapperControllerFactory;
    private Map<GridPosition, ComponentWrapperController> wrappersMap;
    private List<ConnectionLineController> lineControllerList;

    private ResourceProvider resourceProvider;

    private MoveManager moveManager;
    private NewConnectionMessenger newConnectionMessenger;
    private RemoveConnectionMessenger removeConnectionMessenger;
    private RemoveComponentMessenger removeComponentMessenger;

    @Inject
    private GridPositionModel gridPositionModel;
    private CopyModel copyModel;

    @Inject
    private ConnectionLineControllerFactory lineControllerFactory;
    @Inject
    private NewConnectionStartPointModel newConnectionStartPointModel;
    private ConnectionLineController lineController;

    @Inject
    private ComponentsFactory componentsFactory;
    @Inject
    private ComponentsService componentsService;

    @Inject
    private SelectedComponentsModel selectedComponentsModel;

    @Inject
    public ComponentsGridControllerImpl(
            @Editor ResourceProvider resourceProvider,
            ComponentWrapperControllerFactory wrapperControllerFactory,
            @Assisted ComponentsTree tree,
            @Assisted ComponentsPosition componentsPosition,
            MoveManager moveManager,
            NewConnectionMessenger newConnectionMessenger,
            CopyModel copyModel,
            RemoveConnectionMessenger removeConnectionMessenger,
            RemoveComponentMessenger removeComponentMessenger
    )
    {
        this.resourceProvider = resourceProvider;
        this.wrapperControllerFactory = wrapperControllerFactory;
        wrappersMap = new HashMap<>();
        lineControllerList = new ArrayList<>();
        this.tree = tree;
        this.componentsPosition = componentsPosition;
        this.moveManager = moveManager;
        moveManager.addCallback(this);
        this.newConnectionMessenger = newConnectionMessenger;
        newConnectionMessenger.addCallback(this);
        this.copyModel = copyModel;
        copyModel.addListener(this);
        this.removeConnectionMessenger = removeConnectionMessenger;
        removeConnectionMessenger.addCallback(this);
        this.removeComponentMessenger = removeComponentMessenger;
        removeComponentMessenger.addCallback(this);

        createUI();
    }

    private void createUI()
    {
        scrollPane = new ScrollPane();
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        anchorPane = new AnchorPane();
        anchorPane.setStyle("-fx-background-color: white");
        scrollPane.setContent(anchorPane);

        gridPane = new GridPane();
        gridPane.setHgap(GridPositionModel.GRID_SPACING);
        gridPane.setVgap(GridPositionModel.GRID_SPACING);
        gridPane.setPadding(new Insets(GridPositionModel.GRID_PADDING));

        gridPane.setOnMouseClicked(event ->
        {
            if (event.getButton() == MouseButton.PRIMARY)
                selectedComponentsModel.clearSelected();
        });
        gridPane.setOnDragOver(event ->
        {
            if (event.getTransferMode() == TransferMode.COPY && event.getDragboard().hasString() && componentsService.exists(event.getDragboard().getString()))
            {
                event.acceptTransferModes(TransferMode.COPY);
                event.consume();
            }
            else if (event.getTransferMode() == TransferMode.MOVE && moveManager.hasStarted() && tree.componentsExists(moveManager.getCurrent().getStartComponent()))
            {
                event.acceptTransferModes(TransferMode.MOVE);
                event.consume();
            }
            else if (event.getTransferMode() == TransferMode.LINK && newConnectionMessenger.hasStarted())
            {
                //event.acceptTransferModes(TransferMode.LINK);

                double x = 0;
                double y = 0;

                if (lineController.getStartPoint().getX() < event.getX())
                    x = event.getX() - 4;
                else
                    x = event.getX() + 4;

                if (lineController.getStartPoint().getY() < event.getY())
                    y = event.getY() - 4;
                else
                    y = event.getY() + 4;

                lineController.setEnd(x, y);
                event.consume();
            }

        });

        gridPane.setOnDragDropped(event ->
        {
            GridPosition position = gridPositionModel.getNearestPosition(new Point(event.getX(), event.getY()));

            if (position.getRow() > rowsCount - 1)
                position.setRow(rowsCount - 1);

            if (position.getColumn() > columnsCount - 1)
                position.setColumn(columnsCount - 1);

            if (event.getAcceptedTransferMode() == TransferMode.COPY)
            {
                try
                {
                    Component component = componentsFactory.create(event.getDragboard().getString());
                    copyModel.copy(tree, position, component);

                    event.consume();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if (event.getAcceptedTransferMode() == TransferMode.MOVE)
            {
                try
                {
                    if (moveManager.getCurrent().getStartComponent() != wrappersMap.get(position).getComponent()) // if moved not to same cell
                    {
                        moveManager.moveTo(position);

                        ClipboardContent clipboardContent = new ClipboardContent();
                        clipboardContent.putString("dropped");
                        event.getDragboard().setContent(clipboardContent);

                        event.consume();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        AnchorPane.setLeftAnchor(gridPane, 0d);
        AnchorPane.setTopAnchor(gridPane, 0d);
        AnchorPane.setRightAnchor(gridPane, 0d);
        AnchorPane.setBottomAnchor(gridPane, 0d);
        anchorPane.getChildren().add(gridPane);

        addColumns(DEFAULT_CELL_INCREASE);
        addRows(DEFAULT_CELL_INCREASE);
        enableAutoScroll();
    }

    private void enableAutoScroll()
    {
        gridPane.addEventHandler(DragEvent.DRAG_OVER, event ->
        {
            Bounds sceneBounds = scrollPane.localToScene(scrollPane.getBoundsInLocal());
            double sceneX = sceneBounds.getMinX();
            double sceneY = sceneBounds.getMinY();
            double height = scrollPane.getViewportBounds().getHeight();
            double width = scrollPane.getViewportBounds().getWidth();

            double marginBottom = sceneY + height - event.getSceneY();
            double marginTop = event.getSceneY() - sceneY;
            double marginLeft = event.getSceneX() - sceneX;
            double marginRight = sceneX + width - event.getSceneX();

            double vCoefficient = gridPane.getHeight() / 1.7;
            double hCoefficient = gridPane.getWidth() / 1.7;

            if (marginBottom <= 20 && marginBottom > 0)
                scrollPane.setVvalue(scrollPane.getVvalue() + (20 - marginBottom) / vCoefficient);
            else if (marginTop <= 20 && marginTop > 0)
                scrollPane.setVvalue(scrollPane.getVvalue() - (20 - marginTop) / vCoefficient);


            if (marginLeft <= 20 && marginLeft > 0)
                scrollPane.setHvalue(scrollPane.getHvalue() - (20 - marginLeft) / hCoefficient);
            else if (marginRight <= 20 && marginRight > 0)
                scrollPane.setHvalue(scrollPane.getHvalue() + (20 - marginRight) / hCoefficient);
        });
    }

    private ComponentWrapperController makeWrapper(GridPosition gridPosition)
    {
        ComponentWrapperController wrapperController = wrapperControllerFactory.create(tree, componentsPosition, gridPosition);
        GridPane.setHalignment(wrapperController.getView(), HPos.CENTER);
        GridPane.setValignment(wrapperController.getView(), VPos.CENTER);
        GridPane.setHgrow(wrapperController.getView(), Priority.ALWAYS);
        GridPane.setVgrow(wrapperController.getView(), Priority.ALWAYS);

        return wrapperController;
    }

    private void addRows(int count)
    {
        int previousCount = gridPane.getRowConstraints().size();

        for (int i = 0; i < count; i++)
            gridPane.getRowConstraints().add(new RowConstraints(GridPositionModel.CELL_SIZE));

        for (int i = previousCount; i < previousCount + count; i++)
        {
            for (int j = 0; j < gridPane.getColumnConstraints().size(); j++)
            {
                GridPosition position = new GridPosition(i, j);
                ComponentWrapperController wrapperController = makeWrapper(position);
                gridPane.add(wrapperController.getView(), j, i);
                wrappersMap.put(position, wrapperController);
            }
        }

        rowsCount = gridPane.getRowConstraints().size();
    }

    private void addColumns(int count)
    {
        int previousCount = gridPane.getColumnConstraints().size();

        for (int i = 0; i < count; i++)
            gridPane.getColumnConstraints().add(new ColumnConstraints(GridPositionModel.CELL_SIZE));

        for (int i = 0; i < gridPane.getRowConstraints().size(); i++)
        {
            for (int j = previousCount; j < previousCount + count; j++)
            {
                GridPosition position = new GridPosition(i, j);
                ComponentWrapperController wrapperController = makeWrapper(position);
                gridPane.add(wrapperController.getView(), j, i);
                wrappersMap.put(position, wrapperController);
            }
        }

        columnsCount = gridPane.getColumnConstraints().size();
    }

    @Override
    public Parent getView()
    {
        return scrollPane;
    }

    @Override
    public void onMoveStarted(MoveModel model)
    {

    }

    @Override
    public void onMove(MoveModel model)
    {
        if (model.getTree() == tree)
        {
            if (model.getEndPosition().getRow() == rowsCount - 1)
                addRows(DEFAULT_CELL_INCREASE);
            if (model.getEndPosition().getColumn() == columnsCount - 1)
                addColumns(DEFAULT_CELL_INCREASE);
        }
    }

    @Override
    public void onMoveNotEnded(MoveModel model)
    {

    }

    @Override
    public void onCopy(ComponentsTree tree, GridPosition position, Component component)
    {
        if (this.tree == tree)
        {
            if (position.getRow() == rowsCount - 1)
                addRows(DEFAULT_CELL_INCREASE);
            if (position.getColumn() == columnsCount - 1)
                addColumns(DEFAULT_CELL_INCREASE);
        }
    }

    @Override
    public void onStartedNewConnection(Component startComponent, ConnectionType type)
    {
        if (tree.componentsExists(startComponent))
        {
            lineController = lineControllerFactory.create(tree, componentsPosition);
            lineController.setStart(newConnectionStartPointModel.getX(), newConnectionStartPointModel.getY());
            lineController.setType(type);

            anchorPane.getChildren().addAll(lineController.getPaths());
        }
    }

    @Override
    public void onNotEndedNewConnection(Component startComponent, ConnectionType type)
    {
        if (tree.componentsExists(startComponent))
        {
            anchorPane.getChildren().removeAll(lineController.getPaths());
            lineController.clear();
            lineController = null;
        }
    }

    @Override
    public void onEndedNewConnection(Component startComponent, ConnectionType type, Component endComponent)
    {
        if (tree.componentsExists(startComponent))
        {
            lineController.setComponents(startComponent, endComponent);
            lineControllerList.add(lineController);

            ComponentDescriptor descriptor = tree.getDescriptor(startComponent);
            if (type == ConnectionType.GREEN)
                descriptor.setGreenTarget(endComponent);
            else if (type == ConnectionType.RED)
                descriptor.setRedTarget(endComponent);
        }
    }

    @Override
    public void onRemoveConnection(Component startComponent, Component endComponent, ConnectionType type)
    {
        if (tree.componentsExists(startComponent))
        {
            for (ConnectionLineController lineController : lineControllerList)
            {
                if (lineController.getStartComponent() == startComponent && lineController.getType() == type)
                {
                    anchorPane.getChildren().removeAll(lineController.getPaths());
                    lineControllerList.remove(lineController);
                    ComponentDescriptor descriptor = tree.getDescriptor(startComponent);
                    lineController.clearConnection();
                    if (type == ConnectionType.GREEN)
                        descriptor.setGreenTarget(null);
                    else if (type == ConnectionType.RED)
                        descriptor.setRedTarget(null);
                    
                    break;
                }
            }
        }
    }

    @Override
    public void onRemoveComponent(Component component)
    {
        if (tree.componentsExists(component))
        {
            ComponentDescriptor descriptor = tree.getDescriptor(component);

            // Remove output connections
            if (descriptor.hasGreenTarget())
                removeConnectionMessenger.request(component, descriptor.getGreenTarget(), ConnectionType.GREEN);
            if (descriptor.hasRedTarget())
                removeConnectionMessenger.request(component, descriptor.getRedTarget(), ConnectionType.RED);

            //Remove all connections to this component
            for (Map.Entry<Component, ComponentDescriptor> entry : tree.getComponentsMap().entrySet())
            {
                if (entry.getValue().getGreenTarget() == component)
                    removeConnectionMessenger.request(entry.getKey(), component, ConnectionType.GREEN);

                if (entry.getValue().getRedTarget() == component)
                    removeConnectionMessenger.request(entry.getKey(), component, ConnectionType.RED);
            }

            tree.removeComponent(component);
            componentsPosition.remove(component);
        }
    }
}

package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.editor.ui.messenger.*;
import com.romanccoder.axecutor.editor.ui.model.ConnectionPoints;
import com.romanccoder.axecutor.editor.ui.model.GridPositionModel;
import com.romanccoder.axecutor.editor.ui.model.MoveManager;
import com.romanccoder.axecutor.editor.ui.model.MoveModel;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;

import java.util.Observer;

/**
 * Created by Roman on 17.08.2016.
 */
public class ConnectionLineControllerImpl implements ConnectionLineController, MoveManager.MoveCallback
{
    private enum ControlPointsSet
    {
        ONE_COLUMN,
        DIFFERENCE_COLUMN
    }

    private Path linesPath;
    private Path arrowsPath;

    private boolean dotted;
    private boolean setManual;
    private MoveTo startMove;
    private LineTo lineTo;

    private Component startComponent;
    private ConnectionType type;
    private Component endComponent;

    @Inject
    private ConnectionPointsMessenger connectionPointsMessenger;

    private ComponentsPosition componentsPosition;

    private ConnectionPoints startConnectionPoints;
    private ConnectionPoints endConnectionPoints;

    private Observer observer;
    private ComponentsPosition.GridPositionListener gridPositionListener;

    @Inject
    private GridPositionModel gridPositionModel;

    @Inject
    public ConnectionLineControllerImpl(
            @Assisted ComponentsTree tree,
            @Assisted ComponentsPosition componentsPosition,
            MoveManager moveManager,
            RemoveConnectionMessenger removeConnectionMessenger
    )
    {
        linesPath = new Path();
        arrowsPath = new Path();

        linesPath.setStrokeWidth(STROKE_WIDTH);
        arrowsPath.setStrokeWidth(STROKE_WIDTH);
        linesPath.setOnMouseEntered(event ->
        {
            if (dotted)
                linesPath.getStrokeDashArray().clear();


            linesPath.setStrokeWidth(STROKE_WIDTH + STROKE_WIDTH_INCREASE);
            arrowsPath.setStrokeWidth(STROKE_WIDTH + STROKE_WIDTH_INCREASE);
        });
        linesPath.setOnMouseExited(event ->
        {
            if (dotted)
                linesPath.getStrokeDashArray().addAll(SEGMENT_LENGTH, SEGMENT_GAP);

            linesPath.setStrokeWidth(STROKE_WIDTH);
            arrowsPath.setStrokeWidth(STROKE_WIDTH);
        });

        this.componentsPosition = componentsPosition;

        observer = (o, arg) -> calculateLine();
        gridPositionListener = (component, position) -> calculateLine();

        ContextMenu contextMenu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Remove");
        removeMenuItem.setOnAction(event ->
        {
            removeConnectionMessenger.request(startComponent,  endComponent, type);
            event.consume();
        });
        contextMenu.getItems().add(removeMenuItem);
        linesPath.setOnMouseClicked(event ->
        {
            if (event.getButton() == MouseButton.SECONDARY)
            {
                contextMenu.show(linesPath, event.getScreenX(), event.getScreenY());

                event.consume();
            }
        });

        moveManager.addCallback(this);
    }

    @Override
    public void setStart(double x, double y)
    {
        clear();
        setManual = true;
        startMove = new MoveTo(x, y);
        lineTo = new LineTo();
        linesPath.getElements().addAll(startMove, lineTo);
    }

    @Override
    public Point getStartPoint()
    {
        if (setManual)
            return new Point(startMove.getX(), startMove.getY());

        throw new IllegalStateException();
    }

    @Override
    public void setType(ConnectionType type)
    {
        this.type = type;

        if (type == ConnectionType.GREEN)
        {
            linesPath.setStroke(Color.GREEN);
            arrowsPath.setStroke(Color.GREEN);
        }
        else if (type == ConnectionType.RED)
        {
            linesPath.setStroke(Color.RED);
            arrowsPath.setStroke(Color.RED);
        }
    }

    @Override
    public ConnectionType getType()
    {
        return type;
    }

    @Override
    public void setEnd(double x, double y)
    {
        if (setManual)
        {
            lineTo.setX(x);
            lineTo.setY(y);
        }
    }

    @Override
    public void setComponents(Component startComponent, Component endComponent)
    {
        if (this.startComponent != null && this.endComponent != null)
        {
            startConnectionPoints.deleteObserver(observer);
            componentsPosition.removeListener(this.startComponent, gridPositionListener);
            endConnectionPoints.deleteObserver(observer);
            componentsPosition.removeListener(this.endComponent, gridPositionListener);
        }

        this.startComponent = startComponent;
        startConnectionPoints = connectionPointsMessenger.request(startComponent);
        startConnectionPoints.addObserver(observer);
        componentsPosition.addListener(startComponent, gridPositionListener);

        this.endComponent = endComponent;
        endConnectionPoints = connectionPointsMessenger.request(endComponent);
        endConnectionPoints.addObserver(observer);
        componentsPosition.addListener(endComponent, gridPositionListener);

        calculateLine();
    }

    @Override
    public void clearConnection()
    {
        startConnectionPoints.deleteObserver(observer);
        componentsPosition.removeListener(startComponent, gridPositionListener);
        endConnectionPoints.deleteObserver(observer);
        componentsPosition.removeListener(endComponent, gridPositionListener);
        startComponent = null;
        endComponent = null;
    }

    @Override
    public Component getStartComponent()
    {
        return startComponent;
    }

    @Override
    public Component getEndComponent()
    {
        return endComponent;
    }

    @Override
    public void clear()
    {
        linesPath.getElements().clear();
        arrowsPath.getElements().clear();
    }

    private void setDotted(boolean dotted)
    {
        this.dotted = dotted;

        linesPath.getStrokeDashArray().clear();

        if (dotted)
            linesPath.getStrokeDashArray().addAll(SEGMENT_LENGTH, SEGMENT_GAP);
    }

    private void calculateLine()
    {
        clear();

        if (type == ConnectionType.GREEN)
            calculateGreen();
        else if (type == ConnectionType.RED)
            calculateRed();
    }

    private void calculateGreen()
    {
        GridPosition startPosition = componentsPosition.getPosition(startComponent);
        Point startPoint = gridPositionModel.getPoint(startPosition);
        GridPosition endPosition = componentsPosition.getPosition(endComponent);
        Point endPoint = gridPositionModel.getPoint(endPosition);

        if (startPosition.getColumn() == endPosition.getColumn() && endPosition.getRow() == startPosition.getRow() - 1)
        {
            drawLine(
                    getConnectionPoint(startComponent, startPosition, startConnectionPoints, ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_TOP),
                    getConnectionPoint(endComponent, endPosition, endConnectionPoints, ConnectionPoints.ConnectionPointsLocation.INPUT_BOTTOM)
            );

            setDotted(false);
        }
        else if (startPosition.getColumn() == endPosition.getColumn() && endPosition.getRow() == startPosition.getRow() + 1)
        {
            drawLine(
                    getConnectionPoint(startComponent, startPosition, startConnectionPoints, ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_BOTTOM),
                    getConnectionPoint(endComponent, endPosition, endConnectionPoints, ConnectionPoints.ConnectionPointsLocation.INPUT_TOP)
            );

            setDotted(false);
        }
        else if (startPosition.getColumn() == endPosition.getColumn())
        {
            drawCurvedLine(
                    getConnectionPoint(startComponent, startPosition, startConnectionPoints, ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_LEFT),
                    getConnectionPoint(endComponent, endPosition, endConnectionPoints, ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT),
                    ControlPointsSet.ONE_COLUMN
            );

            setDotted(true);
        }
        else if (startPosition.getRow() == endPosition.getRow() && Math.abs(startPosition.getColumn() - endPosition.getColumn()) == 1)
        {
            ConnectionPoints.ConnectionPointsLocation outputLocation;
            ConnectionPoints.ConnectionPointsLocation inputLocation;

            if (startPosition.getColumn() < endPosition.getColumn())
            {
                outputLocation = ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_RIGHT;
                inputLocation = ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT;

            }
            else
            {
                outputLocation = ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_LEFT;
                inputLocation = ConnectionPoints.ConnectionPointsLocation.INPUT_RIGHT;
            }

            drawLine(
                    getConnectionPoint(startComponent, startPosition, startConnectionPoints, outputLocation),
                    getConnectionPoint(endComponent, endPosition, endConnectionPoints, inputLocation)
            );

            setDotted(true);
        }
        else if (Math.abs(startPosition.getColumn() - endPosition.getColumn()) == 1)
        {
            ConnectionPoints.ConnectionPointsLocation outputLocation;
            ConnectionPoints.ConnectionPointsLocation inputLocation;

            if (startPosition.getColumn() < endPosition.getColumn())
            {
                outputLocation = ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_RIGHT;
                inputLocation = ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT;
            }
            else
            {
                outputLocation = ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_LEFT;
                inputLocation = ConnectionPoints.ConnectionPointsLocation.INPUT_RIGHT;
            }

            drawCurvedLine(
                    getConnectionPoint(startComponent, startPosition, startConnectionPoints, outputLocation),
                    getConnectionPoint(endComponent, endPosition, endConnectionPoints, inputLocation),
                    ControlPointsSet.DIFFERENCE_COLUMN
            );

            setDotted(true);
        }
        else
        {
            ConnectionPoints.ConnectionPointsLocation outputLocation;
            Point additionalPoint1;
            Point additionalPoint2;
            ConnectionPoints.ConnectionPointsLocation inputLocation;

            if (startPosition.getColumn() < endPosition.getColumn())
            {
                outputLocation = ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_RIGHT;
                Point additionalPoint1NotModified = gridPositionModel.getPoint(new GridPosition(endPosition.getRow(), startPosition.getColumn() + 1));
                additionalPoint1 = new Point(additionalPoint1NotModified.getX() - GridPositionModel.GRID_SPACING / 2, additionalPoint1NotModified.getY() - GridPositionModel.GRID_SPACING / 2);
                Point additionalPoint2NotModified = gridPositionModel.getPoint(new GridPosition(endPosition.getRow(), endPosition.getColumn() - 1));
                additionalPoint2 = new Point(additionalPoint2NotModified.getX() + GridPositionModel.CELL_SIZE + GridPositionModel.GRID_SPACING / 2, additionalPoint2NotModified.getY() - GridPositionModel.GRID_SPACING / 2);
                inputLocation = ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT;
            }
            else
            {
                outputLocation = ConnectionPoints.ConnectionPointsLocation.OUTPUT_GREEN_LEFT;
                Point additionalPoint1NotModified = gridPositionModel.getPoint(new GridPosition(endPosition.getRow(), startPosition.getColumn() - 1));
                additionalPoint1 = new Point(additionalPoint1NotModified.getX() + GridPositionModel.CELL_SIZE + GridPositionModel.GRID_SPACING / 2, additionalPoint1NotModified.getY() - GridPositionModel.GRID_SPACING / 2);
                Point additionalPoint2NotModified = gridPositionModel.getPoint(new GridPosition(endPosition.getRow(), endPosition.getColumn() + 1));
                additionalPoint2 = new Point(additionalPoint2NotModified.getX() - GridPositionModel.GRID_SPACING / 2, additionalPoint2NotModified.getY() - GridPositionModel.GRID_SPACING / 2);
                inputLocation = ConnectionPoints.ConnectionPointsLocation.INPUT_RIGHT;
            }

            drawCurvedLine(
                    getConnectionPoint(startComponent, startPosition, startConnectionPoints, outputLocation),
                    additionalPoint1,
                    ControlPointsSet.DIFFERENCE_COLUMN,
                    false
            );
            drawLine(additionalPoint1, additionalPoint2, false);
            drawCurvedLine(
                    additionalPoint2,
                    getConnectionPoint(endComponent, endPosition, endConnectionPoints, inputLocation),
                    ControlPointsSet.DIFFERENCE_COLUMN
            );

            setDotted(true);
        }
    }

    private void calculateRed()
    {
        GridPosition startPosition = componentsPosition.getPosition(startComponent);
        Point startPoint = gridPositionModel.getPoint(startPosition);
        GridPosition endPosition = componentsPosition.getPosition(endComponent);
        Point endPoint = gridPositionModel.getPoint(endPosition);

        if (startPosition.getColumn() == endPosition.getColumn())
        {
            drawCurvedLine(
                    new Point(
                            startPoint.getX() + startConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.OUTPUT_RED_LEFT).getX(),
                            startPoint.getY() + startConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.OUTPUT_RED_LEFT).getY()
                    ),
                    new Point(
                            endPoint.getX() + endConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT).getX(),
                            endPoint.getY() + endConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT).getY()
                    ),
                    ControlPointsSet.ONE_COLUMN
            );

            setDotted(true);
        }
        else if (startPosition.getRow() == endPosition.getRow() && Math.abs(startPosition.getColumn() - endPosition.getColumn()) == 1)
        {
            Point outputPoint;
            Point inputPoint;

            if (startPosition.getColumn() < endPosition.getColumn())
            {
                outputPoint = startConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.OUTPUT_RED_RIGHT);
                inputPoint = endConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT);

            }
            else
            {
                outputPoint = startConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.OUTPUT_RED_LEFT);
                inputPoint = endConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.INPUT_RIGHT);
            }

            drawLine(
                    new Point(
                            startPoint.getX() + outputPoint.getX(),
                            startPoint.getY() + outputPoint.getY()
                    ),
                    new Point(
                            endPoint.getX() + inputPoint.getX(),
                            endPoint.getY() + inputPoint.getY()
                    )
            );

            setDotted(true);
        }
        else if (Math.abs(startPosition.getColumn() - endPosition.getColumn()) == 1)
        {
            Point outputPoint;
            Point inputPoint;

            if (startPosition.getColumn() < endPosition.getColumn())
            {
                outputPoint = startConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.OUTPUT_RED_RIGHT);
                inputPoint = endConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.INPUT_LEFT);
            }
            else
            {
                outputPoint = startConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.OUTPUT_RED_LEFT);
                inputPoint = endConnectionPoints.getPoint(ConnectionPoints.ConnectionPointsLocation.INPUT_RIGHT);
            }

            drawCurvedLine(
                    new Point(
                            startPoint.getX() + outputPoint.getX(),
                            startPoint.getY() + outputPoint.getY()
                    ),
                    new Point(
                            endPoint.getX() + inputPoint.getX(),
                            endPoint.getY() + inputPoint.getY()
                    ),
                    ControlPointsSet.DIFFERENCE_COLUMN
            );

            setDotted(true);
        }
    }

    private Point getConnectionPoint(Component component, GridPosition position, ConnectionPoints connectionPoints, ConnectionPoints.ConnectionPointsLocation connectionPointLocation)
    {
        return new Point(
                gridPositionModel.getPoint(position).getX() + gridPositionModel.getHCellGap(component.getType()) + connectionPoints.getPoint(connectionPointLocation).getX(),
                gridPositionModel.getPoint(position).getY() + gridPositionModel.getVCellGap(component.getType()) + connectionPoints.getPoint(connectionPointLocation).getY()
        );
    }

    private void drawLine(Point startPoint, Point endPoint, boolean drawArrow)
    {
        MoveTo startMove = new MoveTo(startPoint.getX(), startPoint.getY());
        linesPath.getElements().add(startMove);

        LineTo mainLine = new LineTo();
        mainLine.setX(endPoint.getX());
        mainLine.setY(endPoint.getY());
        linesPath.getElements().add(mainLine);

        if (drawArrow)
        {
            double lineAngle = Math.atan2(startPoint.getY() - endPoint.getY(), startPoint.getX() - endPoint.getX());

            double arrowX1 = Math.cos(lineAngle + Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getX();
            double arrowY1 = Math.sin(lineAngle + Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getY();

            double arrowX2 = Math.cos(lineAngle - Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getX();
            double arrowY2 = Math.sin(lineAngle - Math.toRadians(ARROW_ANGLE)) * ARROW_LENGTH + endPoint.getY();

            MoveTo leftArrowMove = new MoveTo(endPoint.getX(), endPoint.getY());
            LineTo leftArrowLine = new LineTo(arrowX1, arrowY1);
            MoveTo rightArrowMove = new MoveTo(endPoint.getX(), endPoint.getY());
            LineTo rightArrowLine = new LineTo(arrowX2, arrowY2);

            arrowsPath.getElements().addAll(leftArrowMove, leftArrowLine, rightArrowMove, rightArrowLine);
        }

    }

    private void drawLine(Point startPoint, Point endPoint)
    {
        drawLine(startPoint, endPoint, true);
    }

    private void drawCurvedLine(Point startPoint, Point endPoint, ControlPointsSet controlPointsSet, boolean drawArrow)
    {
        MoveTo startMove = new MoveTo(startPoint.getX(), startPoint.getY());
        linesPath.getElements().add(startMove);

        CubicCurveTo cubicCurveTo = new CubicCurveTo();

        if (controlPointsSet.equals(ControlPointsSet.ONE_COLUMN))
        {
            cubicCurveTo.setControlX1(startPoint.getX() - 50);
            cubicCurveTo.setControlY1(startPoint.getY());
            cubicCurveTo.setControlX2(endPoint.getX() - 50);
            cubicCurveTo.setControlY2(endPoint.getY());
        }
        else if (controlPointsSet.equals(ControlPointsSet.DIFFERENCE_COLUMN))
        {
            cubicCurveTo.setControlX1(endPoint.getX());
            cubicCurveTo.setControlY1(startPoint.getY());
            cubicCurveTo.setControlX2(startPoint.getX());
            cubicCurveTo.setControlY2(endPoint.getY());
        }


        cubicCurveTo.setX(endPoint.getX());
        cubicCurveTo.setY(endPoint.getY());

        linesPath.getElements().add(cubicCurveTo);

        if (drawArrow)
        {
            Rotate rotation = new Rotate(ARROW_ANGLE);
            Point2D tan = new Point2D(
                    cubicCurveTo.getControlX2() - cubicCurveTo.getX(),
                    cubicCurveTo.getControlY2() - cubicCurveTo.getY()
            ).normalize().multiply(ARROW_LENGTH);

            MoveTo move = new MoveTo(cubicCurveTo.getX(), cubicCurveTo.getY());
            Point2D arrowPoint = rotation.transform(tan);

            LineTo arrow1Line = new LineTo(arrowPoint.getX(), arrowPoint.getY());
            arrow1Line.setAbsolute(false);
            rotation.setAngle(-ARROW_ANGLE);

            arrowPoint = rotation.transform(tan);
            LineTo arrow2Line = new LineTo(arrowPoint.getX(), arrowPoint.getY());
            arrow2Line.setAbsolute(false);

            arrowsPath.getElements().addAll(move, arrow1Line, move, arrow2Line);
        }
    }

    private void drawCurvedLine(Point startPoint, Point endPoint, ControlPointsSet controlPointsSet)
    {
        drawCurvedLine(startPoint, endPoint, controlPointsSet, true);
    }

    @Override
    public Path[] getPaths()
    {
        return new Path[] {linesPath, arrowsPath};
    }

    @Override
    public Parent getView()
    {
        return null;
    }

    @Override
    public void onMoveStarted(MoveModel model)
    {
        if (model.getStartComponent() == startComponent || model.getStartComponent() == endComponent)
            clear();
    }

    @Override
    public void onMove(MoveModel model)
    {
        if (model.getStartComponent() == startComponent || model.getStartComponent() == endComponent)
            calculateLine();
    }

    @Override
    public void onMoveNotEnded(MoveModel model)
    {
        if (model.getStartComponent() == startComponent || model.getStartComponent() == endComponent)
            calculateLine();
    }
}

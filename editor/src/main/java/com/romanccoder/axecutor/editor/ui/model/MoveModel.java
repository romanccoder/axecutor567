package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.GridPosition;

import java.util.List;

/**
 * Created by Roman on 11.08.2016.
 */
public abstract class MoveModel
{
    protected abstract void setCallbackList(List<MoveManager.MoveCallback> callbackList);

    protected abstract void setTree(ComponentsTree tree);
    public abstract ComponentsTree getTree();

    protected abstract void setStartComponent(Component startComponent);
    public abstract Component getStartComponent();

    protected abstract void setEndPosition(GridPosition gridPosition);
    public abstract GridPosition getEndPosition();

    protected abstract void end();
}

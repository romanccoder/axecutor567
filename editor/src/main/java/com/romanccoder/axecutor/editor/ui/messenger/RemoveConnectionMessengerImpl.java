package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ConnectionType;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Roman on 03.08.2016.
 */
public class RemoveConnectionMessengerImpl implements RemoveConnectionMessenger
{
    private List<RemoveConnectionCallback> callbackList;

    public RemoveConnectionMessengerImpl()
    {
        callbackList = new CopyOnWriteArrayList<>();
    }

    @Override
    public void request(Component startComponent, Component endComponent, ConnectionType type)
    {
        for (RemoveConnectionCallback callback : callbackList)
            callback.onRemoveConnection(startComponent, endComponent, type);
    }

    @Override
    public void addCallback(RemoveConnectionCallback callback)
    {
        callbackList.add(callback);
    }

    @Override
    public void removeCallback(RemoveConnectionCallback callback)
    {
        callbackList.remove(callback);
    }
}

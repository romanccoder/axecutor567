package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentCategory;
import com.romanccoder.axecutor.core.component.ComponentsService;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.renderer.ComponentRenderer;
import javafx.scene.Parent;
import javafx.scene.control.ListView;

/**
 * Created by Roman on 07.07.2016.
 */
public class ComponentsListControllerImpl implements ComponentsListController
{
    private TranslationProvider coreTranslationProvider;
    private ResourceProvider coreResourceProvider;

    private TitledListController titledListController;
    private Provider<TitledPaneBuilder> builderProvider;
    private ComponentRenderer componentRenderer;
    private ComponentsService componentsService;

    @Inject
    public ComponentsListControllerImpl(
            TitledListController titledListController,
            @Core TranslationProvider coreTranslationProvider,
            @Editor ResourceProvider coreResourceProvider,
            Provider<TitledPaneBuilder> builderProvider,
            ComponentRenderer componentRenderer,
            ComponentsService componentsService
    )
    {
        this.coreTranslationProvider = coreTranslationProvider;
        this.coreResourceProvider = coreResourceProvider;
        this.titledListController = titledListController;
        this.builderProvider = builderProvider;
        this.componentRenderer = componentRenderer;
        this.componentsService = componentsService;

        drawComponents();
    }

    private void drawComponents()
    {
        for (ComponentCategory category : ComponentCategory.values())
        {
            TitledPaneBuilder builder = builderProvider.get();
            builder.setLabel(coreTranslationProvider.get(category));
            titledListController.getService().add(builder);

            ListView<Component> componentsListView = new ListView<>();
            componentsListView.getStylesheets().add(coreResourceProvider.getCSS("components_list.css").toExternalForm());
            componentsListView.getItems().addAll(componentsService.getByCategory(category));
            componentsListView.setCellFactory(componentRenderer);
            builder.setContent(componentsListView);
        }
    }

    @Override
    public Parent getView()
    {
        return titledListController.getView();
    }
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by roman on 9/14/16.
 */
public interface RemoveComponentMessenger
{
    void request(Component component);

    void addCallback(RemoveComponentCallback callback);
    void removeCallback(RemoveComponentCallback callback);
}

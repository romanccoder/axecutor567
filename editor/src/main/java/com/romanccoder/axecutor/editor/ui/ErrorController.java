package com.romanccoder.axecutor.editor.ui;

/**
 * Created by Roman on 09.07.2016.
 */
public interface ErrorController
{
    void setException(Exception e);
    void showDialog();
}

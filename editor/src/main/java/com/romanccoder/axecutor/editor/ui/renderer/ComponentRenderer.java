package com.romanccoder.axecutor.editor.ui.renderer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.editor.ui.ComponentItemController;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * Created by Roman on 07.07.2016.
 */
public class ComponentRenderer implements Callback<ListView<Component>, ListCell<Component>>
{
    private class DraggableListCell extends ListCell<Component>
    {
        protected ComponentItemController controller;

        private DraggableListCell()
        {
            controller = provider.get();
        }

        @Override
        protected void updateItem(Component item, boolean empty)
        {
            if (empty)
            {
                setText(null);
                setGraphic(null);
            }
            else
            {
                controller.setComponent(item);
                setText(null);
                setGraphic(controller.getView());
            }
        }
    }

    @Inject
    private Provider<ComponentItemController> provider;

    @Override
    public ListCell<Component> call(ListView<Component> param)
    {
        return new DraggableListCell();
    }

}

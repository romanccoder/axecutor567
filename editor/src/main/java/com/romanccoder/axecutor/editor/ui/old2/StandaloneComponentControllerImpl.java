package com.romanccoder.axecutor.editor.ui.old2;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionCallback;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.model.DragModel;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;

/**
 * Created by Roman on 31.07.2016.
 */
public class StandaloneComponentControllerImpl extends AbstractController implements StandaloneComponentController, NewConnectionCallback
{
    @FXML
    private AnchorPane rootPane;
    @FXML
    private Rectangle mainRectangle;
    @FXML
    private Rectangle leftRedRectangle;
    @FXML
    private Rectangle leftGreenRectangle;
    @FXML
    private Rectangle leftInputRectangle;
    @FXML
    private Rectangle rightRedRectangle;
    @FXML
    private Rectangle rightGreenRectangle;
    @FXML
    private Rectangle rightInputRectangle;

    private NewConnectionMessenger newConnectionMessenger;
    private DragModel dragModel;

    private Component component;
    private ComponentDescriptor descriptor;

    @Inject
    public StandaloneComponentControllerImpl(
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider,
            NewConnectionMessenger newConnectionMessenger,
            DragModel dragModel
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        this.newConnectionMessenger = newConnectionMessenger;
        newConnectionMessenger.addCallback(this);
        this.dragModel = dragModel;

        initUI();
    }

    private void initUI()
    {
        EventHandler<? super MouseEvent> inputEnteredHandler = event -> {((Rectangle)event.getSource()).getStyleClass().add("input_hover"); event.consume();};
        EventHandler<? super MouseEvent> inputExitedHandler = event -> {((Rectangle) event.getSource()).getStyleClass().remove("input_hover"); event.consume();};
        EventHandler<? super DragEvent> inputDroppedHandler = event -> {newConnectionMessenger.end(getComponent()); event.consume();};
        leftInputRectangle.setOnMouseEntered(inputEnteredHandler);
        leftInputRectangle.setOnMouseExited(inputExitedHandler);
        leftInputRectangle.setOnDragDropped(inputDroppedHandler);
        rightInputRectangle.setOnMouseEntered(inputEnteredHandler);
        rightInputRectangle.setOnMouseExited(inputExitedHandler);
        rightInputRectangle.setOnDragDropped(inputDroppedHandler);

        EventHandler<? super MouseEvent> greenEnteredHandler = event -> {if (!getDescriptor().hasGreenTarget()) ((Rectangle)event.getSource()).getStyleClass().add("green_hover"); event.consume();};
        EventHandler<? super MouseEvent> greenExitedHandler = event -> {if (!getDescriptor().hasGreenTarget()) ((Rectangle) event.getSource()).getStyleClass().remove("green_hover"); event.consume();};
        EventHandler<? super MouseEvent> greenDragDetectedHandler = event ->
        {
            if (!getDescriptor().hasGreenTarget())
            {
                Dragboard db = ((Rectangle)event.getSource()).startDragAndDrop(TransferMode.LINK);
                ClipboardContent clipboardContent = new ClipboardContent();
                clipboardContent.putString("connect");
                db.setContent(clipboardContent);

                dragModel.setX(rootPane.getLayoutX() + ((Rectangle)event.getSource()).getLayoutX());
                dragModel.setY(rootPane.getLayoutY() + ((Rectangle)event.getSource()).getLayoutY());

                newConnectionMessenger.start(getComponent(), ConnectionType.GREEN);

                event.consume();
            }
        };
        leftGreenRectangle.setOnMouseEntered(greenEnteredHandler);
        leftGreenRectangle.setOnMouseExited(greenExitedHandler);
        leftGreenRectangle.setOnDragDetected(greenDragDetectedHandler);
        rightGreenRectangle.setOnMouseEntered(greenEnteredHandler);
        rightGreenRectangle.setOnMouseExited(greenExitedHandler);
        rightGreenRectangle.setOnDragDetected(greenDragDetectedHandler);

        EventHandler<? super MouseEvent> redEnteredHandler = event -> {if (!getDescriptor().hasRedTarget()) ((Rectangle)event.getSource()).getStyleClass().add("red_hover"); event.consume();};
        EventHandler<? super MouseEvent> redExitedHandler = event -> {if (!getDescriptor().hasRedTarget()) ((Rectangle) event.getSource()).getStyleClass().remove("red_hover"); event.consume();};
        leftRedRectangle.setOnMouseEntered(redEnteredHandler);
        leftRedRectangle.setOnMouseExited(redExitedHandler);
        rightRedRectangle.setOnMouseEntered(redEnteredHandler);
        rightRedRectangle.setOnMouseExited(redExitedHandler);
    }

    @Override
    public void setComponent(Component component)
    {
        this.component = component;
    }

    @Override
    public Component getComponent()
    {
        return component;
    }

    @Override
    public void setDescriptor(ComponentDescriptor descriptor)
    {
        this.descriptor = descriptor;
    }

    @Override
    public ComponentDescriptor getDescriptor()
    {
        return descriptor;
    }

    @Override
    public Parent getView()
    {
        return rootPane;
    }

    @Override
    public void onStartedNewConnection(Component startComponent, ConnectionType type)
    {

    }

    @Override
    public void onNotEndedNewConnection(Component startComponent, ConnectionType type)
    {

    }

    @Override
    public void onEndedNewConnection(Component startComponent, ConnectionType type, Component endComponent)
    {
        if (startComponent == getComponent())
        {
            if (type == ConnectionType.GREEN)
                getDescriptor().setGreenTarget(endComponent);
            else if (type == ConnectionType.RED)
                getDescriptor().setRedTarget(endComponent);
        }
    }

    @Override
    public Point[] getOutputPoints(ConnectionType type)
    {
        Point[] points = null;

        if (type == ConnectionType.GREEN)
        {
            points = new Point[4];
            points[0] = new Point(rootPane.getWidth() / 2, 0);
            points[1] = new Point(rightGreenRectangle.getX() + (rightGreenRectangle.getWidth() / 2), rightGreenRectangle.getY() + (rightGreenRectangle.getHeight() / 2));
            points[2] = new Point(rootPane.getWidth() / 2, rootPane.getHeight());
            points[3] = new Point(leftGreenRectangle.getX() + (leftGreenRectangle.getWidth() / 2), leftGreenRectangle.getY() + (leftGreenRectangle.getHeight() / 2));
        }

        return points;
    }

    @Override
    public Point[] getInputPoints(ConnectionType type)
    {
        Point[] points = null;

        if (type == ConnectionType.GREEN)
        {
            points = new Point[4];
            points[0] = new Point(rootPane.getWidth() / 2, 0);
            points[1] = new Point(rightInputRectangle.getX() + (rightInputRectangle.getWidth() / 2), rightInputRectangle.getY() + (rightInputRectangle.getHeight() / 2));
            points[2] = new Point(rootPane.getWidth() / 2, rootPane.getHeight());
            points[3] = new Point(leftInputRectangle.getX() + (leftInputRectangle.getWidth() / 2), leftInputRectangle.getY() + (leftInputRectangle.getHeight() / 2));
        }

        return points;
    }

}

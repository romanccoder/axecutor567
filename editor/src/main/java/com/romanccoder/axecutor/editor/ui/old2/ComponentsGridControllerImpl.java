package com.romanccoder.axecutor.editor.ui.old2;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.component.*;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import com.romanccoder.axecutor.editor.ui.control.ItemWrapper;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionCallback;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.messenger.ComponentPositionMessenger;
import com.romanccoder.axecutor.editor.ui.model.DragModel;
import com.romanccoder.axecutor.editor.ui.model.DropModel;
import com.romanccoder.axecutor.editor.ui.model.MoveManager;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by Roman on 31.07.2016.
 */
public class ComponentsGridControllerImpl //implements ComponentsGridController, NewConnectionCallback, ComponentPositionMessenger.ComponentPositionListener
{
    /*private ScrollPane scrollPane;
    private AnchorPane anchorPane;
    private GridPane gridPane;

    private ComponentsTree tree;
    private ComponentsPosition componentsPosition;

    private ResourceProvider resourceProvider;

    private ComponentsService componentsService;
    private ComponentsFactory componentsFactory;
    private Provider<StandaloneComponentController> standaloneControllerProvider;
    private Provider<ConnectionLineController> lineControllerProvider;

    private NewConnectionMessenger newConnectionMessenger;
    private ComponentPositionMessenger componentPositionMessenger;

    private MoveManager moveManager;
    private DropModel dropModel;
    private DragModel dragModel;

    private Map<Component, VisualComponentController> controllerMap;
    private ConnectionLineController lineController;
    private Map<Component, ConnectionLineController> lineMap;

    @Inject
    public ComponentsGridControllerImpl(
            @Editor ResourceProvider resourceProvider,
            ComponentsService componentsService,
            ComponentsFactory componentsFactory,
            Provider<StandaloneComponentController> standaloneControllerProvider,
            MoveManager moveManager,
            DropModel dropModel,
            DragModel dragModel,
            Provider<ConnectionLineController> lineControllerProvider,
            NewConnectionMessenger newConnectionMessenger,
            ComponentPositionMessenger componentPositionMessenger
    )
    {
        this.resourceProvider = resourceProvider;
        this.componentsService = componentsService;
        this.componentsFactory = componentsFactory;
        this.standaloneControllerProvider = standaloneControllerProvider;
        this.lineControllerProvider = lineControllerProvider;
        this.moveManager = moveManager;
        this.dropModel = dropModel;
        this.dragModel = dragModel;
        this.newConnectionMessenger = newConnectionMessenger;
        newConnectionMessenger.addListener(this);
        this.componentPositionMessenger = componentPositionMessenger;
        componentPositionMessenger.addListener(this);

        controllerMap = new HashMap<>();
        lineMap = new HashMap<>();

        createUI();
    }

    private void createUI()
    {
        scrollPane = new ScrollPane();
        scrollPane.getStylesheets().add(resourceProvider.getCSS("components_wrapper.css").toExternalForm());
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        anchorPane = new AnchorPane();
        scrollPane.setContent(anchorPane);

        gridPane = new GridPane();
        gridPane.setHgap(25);
        gridPane.setVgap(25);
        gridPane.setPadding(new Insets(20));
        gridPane.setOnDragEntered(event -> highlightCells());
        gridPane.setOnDragExited(event -> unHighlightCells());
        gridPane.setOnDragOver(event ->
        {
            if (event.getTransferMode() == TransferMode.COPY && event.getDragboard().hasString() && componentsService.exists(event.getDragboard().getString()))
            {
                event.acceptTransferModes(TransferMode.COPY);
            }
            else if (event.getTransferMode() == TransferMode.MOVE && moveManager.isActive() && getWrapper(moveManager.getStartPosition()) != null)
            {
                event.acceptTransferModes(TransferMode.MOVE);
            }
            else if (event.getTransferMode() == TransferMode.LINK && newConnectionMessenger.hasStarted())
            {
                event.acceptTransferModes(TransferMode.LINK);
                lineController.setEnd(event.getX() - 5, event.getY() - 5);
            }

            Bounds sceneBounds = scrollPane.localToScene(scrollPane.getBoundsInLocal());
            double sceneX = sceneBounds.getMinX();
            double sceneY = sceneBounds.getMinY();
            double height = scrollPane.getViewportBounds().getHeight();
            double width = scrollPane.getViewportBounds().getWidth();

            double marginBottom = sceneY + height - event.getSceneY();
            double marginTop = event.getSceneY() - sceneY;
            double marginLeft = event.getSceneX() - sceneX;
            double marginRight = sceneX + width - event.getSceneX();

            double vCoefficient = gridPane.getHeight() / 1.7;
            double hCoefficient = gridPane.getWidth() / 1.7;

            if (marginBottom <= 20 && marginBottom > 0)
                scrollPane.setVvalue(scrollPane.getVvalue() + (20 - marginBottom) / vCoefficient);
            else if (marginTop <= 20 && marginTop > 0)
                scrollPane.setVvalue(scrollPane.getVvalue() - (20 - marginTop) / vCoefficient);


            if (marginLeft <= 20 && marginLeft > 0)
                scrollPane.setHvalue(scrollPane.getHvalue() - (20 - marginLeft) / hCoefficient);
            else if (marginRight <= 20 && marginRight > 0)
                scrollPane.setHvalue(scrollPane.getHvalue() + (20 - marginRight) / hCoefficient);

            event.consume();

        });
        gridPane.setOnDragDropped(event ->
        {
            if (event.getAcceptedTransferMode() == TransferMode.COPY)
            {
                try
                {
                    Component component = componentsFactory.create(event.getDragboard().getString());

                    if (dropModel.hasPosition())
                        addComponent(component, dropModel.getPosition());

                    event.setDropCompleted(true);
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
                finally
                {
                    dropModel.setPosition(null);
                }
            }
            else if (event.getAcceptedTransferMode() == TransferMode.MOVE)
            {
                try
                {
                    if (moveManager.hasEndPosition() && !moveManager.getStartPosition().equals(moveManager.getEndPosition())) // was not dropped on same cell
                    {
                        moveComponent(moveManager.getStartPosition(), moveManager.getEndPosition());
                        getWrapper(moveManager.getEndPosition()).showWrapped();

                        event.setDropCompleted(true);
                    }
                    else if (!moveManager.hasEndPosition() || moveManager.getStartPosition().equals(moveManager.getEndPosition())) // if moved to white space
                    {
                        getWrapper(moveManager.getStartPosition()).showWrapped();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    moveManager.reset();
                }
            }
            else if (event.getAcceptedTransferMode() == TransferMode.LINK)
            {
                newConnectionMessenger.end();
            }
        });
        AnchorPane.setLeftAnchor(gridPane, 0d);
        AnchorPane.setTopAnchor(gridPane, 0d);
        AnchorPane.setRightAnchor(gridPane, 0d);
        AnchorPane.setBottomAnchor(gridPane, 0d);
        anchorPane.getChildren().add(gridPane);
        createCells();
    }

    private void createCells()
    {
        increaseRows(15);
        increaseColumns(15);
    }

    private void increaseRows(int count)
    {
        int previousCount = gridPane.getRowConstraints().size();

        for (int i = 0; i < count; i++)
            gridPane.getRowConstraints().add(new RowConstraints(80));

        for (int i = previousCount; i < previousCount + count; i++)
        {
            for (int j = 0; j < gridPane.getColumnConstraints().size(); j++)
            {
                ItemWrapper wrapper = makeWrapper();
                GridPane.setHgrow(wrapper, Priority.ALWAYS);
                GridPane.setVgrow(wrapper, Priority.ALWAYS);
                gridPane.add(wrapper, j, i);
            }
        }
    }

    private void increaseColumns(int count)
    {
        int previousCount = gridPane.getColumnConstraints().size();

        for (int i = 0; i < count; i++)
            gridPane.getColumnConstraints().add(new ColumnConstraints(80));

        for (int i = 0; i < gridPane.getRowConstraints().size(); i++)
        {
            for (int j = previousCount; j < previousCount + count; j++)
            {
                ItemWrapper wrapper = makeWrapper();
                GridPane.setHgrow(wrapper, Priority.ALWAYS);
                GridPane.setVgrow(wrapper, Priority.ALWAYS);
                gridPane.add(wrapper, j, i);
            }
        }
    }

    private ItemWrapper makeWrapper()
    {
        ItemWrapper wrapper = new ItemWrapper();
        wrapper.setOnDragDetected(event ->
        {
            if (wrapper.hasWrapped())
            {
                Dragboard db = wrapper.startDragAndDrop(TransferMode.MOVE);
                db.setDragView(wrapper.getWrapped().snapshot(null, null));
                ClipboardContent clipboardContent = new ClipboardContent();
                clipboardContent.putString("move");
                db.setContent(clipboardContent);
                moveManager.setStartPosition(getPosition(wrapper));
                wrapper.hideWrapped();

                event.consume();
            }
        });
        wrapper.setOnDragDropped(event ->
        {
            if (event.getAcceptedTransferMode() == TransferMode.MOVE && moveManager.isActive())
                moveManager.setEndPosition(getPosition(wrapper));
            else if(event.getAcceptedTransferMode() == TransferMode.COPY)
                dropModel.setPosition(getPosition(wrapper));
        });
        wrapper.setOnDragEntered(event -> wrapper.getStyleClass().add("wrapper_hover"));
        wrapper.setOnDragExited(event -> wrapper.getStyleClass().remove("wrapper_hover"));

        return wrapper;
    }

    private GridPosition getPosition(ItemWrapper wrapper)
    {
        return new GridPosition(GridPane.getRowIndex(wrapper), GridPane.getColumnIndex(wrapper));
    }

    private ItemWrapper getWrapper(GridPosition position)
    {
        for (Node node : gridPane.getChildren())
        {
            if (GridPane.getColumnIndex(node) == position.getColumn() && GridPane.getRowIndex(node) == position.getRow())
                return (ItemWrapper) node;
        }

        return null;
    }

    private void addComponent(Component component, GridPosition position)
    {
        if (position.getRow() == gridPane.getRowConstraints().size() - 1)
            increaseRows(DEFAULT_INCREASE);

        if (position.getColumn() == gridPane.getColumnConstraints().size() - 1)
            increaseColumns(DEFAULT_INCREASE);

        ItemWrapper wrapper = getWrapper(position);

        if (wrapper.hasWrapped())
            moveComponent(position, new GridPosition(position.getRow() + 1, position.getColumn()));

        //TODO move if exists

        VisualComponentController componentController = null;

        if (component.getType() == ComponentType.STANDALONE)
        {
            componentController = standaloneControllerProvider.get();
        }
        else if (component.getType() == ComponentType.CORE)
        {

        }


        // TODO set properties
        tree.addComponent(component);
        componentsPosition.set(component, position);

        componentController.setComponent(component);
        componentController.setDescriptor(tree.getDescriptor(component));
        controllerMap.put(component, componentController);
        wrapper.setWrapped(componentController.getView());
    }

    private void moveComponent(GridPosition currentPosition, GridPosition newPosition)
    {
        if (currentPosition.equals(newPosition) || !getWrapper(currentPosition).hasWrapped())
            throw new IllegalArgumentException();

        if (newPosition.getRow() >= gridPane.getRowConstraints().size() - 1)
        {
            int difference = newPosition.getRow() - gridPane.getRowConstraints().size() - 1;
            int count = difference < 5 ? DEFAULT_INCREASE : difference;
            increaseRows(count);
        }

        if (newPosition.getColumn() >= gridPane.getColumnConstraints().size() - 1)
        {
            int difference = newPosition.getColumn() - gridPane.getColumnConstraints().size() - 1;
            int count = difference < 5 ? DEFAULT_INCREASE : difference;
            increaseColumns(count);
        }

        ItemWrapper currentPositionWrapper = getWrapper(currentPosition);
        ItemWrapper newPositionWrapper = getWrapper(newPosition);

        if (newPositionWrapper.hasWrapped())
            moveComponent(newPosition, new GridPosition(newPosition.getRow() + 1, newPosition.getColumn()));

        componentsPosition.set(componentsPosition.getComponent(currentPosition), newPosition);
        newPositionWrapper.swapWrapped(currentPositionWrapper);
    }

    @Override
    public void onStartedNewConnection(Component startComponent, ConnectionType type)
    {
        if (tree.componentsExists(startComponent))
        {
            lineController = lineControllerProvider.get();
            ItemWrapper wrapper = getWrapper(componentsPosition.getPosition(startComponent));
            lineController.setStart(wrapper.getLayoutX() + dragModel.getX(), wrapper.getLayoutY() + dragModel.getY());

            anchorPane.getChildren().add(lineController.getPath());
        }
    }

    @Override
    public void onNotEndedNewConnection(Component startComponent, ConnectionType type)
    {
        if (tree.componentsExists(startComponent))
        {
            anchorPane.getChildren().remove(lineController.getPath());
            lineController.clear();
            lineController = null;
        }
    }

    @Override
    public void onEndedNewConnection(Component startComponent, ConnectionType type, Component endComponent)
    {
        if (tree.componentsExists(startComponent) && tree.componentsExists(endComponent))
        {
            VisualComponentController startController = controllerMap.get(startComponent);
            lineController.setStart(startComponent, startController.getOutputPoints(type));

            VisualComponentController endController = controllerMap.get(endComponent);
            lineController.setEnd(endComponent, endController.getInputPoints(type));
        }
    }

    @Override
    public GridPosition getPosition(Component component)
    {
        try
        {
            return componentsPosition.getPosition(component);
        }
        catch (NoSuchElementException e)
        {
            return null;
        }
    }

    @Override
    public Point getPoint(Component component)
    {
        GridPosition position = getPosition(component);

        if (position != null)
        {
            ItemWrapper wrapper = getWrapper(position);
            Point point = new Point();
            point.setX(wrapper.getLayoutX() + wrapper.getWrapped().getLayoutX());
            point.setY(wrapper.getLayoutY() + wrapper.getWrapped().getLayoutY());

            return point;
        }

        return null;
    }

    private void highlightCells()
    {
        for (Node node : gridPane.getChildren())
            node.getStyleClass().add("wrapper_highlight");
    }

    private void unHighlightCells()
    {
        for (Node node : gridPane.getChildren())
            node.getStyleClass().remove("wrapper_highlight");
    }

    @Override
    public void setComponentsTree(ComponentsTree tree)
    {
        this.tree = tree;
    }

    @Override
    public ComponentsTree getComponentsTree()
    {
        return tree;
    }

    @Override
    public void setComponentsPosition(ComponentsPosition componentsPosition)
    {
        this.componentsPosition = componentsPosition;
    }

    @Override
    public ComponentsPosition getComponentsPosition()
    {
        return componentsPosition;
    }

    @Override
    public Parent getView()
    {
        return scrollPane;
    }*/
}

package com.romanccoder.axecutor.editor.ui.model;

/**
 * Created by Roman on 24.07.2016.
 */
public interface Callback<T>
{
    void call(T param);
}

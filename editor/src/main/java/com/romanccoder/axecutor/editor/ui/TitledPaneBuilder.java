package com.romanccoder.axecutor.editor.ui;

import javafx.scene.Node;
import javafx.scene.control.TitledPane;

import java.net.URL;

/**
 * Created by Roman on 07.07.2016.
 */
public interface TitledPaneBuilder
{
    void setLabel(String label);
    String getLabel();

    void setIcon(URL icon);
    URL getIcon();

    void setCollapsible(boolean collapsible);
    boolean isCollapsible();

    void setExpanded(boolean expanded);
    boolean isExpanded();

    void setContent(Node node);
    Node getContent();

    void setAssociatedObject(Object associatedObj);
    Object getAssociatedObject();

    TitledPane getPane();
}

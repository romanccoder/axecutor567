package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.project.GridPosition;

/**
 * Created by Roman on 01.08.2016.
 */
public class DropModelImpl implements DropModel
{
    private GridPosition position;

    @Override
    public void setPosition(GridPosition position)
    {
        this.position = position;
    }

    @Override
    public GridPosition getPosition()
    {
        return position;
    }

    @Override
    public boolean hasPosition()
    {
        return position != null;
    }
}

package com.romanccoder.axecutor.editor.ui.model;

import javafx.beans.property.StringProperty;

import java.util.Observable;

/**
 * Created by Roman on 07.07.2016.
 */
public abstract class CreateProjectModel extends Observable
{
    public abstract StringProperty nameProperty();
    public abstract void setName(String name);
    public abstract String getName();

    public abstract StringProperty basePathProperty();
    public abstract void setBasePath(String basePath);
    public abstract String getBasePath();

    public abstract StringProperty fullPathProperty();
    public abstract String getFullPath();
}

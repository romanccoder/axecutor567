package com.romanccoder.axecutor.editor.ui.old2;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by Roman on 31.07.2016.
 */
public interface ComponentsGridController extends Controller
{
    int DEFAULT_INCREASE = 15;

    void setComponentsTree(ComponentsTree tree);
    ComponentsTree getComponentsTree();

    void setComponentsPosition(ComponentsPosition componentsPosition);
    ComponentsPosition getComponentsPosition();
}

package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.project.Project;
import com.romanccoder.axecutor.editor.ui.model.ActiveProjectsModel;
import javafx.collections.SetChangeListener;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Roman on 31.07.2016.
 */
public class ProjectsControllerImpl implements ProjectsController
{
    private class ModelChangeListener implements SetChangeListener<Project>
    {
        @Override
        public void onChanged(SetChangeListener.Change<? extends Project> change)
        {
            if (change.wasAdded())
                addTab(change.getElementAdded());
            else if (change.wasRemoved())
                removeTab(change.getElementRemoved());
        }
    }

    private Map<Project, Tab> tabMap;
    private Map<Project, ComponentsGridController> controllerMap;

    private TabPane tabPane;

    private ActiveProjectsModel model;

    @Inject
    private ComponentsGridControllerFactory gridControllerFactory;

    @Inject
    public ProjectsControllerImpl(ActiveProjectsModel activeProjectsModel)
    {
        tabMap = new HashMap<>();
        controllerMap = new HashMap<>();

        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);

        this.model = activeProjectsModel;

        activeProjectsModel.addListener(new ModelChangeListener());
    }

    private void addTab(Project project)
    {
        Tab tab = new Tab();
        tab.setOnCloseRequest(event -> {model.removeProject(project);event.consume();});
        tab.setText(project.getName());
        tabPane.getTabs().add(tab);
        //TODO Add tooltip with path

        ComponentsGridController gridController = gridControllerFactory.create(project.getTree(), project.getPositions());
        tab.setContent(gridController.getView());

        tabMap.put(project, tab);
        controllerMap.put(project, gridController);
    }

    private void removeTab(Project project)
    {
        tabPane.getTabs().remove(tabMap.get(project));

        tabMap.remove(project);
        controllerMap.remove(project);
    }

    @Override
    public Parent getView()
    {
        return tabPane;
    }
}

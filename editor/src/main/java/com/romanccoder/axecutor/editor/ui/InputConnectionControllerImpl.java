package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionCallback;
import com.romanccoder.axecutor.editor.ui.messenger.NewConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.messenger.RemoveConnectionCallback;
import com.romanccoder.axecutor.editor.ui.messenger.RemoveConnectionMessenger;
import com.romanccoder.axecutor.editor.ui.model.ConnectionModel;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Created by roman on 9/26/16.
 */
public class InputConnectionControllerImpl implements InputConnectionController, NewConnectionCallback, RemoveConnectionCallback
{
    private Rectangle rectangle;
    private EventHandler<? super DragEvent> dragOverHandler;
    private EventHandler<? super DragEvent> dragDroppedHandler;

    private EventHandler<? super MouseEvent> rootEnteredHandler;
    private EventHandler<? super MouseEvent> rootExitedHandler;

    private Component component;
    private ComponentsTree tree;

    @Inject
    private NewConnectionMessenger newConnectionMessenger;

    @Inject
    public InputConnectionControllerImpl(
            NewConnectionMessenger newConnectionMessenger,
            RemoveConnectionMessenger removeConnectionMessenger,
            @Assisted Component component,
            @Assisted ComponentsTree tree
    )
    {
        this.newConnectionMessenger = newConnectionMessenger;
        rectangle = new Rectangle(ConnectionModel.SHAPE_WIDTH, ConnectionModel.SHAPE_HEIGHT);
        rectangle.setRotate(45);
        rectangle.setStroke(Color.GRAY);
        rectangle.setFill(Color.TRANSPARENT);

        setComponent(component);
        setTree(tree);

        newConnectionMessenger.addCallback(this);
        removeConnectionMessenger.addCallback(this);

        initUI();
    }

    private void initUI()
    {
        if (!tree.getConnectedTo(component).isEmpty())
        {
            show();
            highlight();
        }
        else
        {
            hide();
            unHighlight();
        }

        dragOverHandler = event ->
        {
            if (event.getTransferMode() == TransferMode.LINK)
            {
                event.acceptTransferModes(TransferMode.LINK);
            }
        };
        dragDroppedHandler = event ->
        {
            if (event.getAcceptedTransferMode() == TransferMode.LINK)
            {
                ClipboardContent clipboardContent = new ClipboardContent();
                clipboardContent.putString(CONNECTION_ENDED);
                event.getDragboard().setContent(clipboardContent);
                newConnectionMessenger.end(component);
                event.setDropCompleted(true);
                event.consume();
            }
        };

        rectangle.setOnDragOver(dragOverHandler);
        rectangle.setOnDragDropped(dragDroppedHandler);

        rootEnteredHandler = event -> show();
        rootExitedHandler = event ->
        {
            if (tree.getConnectedTo(component).isEmpty())
                hide();
        };
    }

    @Override
    public void applyDragHandlers(Node node)
    {
        node.addEventHandler(DragEvent.DRAG_OVER, dragOverHandler);
        node.addEventHandler(DragEvent.DRAG_DROPPED, dragDroppedHandler);
    }

    private void setComponent(Component component)
    {
        this.component = component;
    }

    private void setTree(ComponentsTree tree)
    {
        this.tree = tree;
    }

    private void highlight()
    {
        rectangle.setFill(Color.GRAY);
    }

    private void unHighlight()
    {
        rectangle.setFill(Color.TRANSPARENT);
    }

    private void show()
    {
        rectangle.setVisible(true);
    }

    private void hide()
    {
        rectangle.setVisible(false);
    }

    @Override
    public Parent getView()
    {
        return null;
    }

    @Override
    public void applyMouseHandlers(Node... nodes)
    {
        for (Node node : nodes)
        {
            node.addEventHandler(MouseEvent.MOUSE_ENTERED, rootEnteredHandler);
            node.addEventHandler(MouseEvent.MOUSE_EXITED, rootExitedHandler);
        }
    }

    @Override
    public Shape getShape()
    {
        return rectangle;
    }

    @Override
    public void onStartedNewConnection(Component startComponent, ConnectionType type)
    {
    }

    @Override
    public void onRemoveConnection(Component startComponent, Component endComponent, ConnectionType type)
    {
        if (tree.componentsExists(component) && endComponent == component && tree.getConnectedTo(component).isEmpty())
        {
            hide();
            unHighlight();
        }
    }

    @Override
    public void onNotEndedNewConnection(Component startComponent, ConnectionType type)
    {
    }

    @Override
    public void onEndedNewConnection(Component startComponent, ConnectionType type, Component endComponent)
    {
        if (endComponent == component)
        {
            show();
            highlight();
        }
    }
}

package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;

/**
 * Created by Roman on 14.08.2016.
 */
public interface ComponentWrapperControllerFactory
{
    ComponentWrapperController create(ComponentsTree tree, ComponentsPosition componentsPosition, GridPosition gridPosition);
}

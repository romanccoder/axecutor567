package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Created by Roman on 24.07.2016.
 */
public interface DeleteComponentMessenger
{
    class DeleteEvent extends EventObject
    {
        private Component component;

        public DeleteEvent(Object source, Component component)
        {
            super(source);
            this.component = component;
        }

        public Component getComponent()
        {
            return component;
        }
    }

    interface DeleteEventListener extends EventListener
    {
        void onDelete(DeleteEvent event);
    }


    void request(Component component);

    void addListener(DeleteEventListener listener);
    void removeListener(DeleteEventListener listener);
}

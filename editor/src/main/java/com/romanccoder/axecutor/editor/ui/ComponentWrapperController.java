package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;

/**
 * Created by Roman on 10.08.2016.
 */
public interface ComponentWrapperController extends Controller
{
    void setComponent(Component component);
    Component getComponent();

    GridPosition getPosition();
}

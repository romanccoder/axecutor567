package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.ComponentType;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;

/**
 * Created by Roman on 13.08.2016.
 */
public class GridPositionModelImpl implements GridPositionModel
{
    @Override
    public Point getPoint(GridPosition gridPosition)
    {
        Point point = new Point();
        point.setX(GRID_PADDING + gridPosition.getColumn() * GRID_SPACING + gridPosition.getColumn() * CELL_SIZE);
        point.setY(GRID_PADDING + gridPosition.getRow() * GRID_SPACING + gridPosition.getRow() * CELL_SIZE);

        return point;
    }

    @Override
    public GridPosition getNearestPosition(Point point)
    {
        GridPosition leftTopPosition = new GridPosition();
        GridPosition nearestPosition = new GridPosition();

        if (point.getX() < GRID_PADDING)
        {
            leftTopPosition.setColumn(0);
            nearestPosition.setColumn(0);
        }
        else
        {
            leftTopPosition.setColumn((int) Math.floor(point.getX() - GRID_PADDING) / (GRID_SPACING + CELL_SIZE));
            nearestPosition.setColumn(leftTopPosition.getColumn());

            if (!isPointInGridCell(point, leftTopPosition))
            {
                Point leftTopPoint = getPoint(leftTopPosition);
                double distanceX =  leftTopPoint.getX() + CELL_SIZE + GRID_SPACING - point.getX();
                double distancePercent = distanceX * 100 / GRID_SPACING;

                if (distancePercent >= 50)
                    nearestPosition.setColumn(leftTopPosition.getColumn());
                else
                    nearestPosition.setColumn(leftTopPosition.getColumn() + 1);
            }

        }

        if (point.getY() < GRID_PADDING)
        {
            leftTopPosition.setRow(0);
            nearestPosition.setRow(0);
        }
        else
        {
            leftTopPosition.setRow((int) Math.floor(point.getY() - GRID_PADDING) / (GRID_SPACING + CELL_SIZE));
            nearestPosition.setRow(leftTopPosition.getRow());

            if (!isPointInGridCell(point, leftTopPosition))
            {
                Point leftTopPoint = getPoint(leftTopPosition);
                double distanceY =  leftTopPoint.getY() + CELL_SIZE + GRID_SPACING - point.getY();
                double distancePercent = distanceY * 100 / GRID_SPACING;

                if (distancePercent >= 50)
                    nearestPosition.setRow(leftTopPosition.getRow());
                else
                    nearestPosition.setRow(leftTopPosition.getRow() + 1);
            }
        }


        return nearestPosition;
    }

    @Override
    public boolean isPointInGridCell(Point point, GridPosition position)
    {
        Point positionPoint = getPoint(position);
        double startX = positionPoint.getX();
        double endX = positionPoint.getX() + CELL_SIZE;
        double startY = positionPoint.getY();
        double endY = positionPoint.getY() + CELL_SIZE;

        return point.getX() >= startX && point.getX() <= endX && point.getY() >= startY && point.getY() <= endY;
    }

    @Override
    public double getHCellGap(ComponentType type)
    {
        if (type == ComponentType.STANDALONE)
            return (CELL_SIZE - StandaloneComponentModel.WIDTH) / 2;
        else
            return (CELL_SIZE - CoreComponentModel.WIDTH) / 2;
    }

    @Override
    public double getVCellGap(ComponentType type)
    {
        if (type == ComponentType.STANDALONE)
            return (CELL_SIZE - StandaloneComponentModel.HEIGHT) / 2;
        else
            return (CELL_SIZE - CoreComponentModel.HEIGHT) / 2;
    }
}

package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;

/**
 * Created by Roman on 07.07.2016.
 */
public interface TitledListController extends Controller
{
    TitledListService getService();
}

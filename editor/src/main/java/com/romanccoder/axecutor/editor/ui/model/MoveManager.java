package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.GridPosition;

import java.util.Observable;

/**
 * Created by Roman on 31.07.2016.
 */
public interface MoveManager
{
    interface MoveCallback
    {
        void onMoveStarted(MoveModel model);
        void onMove(MoveModel model);
        void onMoveNotEnded(MoveModel model);
    }

    void start(ComponentsTree tree, Component component);
    MoveModel getCurrent();
    boolean hasStarted();
    void moveTo(GridPosition position);
    void end();

    void addCallback(MoveCallback callback);
    void removeCallback(MoveCallback callback);
}

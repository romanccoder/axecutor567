package com.romanccoder.axecutor.editor.ui.model;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.App;
import com.romanccoder.axecutor.core.util.validation.FileNotExists;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import org.hibernate.validator.constraints.NotBlank;

import java.io.File;

/**
 * Created by Roman on 07.07.2016.
 */
public class CreateProjectModelImpl extends CreateProjectModel
{
    private StringProperty nameProperty;
    private StringProperty basePathProperty;
    private StringProperty fullPathProperty;

    @Inject
    public CreateProjectModelImpl(
            App app
    )
    {
        nameProperty = new SimpleStringProperty(this, "name");
        basePathProperty = new SimpleStringProperty(this, "basePath", app.getDocumentsRoot().getAbsolutePath());
        fullPathProperty = new SimpleStringProperty(this, "fullPath");
        fullPathProperty.bind(
                Bindings.concat(basePathProperty,
                        File.separator,
                        nameProperty,
                        '.',
                        app.getProjectExtension()
                )
        );

        ChangeListener changeListener = (observable, oldValue, newValue) -> {
            setChanged();
            notifyObservers();
        };

        nameProperty.addListener(changeListener);
        basePathProperty.addListener(changeListener);
    }

    @Override
    public StringProperty nameProperty()
    {
        return nameProperty;
    }

    @Override
    public void setName(String name)
    {
        this.nameProperty.set(name);
    }

    @Override
    @NotBlank
    public String getName()
    {
        return nameProperty.get();
    }

    @Override
    public StringProperty basePathProperty()
    {
        return basePathProperty;
    }

    @Override
    public void setBasePath(String basePath)
    {
        this.basePathProperty.set(basePath);
    }

    @Override
    public String getBasePath()
    {
        return basePathProperty.get();
    }

    @Override
    public StringProperty fullPathProperty()
    {
        return fullPathProperty;
    }

    @Override
    @FileNotExists
    public String getFullPath()
    {
        return fullPathProperty.get();
    }
}

package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.editor.ui.model.ConnectionPoints;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 13.08.2016.
 */
public class ConnectionPointsMessengerImpl implements ConnectionPointsMessenger
{
    private List<ConnectionPointsListener> listenerList;

    public ConnectionPointsMessengerImpl()
    {
        listenerList = new ArrayList<>();
    }

    @Override
    public ConnectionPoints request(Component component)
    {
        for (ConnectionPointsListener listener : listenerList)
        {
            ConnectionPoints points = listener.onPointsRequested(component);

            if (points != null)
                return points;
        }

        return null;
    }

    @Override
    public void addListener(ConnectionPointsListener listener)
    {
        listenerList.add(listener);
    }

    @Override
    public void removeListener(ConnectionPointsListener listener)
    {
        listenerList.remove(listener);
    }
}

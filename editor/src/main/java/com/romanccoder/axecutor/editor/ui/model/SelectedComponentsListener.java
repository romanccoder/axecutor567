package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;

import java.util.List;

/**
 * Created by roman on 10/5/16.
 */
public interface SelectedComponentsListener
{
    void onClearSelection(List<Component> componentsList);
    void onSelect(List<Component> componentsList);
}

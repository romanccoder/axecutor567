package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.GridPosition;

/**
 * Created by roman on 9/7/16.
 */
public interface CopyModel
{
    interface CopyListener
    {
        void onCopy(ComponentsTree tree, GridPosition position, Component component);
    }

    void copy(ComponentsTree tree,GridPosition position, Component component);

    void addListener(CopyListener listener);
    void removeListener(CopyListener listener);
}

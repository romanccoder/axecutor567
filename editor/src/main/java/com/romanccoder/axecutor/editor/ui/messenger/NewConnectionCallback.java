package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ConnectionType;

/**
 * Created by Roman on 04.08.2016.
 */
public interface NewConnectionCallback
{
    void onStartedNewConnection(Component startComponent, ConnectionType type);
    void onNotEndedNewConnection(Component startComponent, ConnectionType type);
    void onEndedNewConnection(Component startComponent, ConnectionType type, Component endComponent);
}

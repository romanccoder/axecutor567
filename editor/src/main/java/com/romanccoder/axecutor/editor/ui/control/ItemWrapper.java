package com.romanccoder.axecutor.editor.ui.control;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Created by Roman on 01.08.2016.
 */
public class ItemWrapper extends BorderPane
{
    public void setWrapped(Node node)
    {
        removeWrapped();

        setCenter(node);
    }

    public Node getWrapped()
    {
        if (hasWrapped())
            return getCenter();

        return null;
    }

    public void removeWrapped()
    {
        setCenter(null);
    }

    public void swapWrapped(ItemWrapper wrapper)
    {
        if (!hasWrapped() && !wrapper.hasWrapped())
            return;

        if (hasWrapped() && wrapper.hasWrapped())
        {
            Node temp = wrapper.getWrapped();
            wrapper.setWrapped(getWrapped());
            setWrapped(temp);
        }
        else if (hasWrapped() && !wrapper.hasWrapped())
        {
            wrapper.setWrapped(getWrapped());
            removeWrapped();
        }
        else
        {
            setWrapped(wrapper.getWrapped());
            wrapper.removeWrapped();
        }
    }

    public void showWrapped()
    {
        if (hasWrapped())
            getWrapped().setVisible(true);
    }

    public void hideWrapped()
    {
        if (hasWrapped())
            getWrapped().setVisible(false);
    }

    public boolean hasWrapped()
    {
        return getCenter() != null;
    }
}

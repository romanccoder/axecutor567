package com.romanccoder.axecutor.editor.ui.old2;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.project.GridPosition;
import com.romanccoder.axecutor.core.ui.Point;
import com.romanccoder.axecutor.editor.ui.control.ItemWrapper;
import javafx.scene.shape.Path;

/**
 * Created by Roman on 04.08.2016.
 */
public interface ConnectionLineController extends Controller
{
    double ARROW_ANGLE  = 30;
    double ARROW_LENGTH = 10;

    void setStart(Component component, Point[] points);
    void setStart(double x, double y);
    void setType(ConnectionType type);
    void setEnd(Component component, Point[] points);
    void setEnd(double x, double y);
    void clear();

    Path getPath();
}

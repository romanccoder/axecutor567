package com.romanccoder.axecutor.editor.ui.model;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.GridPosition;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Roman on 01.08.2016.
 */
public class MoveManagerImpl implements MoveManager
{
    private List<MoveCallback> callbackList;

    @Inject
    private Provider<MoveModel> moveModelProvider;
    @Inject
    private MoveModel currentMoveModel;

    public MoveManagerImpl()
    {
        callbackList = new CopyOnWriteArrayList<>();
    }

    @Override
    public void start(ComponentsTree tree, Component component)
    {
        currentMoveModel = moveModelProvider.get();
        currentMoveModel.setCallbackList(callbackList);
        currentMoveModel.setTree(tree);
        currentMoveModel.setStartComponent(component);
    }

    @Override
    public MoveModel getCurrent()
    {
        return currentMoveModel;
    }

    @Override
    public boolean hasStarted()
    {
        return currentMoveModel != null;
    }

    @Override
    public void moveTo(GridPosition position)
    {
        if (currentMoveModel == null)
            throw new IllegalStateException();

        currentMoveModel.setEndPosition(position);
    }

    @Override
    public void end()
    {
        if (currentMoveModel == null)
            throw new IllegalStateException();

        currentMoveModel.end();
    }

    @Override
    public void addCallback(MoveCallback callback)
    {
        callbackList.add(callback);
    }

    @Override
    public void removeCallback(MoveCallback callback)
    {
        callbackList.remove(callback);
    }
}

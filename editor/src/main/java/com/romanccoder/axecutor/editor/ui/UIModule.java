package com.romanccoder.axecutor.editor.ui;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.romanccoder.axecutor.editor.ui.messenger.*;
import com.romanccoder.axecutor.editor.ui.model.*;
import com.romanccoder.axecutor.editor.ui.renderer.ComponentRenderer;

/**
 * Created by Roman on 07.07.2016.
 */
public class UIModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(CreateProjectModel.class).to(CreateProjectModelImpl.class);

        bind(ControllerMediator.class).to(ControllerMediatorImpl.class).in(Singleton.class);
        bind(MainController.class).to(MainControllerImpl.class);

        bind(CreateProjectController.class).to(CreateProjectControllerImpl.class);

        bind(SettingsController.class).to(SettingsControllerImpl.class);

        bind(ComponentsListController.class).to(ComponentsListControllerImpl.class);
        bind(ComponentRenderer.class);
        bind(ComponentItemController.class).to(ComponentItemControllerImpl.class);
        bind(TitledListController.class).to(TitledListControllerImpl.class);
        bind(TitledListService.class).to(TitledListServiceImpl.class);
        bind(TitledPaneBuilder.class).to(TitledPaneBuilderImpl.class);

        bind(ErrorController.class).to(ErrorControllerImpl.class);

        bind(PropertiesController.class).to(PropertiesControllerImpl.class);

        install(
                new FactoryModuleBuilder().implement(InputConnectionController.class, InputConnectionControllerImpl.class).
                        build(InputConnectionControllerFactory.class)
        );
        install(
                new FactoryModuleBuilder().implement(GreenOutputController.class, GreenOutputControllerImpl.class).
                        build(GreenOutputControllerFactory.class)
        );
        install(
                new FactoryModuleBuilder().implement(RedOutputController.class, RedOutputControllerImpl.class).
                        build(RedOutputControllerFactory.class)
        );

        bind(ActiveProjectsModel.class).to(ActiveProjectsModelImpl.class).in(Singleton.class);
        bind(GridPositionModel.class).to(GridPositionModelImpl.class).in(Singleton.class);
        bind(NewConnectionStartPointModel.class).to(NewConnectionStartPointModelImpl.class).in(Singleton.class);
        bind(MoveModel.class).to(MoveModelImpl.class);
        bind(MoveManager.class).to(MoveManagerImpl.class).in(Singleton.class);
        bind(CopyModel.class).to(CopyModelImpl.class).in(Singleton.class);

        install(
                new FactoryModuleBuilder().implement(ComponentWrapperController.class, ComponentWrapperControllerImpl.class)
                        .build(ComponentWrapperControllerFactory.class)
        );


        install(
                new FactoryModuleBuilder().implement(StandaloneComponentController.class, StandaloneComponentControllerImpl.class)
                        .build(StandaloneComponentControllerFactory.class)
        );
        install(
                new FactoryModuleBuilder().implement(StandaloneConnectionPoints.class, StandaloneConnectionPointsImpl.class)
                        .build(StandaloneConnectionPointsFactory.class)
        );

        install(
                new FactoryModuleBuilder().implement(ConnectionLineController.class, ConnectionLineControllerImpl.class)
                        .build(ConnectionLineControllerFactory.class)
        );

        bind(ProjectsController.class).to(ProjectsControllerImpl.class);
        install(
                new FactoryModuleBuilder().implement(ComponentsGridController.class, ComponentsGridControllerImpl.class)
                .build(ComponentsGridControllerFactory.class)
        );

        install(
                new FactoryModuleBuilder().implement(CoreComponentController.class, CoreComponentControllerImpl.class)
                        .build(CoreComponentControllerFactory.class)
        );
        bind(CoreConnectionPoints.class).to(CoreConnectionPointsImpl.class);

        bind(SelectedComponentsModel.class).to(SelectedComponentsModelImpl.class).in(Singleton.class);

        bind(NewConnectionMessenger.class).to(NewConnectionMessengerImpl.class).in(Singleton.class);

        bind(DeleteComponentMessenger.class).to(DeleteComponentMessengerImpl.class).in(Singleton.class);

        bind(ComponentPositionMessenger.class).to(ComponentPositionMessengerImpl.class).in(Singleton.class);
        bind(RemoveConnectionMessenger.class).to(RemoveConnectionMessengerImpl.class).in(Singleton.class);
        bind(RemoveComponentMessenger.class).to(RemoveComponentMessengerImpl.class).in(Singleton.class);
        bind(ConnectionPointsMessenger.class).to(ConnectionPointsMessengerImpl.class).in(Singleton.class);

        bind(MoveManager.class).to(MoveManagerImpl.class).in(Singleton.class);

        bind(DropModel.class).to(DropModelImpl.class).in(Singleton.class);
        bind(DragModel.class).to(DragModelImpl.class).in(Singleton.class);
    }
}

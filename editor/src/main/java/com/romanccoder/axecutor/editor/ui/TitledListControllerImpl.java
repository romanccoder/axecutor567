package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Roman on 07.07.2016.
 */
public class TitledListControllerImpl implements TitledListController
{
    private TitledListService service;

    private VBox box;

    @Inject
    public TitledListControllerImpl(TitledListService service)
    {
        this.service = service;
        box = new VBox(5);
        drawItems();
        service.addObserver((o, arg) -> drawItems()); // TODO Optimize
    }

    private void drawItems()
    {
        box.getChildren().clear();

        service.getAll().stream().forEach(model -> box.getChildren().add(model.getPane()));
    }

    @Override
    public TitledListService getService()
    {
        return service;
    }

    @Override
    public Parent getView()
    {
        return box;
    }
}

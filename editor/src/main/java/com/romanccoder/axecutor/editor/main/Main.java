package com.romanccoder.axecutor.editor.main;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.romanccoder.axecutor.core.component.*;
import com.romanccoder.axecutor.core.project.Project;
import com.romanccoder.axecutor.core.project.ProjectSerializer;
import com.romanccoder.axecutor.core.util.MaterialTheme;
import com.romanccoder.axecutor.editor.EditorModule;
import com.romanccoder.axecutor.editor.ui.MainController;
import com.romanccoder.axecutor.editor.ui.model.SelectedComponentsModel;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 * Created by Roman on 16.06.2016.
 */
public class Main extends Application
{
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Injector injector = Guice.createInjector(new EditorModule());

        Scene scene = new Scene(injector.getInstance(MainController.class).getView());
        MaterialTheme.applyStyle(scene);

        primaryStage.setTitle("Axecutor Editor");
        primaryStage.setMaximized(true);
        primaryStage.setScene(scene);

        scene.setOnKeyPressed(event ->
        {
            if (event.getCode() == KeyCode.ESCAPE)
                injector.getInstance(SelectedComponentsModel.class).clearSelected();
        });

        /*ComponentsTree tree = injector.getInstance(ComponentsTree.class);

        Component startComponent = injector.getInstance(ComponentsFactory.class).create("start");
        Component variableComponent = injector.getInstance(ComponentsFactory.class).create("variable");

        tree.addComponent(startComponent);
        tree.addComponent(variableComponent);
        tree.getDescriptor(startComponent).setGreenTarget(variableComponent);

        Project project = injector.getInstance(Project.class);
        project.setName("name");
        project.setTree(tree);

        ProjectSerializer serializer = injector.getInstance(ProjectSerializer.class);
        serializer.setProject(project);

        System.out.println(serializer.serialize());*/

        //SecretKey key = new SecretKeySpec("qkjll5@2md3gs5Q@".getBytes("UTF-8"), "AES");
        //Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");



        primaryStage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}

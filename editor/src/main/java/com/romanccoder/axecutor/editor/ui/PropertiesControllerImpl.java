package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsFactory;
import com.romanccoder.axecutor.core.component.builder.PropertiesBuilder;
import com.romanccoder.axecutor.core.component.builder.PropertiesBuilderFactory;
import javafx.scene.Parent;

/**
 * Created by Roman on 15.07.2016.
 */
public class PropertiesControllerImpl implements PropertiesController
{
    private Parent root;

    //private PropertiesBuilderFactory builderFactory;
    private ComponentsFactory componentsFactory;



    @Inject
    public PropertiesControllerImpl(
            //PropertiesBuilderFactory builderFactory,
            ComponentsFactory componentsFactory
    )
    {
        //this.builderFactory = builderFactory;
        this.componentsFactory = componentsFactory;

        Component component = componentsFactory.create("variable");

    }

    @Override
    public Parent getView()
    {
        return root;
    }
}

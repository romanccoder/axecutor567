package com.romanccoder.axecutor.editor;

import com.google.inject.AbstractModule;
import com.romanccoder.axecutor.core.CoreModule;
import com.romanccoder.axecutor.editor.ui.UIModule;
import com.romanccoder.axecutor.editor.util.UtilModule;
import com.romanccoder.axecutor.editor.main.MainModule;

/**
 * Created by Roman on 06.07.2016.
 */
public class EditorModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        install(new CoreModule());
        install(new MainModule());
        install(new UIModule());
        install(new UtilModule());
    }
}

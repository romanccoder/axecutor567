package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.editor.main.Editor;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;

/**
 * Created by Roman on 07.07.2016.
 */
public class ComponentItemControllerImpl extends AbstractController implements ComponentItemController
{
    @FXML
    private HBox rootBox;

    @FXML
    private ImageView iconImageView;

    @FXML
    private Label titleLabel;

    private Component component;

    @Inject
    public ComponentItemControllerImpl(
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        rootBox.setOnDragDetected(
            event ->
            {
                Dragboard db = rootBox.startDragAndDrop(TransferMode.COPY);

                if (component.getIcon() != null)
                    db.setDragView(new Image(component.getIcon().toExternalForm()));

                ClipboardContent content = new ClipboardContent();
                content.putString(component.getId());
                db.setContent(content);

                event.consume();
            }
        );
    }

    @Override
    public void setComponent(Component component)
    {
        this.component = component;

        if (component.getIcon() != null)
            iconImageView.setImage(new Image(component.getIcon().toExternalForm()));
        titleLabel.setText(component.getLabel());
    }

    @Override
    public Parent getView()
    {
        return rootBox;
    }
}

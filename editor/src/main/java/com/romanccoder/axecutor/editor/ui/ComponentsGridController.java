package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by Roman on 10.08.2016.
 */
public interface ComponentsGridController extends Controller
{
    int DEFAULT_CELL_INCREASE = 15;
}

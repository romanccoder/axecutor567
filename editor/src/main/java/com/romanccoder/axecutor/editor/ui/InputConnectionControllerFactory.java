package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;

/**
 * Created by roman on 9/27/16.
 */
public interface InputConnectionControllerFactory
{
    InputConnectionController create(Component component, ComponentsTree tree);
}

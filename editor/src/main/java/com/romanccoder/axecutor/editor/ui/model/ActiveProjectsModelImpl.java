package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.project.Project;
import javafx.collections.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Roman on 17.07.2016.
 */
public class ActiveProjectsModelImpl implements ActiveProjectsModel
{
    private ObservableSet<Project> projectsSet;

    public ActiveProjectsModelImpl()
    {
        projectsSet = FXCollections.observableSet();
    }

    @Override
    public void addProject(Project project)
    {
        projectsSet.add(project);
    }

    @Override
    public void removeProject(Project project)
    {
        projectsSet.remove(project);
    }

    @Override
    public void addListener(SetChangeListener<Project> listener)
    {
        projectsSet.addListener(listener);
    }

    @Override
    public void removeListener(SetChangeListener<Project> listener)
    {
        projectsSet.removeListener(listener);
    }
}

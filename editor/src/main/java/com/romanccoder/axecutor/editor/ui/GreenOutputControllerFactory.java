package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by roman on 9/27/16.
 */
public interface GreenOutputControllerFactory
{
    GreenOutputController create(Component component, ComponentsTree tree, ComponentsPosition componentsPosition);
}

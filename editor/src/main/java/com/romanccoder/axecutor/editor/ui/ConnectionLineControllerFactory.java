package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by Roman on 20.08.2016.
 */
public interface ConnectionLineControllerFactory
{
    ConnectionLineController create(ComponentsTree tree, ComponentsPosition componentsPosition);
}

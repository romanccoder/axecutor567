package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.project.ComponentsPosition;
import com.romanccoder.axecutor.core.project.GridPosition;
import javafx.scene.shape.Shape;

/**
 * Created by Roman on 10.08.2016.
 */
public interface ComponentController extends Controller
{
    Shape getShape();
}

package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 * Created by roman on 9/26/16.
 */
public interface InputConnectionController extends ConnectionController
{
    void applyDragHandlers(Node node);
}

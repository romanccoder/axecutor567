package com.romanccoder.axecutor.editor.ui.old2;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.ConnectionType;
import com.romanccoder.axecutor.core.ui.Point;

/**
 * Created by Roman on 31.07.2016.
 */
public interface VisualComponentController extends Controller
{
    void setComponent(Component component);
    Component getComponent();

    void setDescriptor(ComponentDescriptor descriptor);
    ComponentDescriptor getDescriptor();

    Point[] getOutputPoints(ConnectionType type);
    Point[] getInputPoints(ConnectionType type);
}

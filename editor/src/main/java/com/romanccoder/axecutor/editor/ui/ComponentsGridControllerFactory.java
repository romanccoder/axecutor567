package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.ComponentsPosition;

/**
 * Created by Roman on 14.08.2016.
 */
public interface ComponentsGridControllerFactory
{
    ComponentsGridController create(ComponentsTree tree, ComponentsPosition componentsPosition);
}

package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by Roman on 07.07.2016.
 */
public interface ComponentItemController extends Controller
{
    void setComponent(Component component);
}

package com.romanccoder.axecutor.editor.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.base.AbstractController;
import com.romanccoder.axecutor.core.locale.LocaleService;
import com.romanccoder.axecutor.core.locale.LocaleWrapper;
import com.romanccoder.axecutor.core.settings.Settings;
import com.romanccoder.axecutor.core.settings.SettingsException;
import com.romanccoder.axecutor.core.settings.SettingsService;
import com.romanccoder.axecutor.core.util.MaterialTheme;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import com.romanccoder.axecutor.core.util.validation.ViewValidationUtil;
import com.romanccoder.axecutor.editor.main.Editor;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.Optional;

/**
 * Created by Roman on 07.07.2016.
 */
public class SettingsControllerImpl extends AbstractController implements SettingsController
{
    private class Converter extends StringConverter<Double>
    {
        @Override
        public String toString(Double value)
        {
            return value != Settings.TIMEOUT_NO_LIMIT ? String.valueOf(value.intValue()) : "No limit";
        }

        @Override
        public Double fromString(String str)
        {
            return str.equals("No limit") ? 65 : Double.valueOf(str);
        }
    }

    @FXML
    private TabPane rootTabPane;

    @FXML
    private ComboBox<LocaleWrapper> languageComboBox;

    @FXML
    private Slider commandTimeoutSlider;

    @FXML
    private Slider ftpTimeoutSlider;

    private Settings model;
    private SettingsService settingsService;

    private LocaleService localeService;

    private Provider<ErrorController> errorControllerProvider;

    private Dialog<ButtonType> dialog;

    @Inject
    public SettingsControllerImpl(
            @Editor TranslationProvider translationProvider,
            @Editor ResourceProvider resourceProvider,
            Settings settingsModel,
            SettingsService settingsService,
            LocaleService localeService,
            Provider<ErrorController> errorControllerProvider
    )
    {
        setTranslationProvider(translationProvider);
        setResourceProvider(resourceProvider);
        loadView();

        this.model = settingsModel;
        this.settingsService = settingsService;
        this.errorControllerProvider = errorControllerProvider;
        this.localeService = localeService;

        try
        {
            settingsService.refresh();
        }
        catch (SettingsException e)
        {
            ErrorController errorController = errorControllerProvider.get();
            errorController.setException(e);
            errorController.showDialog();
        }

        settingsModel.copy(settingsService.getModel());

        initUI();
        initUIByModel();
        bindModel();
    }

    private void initUI()
    {
        languageComboBox.getItems().setAll(localeService.getAll());
        commandTimeoutSlider.setLabelFormatter(new Converter());
        commandTimeoutSlider.setMin(Settings.MIN_TIMEOUT);
        commandTimeoutSlider.setMax(Settings.MAX_TIMEOUT);
        ftpTimeoutSlider.setLabelFormatter(new Converter());
        ftpTimeoutSlider.setMin(Settings.MIN_TIMEOUT);
        ftpTimeoutSlider.setMax(Settings.MAX_TIMEOUT);

        dialog = new Dialog<>();
        MaterialTheme.applyStyle(dialog);
        dialog.setTitle(getTranslationProvider().get("com.romanccoder.axecutor.editor.ui.SettingsControllerImpl.title"));
        dialog.setGraphic(new ImageView(getResourceProvider().getImage("settings_64.png").toExternalForm()));
        dialog.setHeaderText(getTranslationProvider().get("com.romanccoder.axecutor.editor.ui.SettingsControllerImpl.header_text"));
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getResourceProvider().getImage("settings_64.png").toString()));
        dialog.getDialogPane().setContent(rootTabPane);
        dialog.getDialogPane().setPrefHeight(600);
        dialog.getDialogPane().setPrefWidth(600);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
    }

    @Override
    public void showDialog()
    {
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == ButtonType.OK)
        {
            try
            {
                settingsService.setModel(model);
                settingsService.save();
            }
            catch (SettingsException e)
            {
                ErrorController errorController = errorControllerProvider.get();
                errorController.setException(e);
                errorController.showDialog();
            }

        }
    }

    private void bindModel()
    {
        model.activeLocaleProperty().bind(languageComboBox.getSelectionModel().selectedItemProperty());
        model.commandTimeoutProperty().bind(commandTimeoutSlider.valueProperty());
        model.ftpOperationTimeoutProperty().bind(ftpTimeoutSlider.valueProperty());
    }

    private void initUIByModel()
    {
        languageComboBox.getSelectionModel().select(model.getActiveLocale());
        commandTimeoutSlider.setValue(model.getCommandTimeout());
        ftpTimeoutSlider.setValue(model.getFtpOperationTimeout());
    }

    @Override
    public Parent getView()
    {
        return rootTabPane;
    }
}

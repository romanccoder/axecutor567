package com.romanccoder.axecutor.editor.ui.messenger;

import com.romanccoder.axecutor.core.component.Component;

import javax.swing.event.EventListenerList;

/**
 * Created by Roman on 25.07.2016.
 */
public class DeleteComponentMessengerImpl implements DeleteComponentMessenger
{
    private EventListenerList listenerList;

    public DeleteComponentMessengerImpl()
    {
        listenerList = new EventListenerList();
    }

    @Override
    public void request(Component component)
    {
        for (DeleteEventListener listener : listenerList.getListeners(DeleteEventListener.class))
            listener.onDelete(new DeleteEvent(this, component));
    }

    @Override
    public void addListener(DeleteEventListener listener)
    {
        listenerList.add(DeleteEventListener.class, listener);
    }

    @Override
    public void removeListener(DeleteEventListener listener)
    {
        listenerList.remove(DeleteEventListener.class, listener);
    }
}

package com.romanccoder.axecutor.editor.ui;

import java.util.List;
import java.util.Observable;

/**
 * Created by Roman on 07.07.2016.
 */
public abstract class TitledListService extends Observable
{
    public abstract void add(TitledPaneBuilder model);
    public abstract boolean remove(TitledPaneBuilder model);
    public abstract TitledPaneBuilder getByAssociated(Object associatedObject);
    public abstract List<TitledPaneBuilder> getAll();
}

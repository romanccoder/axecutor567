package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.ui.Point;

import java.util.Observable;

/**
 * Created by Roman on 13.08.2016.
 */
public abstract class ConnectionPoints extends Observable
{
    public enum ConnectionPointsLocation
    {
        // Input points
        INPUT_TOP,
        INPUT_RIGHT,
        INPUT_BOTTOM,
        INPUT_LEFT,

        // Green output points
        OUTPUT_GREEN_TOP,
        OUTPUT_GREEN_RIGHT,
        OUTPUT_GREEN_BOTTOM,
        OUTPUT_GREEN_LEFT,

        // Red output points
        OUTPUT_RED_RIGHT,
        OUTPUT_RED_LEFT,
    }

    public abstract Point getPoint(ConnectionPointsLocation location);
}

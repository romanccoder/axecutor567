package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.project.GridPosition;

/**
 * Created by Roman on 01.08.2016.
 */
public interface DropModel
{
    void setPosition(GridPosition position);
    GridPosition getPosition();
    boolean hasPosition();
}

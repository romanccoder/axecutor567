package com.romanccoder.axecutor.editor.ui.model;

import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentsTree;
import com.romanccoder.axecutor.core.project.GridPosition;

import java.util.List;
import java.util.ListIterator;

/**
 * Created by Roman on 11.08.2016.
 */
public class MoveModelImpl extends MoveModel
{
    private ComponentsTree tree;
    private Component startComponent;
    private GridPosition endPosition;

    private List<MoveManager.MoveCallback> callbackList;

    @Override
    protected void setCallbackList(List<MoveManager.MoveCallback> callbackList)
    {
        this.callbackList = callbackList;
    }

    @Override
    protected void setTree(ComponentsTree tree)
    {
        this.tree = tree;
    }

    @Override
    public ComponentsTree getTree()
    {
        return tree;
    }

    @Override
    protected void setStartComponent(Component startComponent)
    {
        this.startComponent = startComponent;

        if (callbackList == null)
            throw new IllegalStateException("Callback list hasn't been set");


        for (MoveManager.MoveCallback callback : callbackList)
            callback.onMoveStarted(this);
    }

    @Override
    public Component getStartComponent()
    {
        return startComponent;
    }

    @Override
    protected void setEndPosition(GridPosition gridPosition)
    {
        this.endPosition = gridPosition;

        if (callbackList == null)
            throw new IllegalStateException("Callback list hasn't been set");


        for (MoveManager.MoveCallback callback : callbackList)
            callback.onMove(this);
    }

    @Override
    protected void end()
    {
        if (callbackList == null)
            throw new IllegalStateException("Callback list hasn't been set");


        for (MoveManager.MoveCallback callback : callbackList)
            callback.onMoveNotEnded(this);
    }

    @Override
    public GridPosition getEndPosition()
    {
        return endPosition;
    }
}

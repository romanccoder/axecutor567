package com.romanccoder.axecutor.editor.ui;

import com.romanccoder.axecutor.core.base.Controller;
import com.romanccoder.axecutor.core.component.Component;
import javafx.scene.Node;
import javafx.scene.shape.Shape;

/**
 * Created by roman on 9/26/16.
 */
public interface ConnectionController extends Controller
{
    String CONNECTION_ENDED = "connection ended";

    void applyMouseHandlers(Node... nodes);

    Shape getShape();
}

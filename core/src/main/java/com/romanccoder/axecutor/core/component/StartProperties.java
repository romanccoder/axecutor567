package com.romanccoder.axecutor.core.component;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by roman on 9/29/16.
 */
public class StartProperties implements Properties
{
    private StringProperty shortDescriptionProperty;

    public StartProperties()
    {
        shortDescriptionProperty = new SimpleStringProperty("Start");
    }

    @Override
    public StringProperty shortDescriptionProperty()
    {
        return shortDescriptionProperty;
    }

    @Override
    public String getShortDescription()
    {
        return shortDescriptionProperty.get();
    }
}

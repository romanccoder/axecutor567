package com.romanccoder.axecutor.core.project;

import com.romanccoder.axecutor.core.component.ComponentsTree;

/**
 * Created by Roman on 11.07.2016.
 */
public class ProjectImpl implements Project
{
    private String name;
    private ComponentsTree tree;
    private ComponentsPosition componentsPosition;

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void setTree(ComponentsTree tree)
    {
        this.tree = tree;
    }

    @Override
    public ComponentsTree getTree()
    {
        return tree;
    }

    @Override
    public void setPositions(ComponentsPosition componentsPosition)
    {
        this.componentsPosition = componentsPosition;
    }

    @Override
    public ComponentsPosition getPositions()
    {
        return componentsPosition;
    }
}

package com.romanccoder.axecutor.core.util;

import java.net.URL;

/**
 * Created by Roman on 06.07.2016.
 */
public class CoreResourceProvider implements ResourceProvider
{
    @Override
    public String getBasePath()
    {
        return "/com/romanccoder/axecutor/core";
    }
}

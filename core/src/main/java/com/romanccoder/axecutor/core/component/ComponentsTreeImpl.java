package com.romanccoder.axecutor.core.component;

import com.google.inject.Inject;

import java.util.*;

/**
 * Created by Roman on 13.07.2016.
 */
public class ComponentsTreeImpl implements ComponentsTree
{
    private Map<Component, ComponentDescriptor> componentsMap;

    private List<TreeChangedListener> listenerList;

    @Inject
    private ComponentDescriptorFactory descriptorFactory;

    public ComponentsTreeImpl()
    {
        componentsMap = new HashMap<>();
        listenerList = new ArrayList<>();
    }

    @Override
    public void addComponent(Component component)
    {
        if (!isAddAllowed(component))
            throw new CoreComponentExistsException(component);

        if (!componentsMap.containsKey(component))
        {
            ComponentDescriptor descriptor = descriptorFactory.create(component);
            componentsMap.put(component, descriptor);

            listenerList.stream().forEach(listener -> listener.onComponentAdded(component));
        }
    }

    private boolean isAddAllowed(Component component)
    {
        if (component.getType() != ComponentType.CORE)
            return true;

        for (Component currentComponent : getComponentsSet())
        {
            if (currentComponent.getId().equals(component.getId()))
                return false;
        }

        return true;
    }

    @Override
    public void removeComponent(Component component)
    {
        if (componentsMap.containsKey(component))
        {
            //TODO check if someone is connected to this component
            componentsMap.remove(component);

            listenerList.stream().forEach(listener -> listener.onComponentRemoved(component));
        }
    }

    @Override
    public boolean componentsExists(Component component)
    {
        return componentsMap.containsKey(component);
    }

    @Override
    public ComponentDescriptor getDescriptor(Component component)
    {
        if (!componentsMap.containsKey(component))
            throw new NoSuchElementException("No such component in tree: " + component);

        return componentsMap.get(component);
    }

    @Override
    public Map<Component, ComponentDescriptor> getComponentsMap()
    {
        return componentsMap;
    }

    @Override
    public Set<Component> getComponentsSet()
    {
        return componentsMap.keySet();
    }

    @Override
    public List<ComponentDescriptor> getDescriptorsList()
    {
        return new ArrayList<>(componentsMap.values());
    }

    @Override
    public List<Component> getConnectedTo(Component component)
    {
        List<Component> componentList = new ArrayList<>();

        for (Map.Entry<Component, ComponentDescriptor> entry : componentsMap.entrySet())
        {
            ComponentDescriptor descriptor = entry.getValue();
            if (descriptor.getGreenTarget() == component || descriptor.getRedTarget() == component)
                componentList.add(entry.getKey());
        }

        return componentList;
    }

    @Override
    public void addListener(TreeChangedListener listener)
    {
        listenerList.add(listener);
    }

    @Override
    public void removeListener(TreeChangedListener listener)
    {
        listenerList.remove(listener);
    }
}

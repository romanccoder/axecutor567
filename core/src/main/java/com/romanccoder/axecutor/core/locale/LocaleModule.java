package com.romanccoder.axecutor.core.locale;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;

/**
 * Created by Roman on 06.07.2016.
 */
public class LocaleModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        install(
                new FactoryModuleBuilder()
                .implement(LocaleWrapper.class, LocaleWrapperImpl.class)
                .build(LocaleFactory.class)
        );
        bind(LocaleService.class).to(LocaleServiceImpl.class).in(Singleton.class);
    }
}

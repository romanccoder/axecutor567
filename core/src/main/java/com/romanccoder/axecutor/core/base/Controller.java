package com.romanccoder.axecutor.core.base;

import javafx.scene.Parent;

/**
 * Created by Roman on 06.07.2016.
 */
public interface Controller
{
    Parent getView();
}

package com.romanccoder.axecutor.core.component.builder;

/**
 * Created by Roman on 14.07.2016.
 */
public @interface EnumValue
{
    String property() default "";
    String value() default "";
}

package com.romanccoder.axecutor.core.component;

import com.romanccoder.axecutor.core.base.Identifiable;
import com.romanccoder.axecutor.core.base.Simplified;
import javafx.scene.Parent;
import javafx.scene.paint.Color;

import java.io.Serializable;

/**
 * Created by Roman on 06.07.2016.
 */
public interface Component extends Identifiable<String>, Simplified
{
    ComponentType getType();
    ComponentCategory getCategory();
    Color getColor();
    Properties getProperties();
    Parent getView();
    boolean execute();
}

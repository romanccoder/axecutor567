package com.romanccoder.axecutor.core.component;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Roman on 10.07.2016.
 */
public interface ComponentsTree
{
    interface TreeChangedListener
    {
        void onComponentAdded(Component component);
        void onComponentRemoved(Component component);
    }

    void addComponent(Component component);
    void removeComponent(Component component);
    boolean componentsExists(Component component);
    ComponentDescriptor getDescriptor(Component component);

    Map<Component, ComponentDescriptor> getComponentsMap();
    Set<Component> getComponentsSet();
    List<ComponentDescriptor> getDescriptorsList();

    List<Component> getConnectedTo(Component component);

    void addListener(TreeChangedListener listener);
    void removeListener(TreeChangedListener listener);
}

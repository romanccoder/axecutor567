package com.romanccoder.axecutor.core.project;

import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by Roman on 20.07.2016.
 */
public interface ComponentsPosition
{
    interface GridPositionListener
    {
        void onPositionChanged(Component component, GridPosition position);
    }

    void set(Component component, GridPosition position);
    boolean hasSet(Component component);
    boolean hasSet(GridPosition position);
    void swap(GridPosition position1, GridPosition position2);

    void remove(Component component);

    GridPosition getPosition(Component component);
    Component getComponent(GridPosition position);

    void addListener(Component component, GridPositionListener listener);
    void removeListener(Component component, GridPositionListener listener);
}

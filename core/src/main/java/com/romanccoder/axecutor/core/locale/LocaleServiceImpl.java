package com.romanccoder.axecutor.core.locale;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.util.ResourceProvider;

import java.util.*;

/**
 * Created by Roman on 06.07.2016.
 */
public class LocaleServiceImpl implements LocaleService
{
    private List<LocaleWrapper> wrappers;

    @Inject
    public LocaleServiceImpl(LocaleFactory factory, @Core ResourceProvider resourceProvider)
    {
        wrappers = new ArrayList<>(3);
        wrappers.add(factory.create(resourceProvider.getImage("usa_flag.png"), "English", Locale.ENGLISH));
        wrappers.add(factory.create(resourceProvider.getImage("russian_flag.png"), "Russian", new Locale("Russian")));
        wrappers.add(factory.create(resourceProvider.getImage("ukrainian_flag.png"), "Ukrainian", new Locale("Ukrainian")));
    }

    @Override
    public List<LocaleWrapper> getAll()
    {
        return wrappers;
    }

    @Override
    public LocaleWrapper getById(String id)
    {
        Optional<LocaleWrapper> wrapperOptional = wrappers.stream().filter(wrapper -> wrapper.getId().equals(id)).findFirst();

        if (!wrapperOptional.isPresent())
            throw new NoSuchElementException("No such locale: " + id);

        return wrapperOptional.get();
    }

    @Override
    public boolean exists(String id)
    {
        return wrappers.stream().anyMatch(wrapper -> wrapper.getId().equals(id));
    }
}

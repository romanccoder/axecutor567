package com.romanccoder.axecutor.core.project;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.ComponentDescriptor;
import com.romanccoder.axecutor.core.component.property.PropertyHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 13.07.2016.
 */
public class ProjectSerializerImpl implements ProjectSerializer
{
    private Project project;

    private Gson gson;

    private List<Component> componentsList;

    @Override
    public void setProject(Project project)
    {
        this.project = project;

        gson = new Gson();
        componentsList = new ArrayList<>(project.getTree().getComponentsSet());
    }

    @Override
    public Project getProject()
    {
        return project;
    }

    @Override
    public String serialize()
    {
        if (getProject() == null)
            throw new IllegalStateException("Project model is null");

        JsonObject projectJson = new JsonObject();
        projectJson.addProperty(NAME_KEY, getProject().getName());

        JsonArray componentsJson = new JsonArray();
        projectJson.add(COMPONENTS_KEY, componentsJson);

        for (Component component : getProject().getTree().getComponentsSet())
        {
            componentsJson.add(serializeComponent(component));
        }

        return projectJson.toString();
    }

    private JsonObject serializeComponent(Component component)
    {
        JsonObject componentJson = new JsonObject();

        componentJson.addProperty(COMPONENT_ID_KEY, component.getId());

        JsonObject positionJson = new JsonObject();
        GridPosition position = getProject().getPositions().getPosition(component);
        positionJson.addProperty(COMPONENT_POSITION_COLUMN_KEY, position.getColumn());
        componentJson.addProperty(COMPONENT_POSITION_ROW_KEY, position.getRow());
        componentJson.add(COMPONENT_POSITION_KEY, positionJson);

        componentJson.add(COMPONENT_DESCRIPTOR_KEY, serializeDescriptor(getProject().getTree().getDescriptor(component)));

        JsonObject modelJson = new JsonObject();
        componentJson.add(COMPONENT_MODEL_KEY, modelJson);

        if (component.getProperties() != null)
        {
            for (String propertyName : PropertyHelper.getProperties(component.getProperties()))
            {
                Object propertyValue = PropertyHelper.getValue(component.getProperties(), propertyName);

                modelJson.addProperty(propertyName, gson.toJson(propertyValue));
            }
        }

        return componentJson;
    }

    private JsonObject serializeDescriptor(ComponentDescriptor descriptor)
    {
        JsonObject descriptorJson = new JsonObject();

        if (descriptor.hasGreenTarget())
            descriptorJson.addProperty(COMPONENT_DESCRIPTOR_GREEN_TARGET_KEY, componentsList.indexOf(descriptor.getGreenTarget()));
        else
            descriptorJson.add(COMPONENT_DESCRIPTOR_GREEN_TARGET_KEY, JsonNull.INSTANCE);

        if (descriptor.hasRedTarget())
            descriptorJson.addProperty(COMPONENT_DESCRIPTOR_RED_TARGET_KEY, componentsList.indexOf(descriptor.getRedTarget()));
        else
            descriptorJson.add(COMPONENT_DESCRIPTOR_RED_TARGET_KEY, JsonNull.INSTANCE);

        return descriptorJson;
    }

    @Override
    public void deserialize()
    {

    }
}

package com.romanccoder.axecutor.core.component.builder;

import javafx.scene.Parent;

import java.lang.annotation.Annotation;

/**
 * Created by Roman on 14.07.2016.
 */
public interface FieldBuilder<T extends Annotation>
{
    T getData();
    Object getProperties();
    String getAssociatedProperty();
    PropertiesBuilder getPropertiesBuilder();

    void build();

    Parent getRoot();
}

package com.romanccoder.axecutor.core.locale;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import java.net.URL;
import java.util.Locale;

/**
 * Created by Roman on 06.07.2016.
 */
public class LocaleWrapperImpl implements LocaleWrapper
{
    private URL icon;
    private String label;
    private Locale locale;

    @Inject
    public LocaleWrapperImpl(@Assisted URL icon, @Assisted String label, @Assisted Locale locale)
    {
        this.icon = icon;
        this.label = label;
        this.locale = locale;
    }

    @Override
    public Locale getLocale()
    {
        return locale;
    }

    @Override
    public String getId()
    {
        return locale.getLanguage();
    }

    @Override
    public URL getIcon()
    {
        return icon;
    }

    @Override
    public String getLabel()
    {
        return label;
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    public String toString()
    {
        return getLabel();
    }
}

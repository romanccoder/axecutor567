package com.romanccoder.axecutor.core.component;

import com.google.inject.Inject;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Roman on 06.07.2016.
 */
public class ComponentsServiceImpl implements ComponentsService
{
    private List<Component> componentsList;

    @Inject
    public ComponentsServiceImpl(ComponentsFactory factory)
    {
        componentsList = factory.createAll();
    }

    @Override
    public List<Component> getByCategory(ComponentCategory category)
    {
        return componentsList.stream().filter(component -> component.getCategory().equals(category)).collect(Collectors.toList());
    }

    @Override
    public Component getById(String id)
    {
        Optional<Component> componentOptional = componentsList.stream().filter(component -> component.getId().equals(id)).findFirst();

        if (!componentOptional.isPresent())
            throw new NoSuchElementException("No such component: " + id);

        return componentOptional.get();
    }

    @Override
    public List<Component> getAll()
    {
        return componentsList;
    }

    @Override
    public boolean exists(String id)
    {
        Optional<Component> componentOptional = componentsList.stream().filter(component -> component.getId().equals(id)).findFirst();

        return componentOptional.isPresent();
    }
}

package com.romanccoder.axecutor.core.project;

import java.io.File;

/**
 * Created by Roman on 17.07.2016.
 */
public interface ProjectServiceFactory
{
    ProjectService create(File file);
}

package com.romanccoder.axecutor.core.component.builder;

/**
 * Created by Roman on 15.07.2016.
 */
public class BuilderException extends RuntimeException
{
    public BuilderException(String message)
    {
        super(message);
    }

    public BuilderException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BuilderException(Throwable cause)
    {
        super(cause);
    }
}

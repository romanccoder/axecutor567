package com.romanccoder.axecutor.core.base;

import javax.validation.Validator;
import java.io.File;

/**
 * Created by Roman on 06.07.2016.
 */
public interface App
{
    String getName();
    String getDescription();
    File getDocumentsRoot();
    String getProjectExtension();
}

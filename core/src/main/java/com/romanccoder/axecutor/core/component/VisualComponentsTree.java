package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 20.07.2016.
 */
public interface VisualComponentsTree extends ComponentsTree
{
    VisualComponentDescriptor getDescriptor(Component component);
}

package com.romanccoder.axecutor.core.base;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

/**
 * Created by Roman on 06.07.2016.
 */
public class BaseModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(App.class).to(AppImpl.class).in(Singleton.class);
    }
}

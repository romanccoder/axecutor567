package com.romanccoder.axecutor.core.util.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.File;

/**
 * Created by Roman on 07.07.2016.
 */
public class FileNotExistsValidator implements ConstraintValidator<FileNotExists, String>
{
    @Override
    public void initialize(FileNotExists anno)
    {
    }

    @Override
    public boolean isValid(String path, ConstraintValidatorContext constraintValidatorContext)
    {
        if (new File(path).exists())
            return false;
        else
            return true;
    }
}

package com.romanccoder.axecutor.core.project;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.assistedinject.Assisted;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Roman on 11.07.2016.
 */
public class ProjectServiceImpl implements ProjectService
{
    private SecretKey secretKey;
    private Cipher cipher;

    private File file;
    private Project project;

    @Inject
    private ProjectSerializer serializer;

    @Inject
    private Provider<Project> projectProvider;

    @Inject
    public ProjectServiceImpl(@Assisted File file)
    {
        this.file = file;

        try
        {
            secretKey = new SecretKeySpec("Lh>P-@&\"h,^JLP4S".getBytes(), "AES");
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        }
        catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {

        }
    }

    @Override
    public File getFile()
    {
        return file;
    }

    @Override
    public void setProject(Project project)
    {
        this.project = project;
    }

    @Override
    public Project getProject()
    {
        return project;
    }

    private byte[] encrypt(String json)
    {
        try
        {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return cipher.doFinal(json.getBytes("UTF-8"));
        }
        catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e)
        {
            throw new ProjectException("Can't encrypt project file", e);
        }
    }

    private String decrypt(byte[] json)
    {
        try
        {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            return new String(cipher.doFinal(json));
        }
        catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e)
        {
            throw new ProjectException("Can't decrypt project file", e);
        }
    }

    @Override
    public void load()
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(getFile())))
        {
        }
        catch (IOException e)
        {
            throw new ProjectException("Project file is broken", e);
        }
    }

    @Override
    public void save()
    {
        getFile().getParentFile().mkdirs();

        try (OutputStream stream = new FileOutputStream(getFile()))
        {
            serializer.setProject(project);
            stream.write(/*encrypt(*/serializer.serialize().getBytes("UTF-8")/*)*/);
        }
        catch (IOException e)
        {
            throw new ProjectException(e);
        }
    }
}

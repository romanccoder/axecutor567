package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 10.07.2016.
 */
public interface ComponentDescriptor
{
    interface DescriptorChangedListener
    {
        void onDescriptorChanged(ComponentDescriptor descriptor, ConnectionType type, Component previousTarget);
    }

    Component getSource();

    void setGreenTarget(Component target);
    Component getGreenTarget();

    void setRedTarget(Component target);
    Component getRedTarget();

    boolean hasGreenTarget();
    boolean hasRedTarget();

    void addListener(DescriptorChangedListener listener);
    void removeListener(DescriptorChangedListener listener);
}

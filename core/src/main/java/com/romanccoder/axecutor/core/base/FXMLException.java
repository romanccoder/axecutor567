package com.romanccoder.axecutor.core.base;

/**
 * Created by Roman on 06.07.2016.
 */
public class FXMLException extends RuntimeException
{
    public FXMLException(Throwable cause)
    {
        super(cause);
    }

    public FXMLException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public FXMLException(String message)
    {
        super(message);
    }
}

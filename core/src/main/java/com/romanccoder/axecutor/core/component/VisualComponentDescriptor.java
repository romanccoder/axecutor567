package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 20.07.2016.
 */
public interface VisualComponentDescriptor extends ComponentDescriptor
{
    void setX(double x);
    double getX();

    void setY(double y);
    double getY();
}

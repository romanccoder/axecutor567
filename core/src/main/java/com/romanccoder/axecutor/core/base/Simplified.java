package com.romanccoder.axecutor.core.base;

import java.net.URL;

/**
 * Created by Roman on 06.07.2016.
 */
public interface Simplified
{
    URL getIcon();
    String getLabel();
    String getDescription();
}

package com.romanccoder.axecutor.core;

import com.google.inject.AbstractModule;
import com.romanccoder.axecutor.core.base.BaseModule;
import com.romanccoder.axecutor.core.component.ComponentModule;
import com.romanccoder.axecutor.core.locale.LocaleModule;
import com.romanccoder.axecutor.core.project.ProjectModule;
import com.romanccoder.axecutor.core.settings.SettingsModule;
import com.romanccoder.axecutor.core.ui.UIModule;
import com.romanccoder.axecutor.core.util.UtilModule;

/**
 * Created by Roman on 06.07.2016.
 */
public class CoreModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        install(new BaseModule());
        install(new ComponentModule());
        install(new LocaleModule());
        install(new ProjectModule());
        install(new SettingsModule());
        install(new UIModule());
        install(new UtilModule());
    }
}

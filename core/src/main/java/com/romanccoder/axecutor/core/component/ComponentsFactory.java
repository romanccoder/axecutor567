package com.romanccoder.axecutor.core.component;

import java.util.List;

/**
 * Created by Roman on 06.07.2016.
 */
public interface ComponentsFactory
{
    Component create(String id);
    List<Component> createAll();
}

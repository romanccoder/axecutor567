package com.romanccoder.axecutor.core.util.validation;

import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Created by Roman on 06.07.2016.
 */
public interface ValidationProvider
{
    Validator getValidator();
    ValidatorFactory getValidatorFactory();
}

package com.romanccoder.axecutor.core.component.property;

/**
 * Created by Roman on 13.07.2016.
 */
public class PropertyException extends RuntimeException
{
    public PropertyException(String message)
    {
        super(message);
    }

    public PropertyException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PropertyException(Throwable cause)
    {
        super(cause);
    }
}

package com.romanccoder.axecutor.core.project;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

/**
 * Created by Roman on 11.07.2016.
 */
public class ProjectModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(Project.class).to(ProjectImpl.class);
        bind(ProjectSerializer.class).to(ProjectSerializerImpl.class);
        install(new FactoryModuleBuilder()
                .implement(ProjectService.class, ProjectServiceImpl.class)
                .build(ProjectServiceFactory.class)
        );

        bind(ComponentsPosition.class).to(ComponentsPositionImpl.class);
    }
}

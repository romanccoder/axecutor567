package com.romanccoder.axecutor.core.component.builder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Roman on 14.07.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ActionField
{
    String label() default "Action";
    Class<? extends Enum> items();
    EnumValue depends() default @EnumValue;
}
package com.romanccoder.axecutor.core.settings;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

/**
 * Created by Roman on 06.07.2016.
 */
public class SettingsModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(Settings.class).to(SettingsImpl.class);
        bind(SettingsService.class).to(SettingsServiceImpl.class).in(Singleton.class);
    }
}

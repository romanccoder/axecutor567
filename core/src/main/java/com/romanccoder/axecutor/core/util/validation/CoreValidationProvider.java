package com.romanccoder.axecutor.core.util.validation;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Created by Roman on 06.07.2016.
 */
public class CoreValidationProvider implements ValidationProvider
{
    private ValidatorFactory factory;

    public CoreValidationProvider()
    {
        factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(
                        new ResourceBundleMessageInterpolator(
                                new PlatformResourceBundleLocator("com.romanccoder.axecutor.core.bundles.Messages")
                        )
                )
                .buildValidatorFactory();
    }

    @Override
    public Validator getValidator()
    {
        return factory.getValidator();
    }

    @Override
    public ValidatorFactory getValidatorFactory()
    {
        return factory;
    }
}

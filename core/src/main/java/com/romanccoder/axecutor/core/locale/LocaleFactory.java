package com.romanccoder.axecutor.core.locale;

import java.net.URL;
import java.util.Locale;

/**
 * Created by Roman on 06.07.2016.
 */
public interface LocaleFactory
{
    LocaleWrapper create(URL icon, String label, Locale locale);
}

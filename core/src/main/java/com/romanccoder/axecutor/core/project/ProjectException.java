package com.romanccoder.axecutor.core.project;

/**
 * Created by Roman on 12.07.2016.
 */
public class ProjectException extends RuntimeException
{
    public ProjectException(String message)
    {
        super(message);
    }

    public ProjectException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ProjectException(Throwable cause)
    {
        super(cause);
    }
}

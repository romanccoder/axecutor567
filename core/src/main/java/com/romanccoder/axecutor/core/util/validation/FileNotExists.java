package com.romanccoder.axecutor.core.util.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Roman on 07.07.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = FileNotExistsValidator.class)
public @interface FileNotExists
{
    String message() default "{com.romanccoder.axecutor.core.util.validation.FileNotExists.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}


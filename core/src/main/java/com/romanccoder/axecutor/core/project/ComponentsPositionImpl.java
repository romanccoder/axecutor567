package com.romanccoder.axecutor.core.project;

import com.romanccoder.axecutor.core.component.Component;

import java.util.*;

/**
 * Created by Roman on 20.07.2016.
 */
public class ComponentsPositionImpl implements ComponentsPosition
{
    private Map<Component, GridPosition> componentPositionsMap;
    private Map<Component, List<GridPositionListener>> listenersMap;

    public ComponentsPositionImpl()
    {
        componentPositionsMap = new HashMap<>();
        listenersMap = new HashMap<>();
    }

    @Override
    public void set(Component component, GridPosition position)
    {
        if (component == null || position == null)
            throw new NullPointerException();

        componentPositionsMap.put(component, position);

        if (listenersMap.containsKey(component))
        {
            for (GridPositionListener listener : listenersMap.get(component))
                listener.onPositionChanged(component, position);
        }
        else
        {
            listenersMap.put(component, new ArrayList<>());
        }
    }

    @Override
    public boolean hasSet(Component component)
    {
        return componentPositionsMap.containsKey(component);
    }

    @Override
    public boolean hasSet(GridPosition position)
    {
        return componentPositionsMap.containsValue(position);
    }

    @Override
    public void swap(GridPosition position1, GridPosition position2)
    {
        if (!hasSet(position1) || !hasSet(position2) || position1.equals(position2))
            throw new IllegalArgumentException();

        Component temp = getComponent(position2);
        set(getComponent(position1), position2);
        set(temp, position1);
    }

    @Override
    public void remove(Component component)
    {
        if (!componentPositionsMap.containsKey(component))
            throw new NoSuchElementException("No such component: " + component);

        componentPositionsMap.remove(component);
        listenersMap.remove(component);
    }

    @Override
    public GridPosition getPosition(Component component)
    {
        if (!componentPositionsMap.containsKey(component))
            throw new NoSuchElementException("No such component: " + component);

        return componentPositionsMap.get(component);
    }

    @Override
    public Component getComponent(GridPosition position)
    {
        for (Map.Entry<Component, GridPosition> entry : componentPositionsMap.entrySet())
        {
            if (entry.getValue().equals(position))
                return entry.getKey();
        }

        throw new NoSuchElementException("No such component with position: " + position);
    }

    @Override
    public void addListener(Component component, GridPositionListener listener)
    {
        if (!componentPositionsMap.containsKey(component))
            throw new NoSuchElementException("No such component: " + component);

        listenersMap.get(component).add(listener);
    }

    @Override
    public void removeListener(Component component, GridPositionListener listener)
    {
        if (!componentPositionsMap.containsKey(component))
            throw new NoSuchElementException("No such component: " + component);

        listenersMap.get(component).remove(listener);
    }
}

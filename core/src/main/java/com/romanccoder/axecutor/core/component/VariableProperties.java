package com.romanccoder.axecutor.core.component;

import com.romanccoder.axecutor.core.component.builder.ActionField;
import com.romanccoder.axecutor.core.component.builder.EnumValue;
import com.romanccoder.axecutor.core.component.property.CodeFragment;
import com.romanccoder.axecutor.core.component.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Roman on 14.07.2016.
 */
public class VariableProperties implements Properties
{
    enum Actions
    {
        SET,
        GET
    }

    private StringProperty shortDescriptionProperty;

    private Actions action;

    /* SET action */
    private CodeFragment onSetName;
    private CodeFragment onSetValue;

    public VariableProperties()
    {
        shortDescriptionProperty = new SimpleStringProperty("Variable");
    }

    @Property
    public Actions getAction()
    {
        return Actions.GET;
    }

    public void setAction(Actions action)
    {
        this.action = action;
    }

    @Property
    public Actions getAction2()
    {
        return action;
    }

    public void setAction2(Actions action)
    {
        this.action = action;
    }

    @Property
    public CodeFragment getOnSetName()
    {
        return onSetName;
    }

    public void setOnSetName(CodeFragment fragment)
    {
        this.onSetName = fragment;
    }

    @Property
    public CodeFragment getOnSetValue()
    {
        return onSetValue;
    }

    public void setOnSetValue(CodeFragment fragment)
    {
        this.onSetValue = fragment;
    }

    @Override
    public StringProperty shortDescriptionProperty()
    {
        return shortDescriptionProperty;
    }

    @Override
    public String getShortDescription()
    {
        return null;
    }
}

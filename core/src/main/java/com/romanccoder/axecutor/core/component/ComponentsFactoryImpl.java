package com.romanccoder.axecutor.core.component;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.util.ResourceProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Roman on 06.07.2016.
 */
public class ComponentsFactoryImpl implements ComponentsFactory
{
    private HashMap<String, String> componentsMap;

    @Inject
    private Injector injector;

    @Inject
    public ComponentsFactoryImpl(@Core ResourceProvider resourceProvider)
    {
        componentsMap = new HashMap<>();
        JsonParser parser = new JsonParser();

        try (Reader reader = new BufferedReader(new InputStreamReader(resourceProvider.getData("components.json").openStream())))
        {
            JsonArray arr = (JsonArray) parser.parse(reader);

            for (JsonElement element : arr)
            {
                String componentName = element.getAsString();
                Class<?> clazz = Class.forName(getClass().getPackage().getName() + "." + componentName + "Component");
                Component component = (Component) clazz.newInstance();
                componentsMap.put(component.getId(), clazz.getName());
            }
        }
        catch (IOException e)
        {
            throw new ComponentException(e);
        }
        catch (ClassNotFoundException e)
        {
            throw new ComponentException(e);
        }
        catch (ReflectiveOperationException e)
        {
            throw new ComponentException(e);
        }

    }

    @Override
    public Component create(String id)
    {
        if (!componentsMap.containsKey(id))
            throw new NoSuchElementException("No such component with id: " + id);

        try
        {
            Class<?> clazz = Class.forName(componentsMap.get(id));
            Component component = (Component) clazz.newInstance();
            injector.injectMembers(component);

            return component;
        }
        catch (ReflectiveOperationException e)
        {
            throw new ComponentException(e);
        }
    }

    @Override
    public List<Component> createAll()
    {
        List<Component> componentsList = new ArrayList<>();
        componentsMap.keySet().stream().forEach(id -> componentsList.add(create(id)));

        return componentsList;
    }
}

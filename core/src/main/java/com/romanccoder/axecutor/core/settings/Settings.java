package com.romanccoder.axecutor.core.settings;

import com.romanccoder.axecutor.core.locale.LocaleWrapper;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import org.hibernate.validator.constraints.Range;

import java.util.Observable;

/**
 * Created by Roman on 06.07.2016.
 */
public abstract class Settings extends Observable
{
    public final static int MIN_TIMEOUT      = 5;
    public final static int MAX_TIMEOUT      = 65;
    public final static int TIMEOUT_NO_LIMIT = 65;

    public abstract void setDefault();

    public abstract ObjectProperty<LocaleWrapper> activeLocaleProperty();
    public abstract void setActiveLocale(LocaleWrapper model);
    public abstract LocaleWrapper getActiveLocale();

    public abstract IntegerProperty commandTimeoutProperty();
    public abstract void setCommandTimeout(int timeout);
    @Range(min = MIN_TIMEOUT, max = MAX_TIMEOUT)
    public abstract int getCommandTimeout();

    public abstract IntegerProperty ftpOperationTimeoutProperty();
    public abstract void setFtpOperationTimeout(int timeout);
    @Range(min = MIN_TIMEOUT, max = MAX_TIMEOUT)
    public abstract int getFtpOperationTimeout();

    public abstract BooleanProperty clearLogProperty();
    public abstract void setClearLog(boolean clear);
    public abstract boolean getClearLog();

    public abstract IntegerProperty taskMaxRecordsProperty();
    public abstract void setTaskMaxRecords(int max);
    public abstract int getTaskMaxRecords();

    public void copy(Settings fromObj)
    {
        setActiveLocale(fromObj.getActiveLocale());
        setCommandTimeout(fromObj.getCommandTimeout());
        setFtpOperationTimeout(fromObj.getFtpOperationTimeout());
        setClearLog(fromObj.getClearLog());
        setTaskMaxRecords(fromObj.getTaskMaxRecords());
    }
}

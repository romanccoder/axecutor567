package com.romanccoder.axecutor.core.project;

import java.io.File;

/**
 * Created by Roman on 11.07.2016.
 */
public interface ProjectService
{
    String NAME_KEY = "name";
    String TREE_KEY = "tree";

    File getFile();

    void setProject(Project project);
    Project getProject();

    void load();
    void save();
}

package com.romanccoder.axecutor.core.util.validation;

import javafx.beans.property.BooleanProperty;
import javafx.scene.Parent;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Roman on 07.07.2016.
 */
public interface ViewValidationUtil extends Observer
{
    void setModel(Observable model);
    Observable getModel();

    void setView(Parent view);
    Parent getView();

    void enable();
    void disable();
    boolean isEnabled();

    BooleanProperty hasErrorsProperty();
    boolean hasErrors();
}

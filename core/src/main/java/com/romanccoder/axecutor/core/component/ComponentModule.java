package com.romanccoder.axecutor.core.component;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.romanccoder.axecutor.core.component.builder.PropertiesBuilder;
import com.romanccoder.axecutor.core.component.builder.PropertiesBuilderFactory;
import com.romanccoder.axecutor.core.component.builder.PropertiesBuilderImpl;

/**
 * Created by Roman on 07.07.2016.
 */
public class ComponentModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(ComponentsFactory.class).to(ComponentsFactoryImpl.class).in(Singleton.class);
        bind(ComponentsService.class).to(ComponentsServiceImpl.class).in(Singleton.class);
        bind(ComponentsTree.class).to(ComponentsTreeImpl.class);
        install(
                new FactoryModuleBuilder()
                        .implement(ComponentDescriptor.class, ComponentDescriptorImpl.class)
                        .build(ComponentDescriptorFactory.class)
        );

        install(
                new FactoryModuleBuilder()
                        .implement(PropertiesBuilder.class, PropertiesBuilderImpl.class)
                        .build(PropertiesBuilderFactory.class)
        );
    }
}

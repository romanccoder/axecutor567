package com.romanccoder.axecutor.core.component;

import java.util.List;

/**
 * Created by Roman on 06.07.2016.
 */
public interface ComponentsService
{
    List<Component> getByCategory(ComponentCategory category);
    Component getById(String id);
    List<Component> getAll();

    boolean exists(String id);
}

package com.romanccoder.axecutor.core.component.builder;

import com.romanccoder.axecutor.core.component.property.PropertyHelper;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Roman on 16.07.2016.
 */
public class ChoiceFieldBuilder implements FieldBuilder<ChoiceField>
{
    private HBox hBox;

    private ChoiceField data;
    private Object properties;
    private String associatedProperty;
    private PropertiesBuilder propertiesBuilder;

    public ChoiceFieldBuilder(ChoiceField data, Object properties, String associatedProperty, PropertiesBuilder propertiesBuilder)
    {
        this.data = data;
        this.properties = properties;
        this.associatedProperty = associatedProperty;
        this.propertiesBuilder = propertiesBuilder;

        hBox = new HBox();
    }

    @Override
    public ChoiceField getData()
    {
        return data;
    }

    @Override
    public Object getProperties()
    {
        return properties;
    }

    @Override
    public String getAssociatedProperty()
    {
        return associatedProperty;
    }

    @Override
    public PropertiesBuilder getPropertiesBuilder()
    {
        return propertiesBuilder;
    }

    @Override
    public void build()
    {
        hBox.getChildren().clear();

        ToggleGroup group = new ToggleGroup();
        Map<Enum, RadioButton> itemsMap = new HashMap<>();
        for (Enum item : data.items().getEnumConstants())
        {
            RadioButton radioButton = new RadioButton(item.toString());
            itemsMap.put(item, radioButton);
            radioButton.setToggleGroup(group);
            radioButton.selectedProperty().addListener((observable, oldValue, newValue) -> {if (newValue) PropertyHelper.setValue(properties, associatedProperty, item);});
            hBox.getChildren().add(radioButton);
        }

        propertiesBuilder.addListener(() ->
        {
            for (FieldBuilder builder : propertiesBuilder.getPropertiesMap().values())
            {
                try
                {
                    Method method = builder.getData().annotationType().getDeclaredMethod("depends");

                    if (!method.getReturnType().isAssignableFrom(EnumValue.class))
                        continue;

                    EnumValue enumValue = (EnumValue) method.invoke(builder.getData());
                    if (!enumValue.property().equals(associatedProperty))
                        continue;

                    Enum item = Enum.valueOf(data.items(), enumValue.value());

                    RadioButton associatedRadioButton = itemsMap.get(item);

                }
                catch (NoSuchMethodException e)
                {

                }
                catch (IllegalAccessException | InvocationTargetException | IllegalArgumentException e)
                {
                    throw new BuilderException(e);
                }
            }
        });
    }

    @Override
    public Parent getRoot()
    {
        return hBox;
    }
}

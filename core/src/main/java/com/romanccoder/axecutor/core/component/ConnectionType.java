package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 23.07.2016.
 */
public enum ConnectionType
{
    GREEN,
    RED
}

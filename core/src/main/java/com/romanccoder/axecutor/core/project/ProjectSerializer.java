package com.romanccoder.axecutor.core.project;

/**
 * Created by Roman on 13.07.2016.
 */
public interface ProjectSerializer
{
    String NAME_KEY            = "name";

    String COMPONENTS_KEY                = "components";
    String COMPONENT_ID_KEY              = "id";
    String COMPONENT_POSITION_KEY        = "position";
    String COMPONENT_POSITION_COLUMN_KEY = "x";
    String COMPONENT_POSITION_ROW_KEY    = "y";
    String COMPONENT_MODEL_KEY           = "model";

    String COMPONENT_DESCRIPTOR_KEY              = "tree";
    String COMPONENT_DESCRIPTOR_GREEN_TARGET_KEY = "green_target";
    String COMPONENT_DESCRIPTOR_RED_TARGET_KEY   = "red_target";

    void setProject(Project project);
    Project getProject();

    String serialize();
    void deserialize();
}

package com.romanccoder.axecutor.core.settings;

/**
 * Created by Roman on 06.07.2016.
 */
public class SettingsException extends RuntimeException
{
    public SettingsException(String message)
    {
        super(message);
    }

    public SettingsException(String message, Throwable cause)
    {
        super(message, cause);
    }
}

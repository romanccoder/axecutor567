package com.romanccoder.axecutor.core.component.builder;

import com.romanccoder.axecutor.core.component.property.PropertyHelper;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by Roman on 14.07.2016.
 */
public class ActionFieldBuilder implements FieldBuilder<ActionField>
{
    private VBox vBox;

    private ActionField data;
    private Object properties;
    private String associatedProperty;
    private PropertiesBuilder propertiesBuilder;

    public ActionFieldBuilder(ActionField data, Object properties, String associatedProperty, PropertiesBuilder propertiesBuilder)
    {
        this.data = data;
        this.properties = properties;
        this.associatedProperty = associatedProperty;
        this.propertiesBuilder = propertiesBuilder;

        vBox = new VBox();
    }

    @Override
    public ActionField getData()
    {
        return data;
    }

    @Override
    public Object getProperties()
    {
        return properties;
    }

    @Override
    public String getAssociatedProperty()
    {
        return associatedProperty;
    }

    @Override
    public PropertiesBuilder getPropertiesBuilder()
    {
        return propertiesBuilder;
    }

    @Override
    public void build()
    {
        vBox.getChildren().clear();

        Label label = new Label(data.label());
        label.getStyleClass().add("control-label");
        vBox.getChildren().add(label);

        Enum[] items = data.items().getEnumConstants();
        ComboBox comboBox = new ComboBox();
        comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> PropertyHelper.setValue(properties, associatedProperty, newValue));
        comboBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        comboBox.getItems().addAll(items);
        vBox.getChildren().add(comboBox);

        propertiesBuilder.addListener(() ->
        {
            for (FieldBuilder builder : propertiesBuilder.getPropertiesMap().values())
            {
                try
                {
                    Method method = builder.getData().annotationType().getDeclaredMethod("depends");

                    if (!method.getReturnType().isAssignableFrom(EnumValue.class))
                        continue;

                    EnumValue enumValue = (EnumValue) method.invoke(builder.getData());
                    if (!enumValue.property().equals(associatedProperty))
                        continue;

                    builder.getRoot().visibleProperty().bind(comboBox.getSelectionModel().selectedItemProperty().isEqualTo(Enum.valueOf(data.items(), enumValue.value())));
                }
                catch (NoSuchMethodException e)
                {

                }
                catch (IllegalAccessException | InvocationTargetException | IllegalArgumentException e)
                {
                    throw new BuilderException(e);
                }
            }
        });

    }

    @Override
    public Parent getRoot()
    {
        return vBox;
    }
}

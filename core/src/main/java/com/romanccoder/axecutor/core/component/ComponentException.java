package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 06.07.2016.
 */
public class ComponentException extends RuntimeException
{
    public ComponentException(Throwable cause)
    {
        super(cause);
    }

    public ComponentException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ComponentException(String message)
    {
        super(message);
    }
}

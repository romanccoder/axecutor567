package com.romanccoder.axecutor.core.base;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.File;
import java.nio.file.Paths;

/**
 * Created by Roman on 06.07.2016.
 */
public class AppImpl implements App
{
    @Override
    public String getName()
    {
        return "Axecutor";
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    public File getDocumentsRoot()
    {
        String os = System.getProperty("os.name");

        if (os.contains("Mac"))
        {
            return Paths.get(System.getProperty("user.home"), "Documents", getName()).toFile();
        }
        else
        {
            return Paths.get(System.getProperty("user.home"), getName()).toFile();
        }
    }

    @Override
    public String getProjectExtension()
    {
        return "axproject";
    }

}

package com.romanccoder.axecutor.core.component;

import javafx.beans.property.StringProperty;

/**
 * Created by roman on 9/29/16.
 */
public interface Properties
{
    StringProperty shortDescriptionProperty();
    String getShortDescription();
}

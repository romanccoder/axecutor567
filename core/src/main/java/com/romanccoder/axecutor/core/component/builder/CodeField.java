package com.romanccoder.axecutor.core.component.builder;

/**
 * Created by Roman on 14.07.2016.
 */
public @interface CodeField
{
    String label() default "Code";
    EnumValue depends() default @EnumValue;
}

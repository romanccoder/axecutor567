package com.romanccoder.axecutor.core.util.validation;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.Core;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;

import javax.validation.ConstraintViolation;
import javax.validation.metadata.PropertyDescriptor;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

/**
 * Created by Roman on 07.07.2016.
 */
public class ViewValidationUtilImpl implements ViewValidationUtil
{
    @Inject
    @Core
    private ValidationProvider validationProvider;

    private Observable model;
    private Parent view;

    private Map<String, Label> labelMap;

    private boolean enabled;
    private BooleanProperty hasErrors;

    public ViewValidationUtilImpl()
    {
        enabled = false;
        hasErrors = new SimpleBooleanProperty(false);
        labelMap = new HashMap<>();
    }

    @Override
    public void setModel(Observable model)
    {
        if (enabled)
            disable();
        this.model = model;
    }

    @Override
    public Observable getModel()
    {
        return model;
    }

    @Override
    public void setView(Parent view)
    {
        if (enabled)
            disable();
        this.view = view;
    }

    @Override
    public Parent getView()
    {
        return view;
    }

    @Override
    public void enable()
    {
        if (enabled)
            return;

        model.addObserver(this);
        Set<PropertyDescriptor> propertyDescriptors = validationProvider.getValidator().getConstraintsForClass(model.getClass()).getConstrainedProperties();
        for (PropertyDescriptor descriptor : propertyDescriptors)
        {
            Node node = view.lookup("#" + descriptor.getPropertyName() + "ErrorLabel");
            if (node != null)
                labelMap.put(descriptor.getPropertyName(), (Label)node);
        }
        update(model, null);
        enabled = true;
    }

    @Override
    public void disable()
    {
        model.deleteObserver(this);
        labelMap.clear();
        enabled = false;
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    @Override
    public BooleanProperty hasErrorsProperty()
    {
        return hasErrors;
    }

    @Override
    public boolean hasErrors()
    {
        return hasErrors.get();
    }

    @Override
    public void update(Observable o, Object arg)
    {
        // TODO Wrap in Task or other concurrent class
        Set<ConstraintViolation<Observable>> constraints = validationProvider.getValidator().validate(model);
        if (constraints.isEmpty())
            hasErrors.set(false);
        else
            hasErrors.set(true);

        for (Label label : labelMap.values())
        {
            label.setText(null);
            label.setVisible(false);
        }

        for (ConstraintViolation<Observable> constraint : constraints)
        {
            String propertyName = constraint.getPropertyPath().toString();
            if (labelMap.containsKey(propertyName))
            {
                Label label = labelMap.get(propertyName);
                label.setText(constraint.getMessage());
                label.setVisible(true);
            }
        }
    }
}

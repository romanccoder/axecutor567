package com.romanccoder.axecutor.core.base;

import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

/**
 * Created by Roman on 06.07.2016.
 */
public class AbstractController
{
    private TranslationProvider translationProvider;
    private ResourceProvider resourceProvider;

    protected void setTranslationProvider(TranslationProvider translationProvider)
    {
        this.translationProvider = translationProvider;
    }

    protected TranslationProvider getTranslationProvider()
    {
        return translationProvider;
    }

    protected void setResourceProvider(ResourceProvider resourceProvider)
    {
        this.resourceProvider = resourceProvider;
    }

    protected ResourceProvider getResourceProvider()
    {
        return resourceProvider;
    }

    protected void loadView(String name)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(resourceProvider.getUI(name + ".fxml"));
            loader.setController(this);
            loader.setResources(translationProvider.getBundle());
            loader.load();
        }
        catch (IOException e)
        {
            throw new FXMLException(e);
        }
    }

    protected void loadView()
    {
        loadView(getName().substring(0, 1).toLowerCase() + getName().substring(1).replaceAll("([A-Z])", "_$1").toLowerCase());
    }

    protected String getName()
    {
        return getClass().getSimpleName().substring(0, getClass().getSimpleName().indexOf("Controller"));
    }
}

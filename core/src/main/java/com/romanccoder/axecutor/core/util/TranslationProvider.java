package com.romanccoder.axecutor.core.util;

import java.util.ResourceBundle;

/**
 * Created by Roman on 06.07.2016.
 */
public interface TranslationProvider
{
    ResourceBundle getBundle();

    default String get(String key)
    {
        return getBundle().getString(key);
    }

    default String get(Enum obj)
    {
        return get(obj.getClass().getName() + "." + obj.name());
    }

}

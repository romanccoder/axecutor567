package com.romanccoder.axecutor.core.util;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.text.Font;

/**
 * Created by Roman on 16.06.2016.
 */
public class MaterialTheme
{
    static
    {
        Font.loadFont(MaterialTheme.class.getResource("/com/romanccoder/axecutor/core/fonts/Roboto-Regular.ttf").toExternalForm(), -1);
        Font.loadFont(MaterialTheme.class.getResource("/com/romanccoder/axecutor/core/fonts/Roboto-Medium.ttf").toExternalForm(), -1);
    }

    public static void applyStyle(Scene scene)
    {
        scene.getStylesheets().add(MaterialTheme.class.getResource("/com/romanccoder/axecutor/core/css/material-fx.css").toExternalForm());
        scene.getStylesheets().add(MaterialTheme.class.getResource("/com/romanccoder/axecutor/core/css/styles.css").toExternalForm());
    }

    public static void applyStyle(Dialog dialog)
    {
        applyStyle(dialog.getDialogPane());
    }

    public static void applyStyle(Parent parent)
    {
        parent.getStylesheets().add(MaterialTheme.class.getResource("/com/romanccoder/axecutor/core/css/material-fx.css").toExternalForm());
        parent.getStylesheets().add(MaterialTheme.class.getResource("/com/romanccoder/axecutor/core/css/styles.css").toExternalForm());
    }
}

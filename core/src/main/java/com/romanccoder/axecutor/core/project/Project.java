package com.romanccoder.axecutor.core.project;

import com.romanccoder.axecutor.core.component.ComponentsTree;

/**
 * Created by Roman on 11.07.2016.
 */
public interface Project
{
    void setName(String name);
    String getName();

    void setTree(ComponentsTree tree);
    ComponentsTree getTree();

    void setPositions(ComponentsPosition componentsPosition);
    ComponentsPosition getPositions();
}

package com.romanccoder.axecutor.core.component.builder;

import com.romanccoder.axecutor.core.component.Component;

/**
 * Created by Roman on 14.07.2016.
 */
public interface PropertiesBuilderFactory
{
    PropertiesBuilder create(Component component);
}

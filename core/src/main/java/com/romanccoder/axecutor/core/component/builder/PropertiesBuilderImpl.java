package com.romanccoder.axecutor.core.component.builder;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.romanccoder.axecutor.core.component.Component;
import com.romanccoder.axecutor.core.component.property.PropertyHelper;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import javax.swing.event.EventListenerList;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Roman on 14.07.2016.
 */
public class PropertiesBuilderImpl implements PropertiesBuilder
{
    private Map<Class<? extends Annotation>, Class<? extends FieldBuilder>> buildersMap;
    private Map<String, FieldBuilder> propertiesMap;

    private EventListenerList listenerList;

    private Set<String> ignoreSet;

    private Component component;

    private VBox vBox;

    @Inject
    public PropertiesBuilderImpl(@Assisted Component component)
    {
        this.component = component;
        listenerList = new EventListenerList();
        vBox = new VBox();
        ignoreSet = new HashSet<>();

        buildersMap = new HashMap<>();
        buildersMap.put(ActionField.class, ActionFieldBuilder.class);
        buildersMap.put(ChoiceField.class, ChoiceFieldBuilder.class);

        propertiesMap = new HashMap<>();
    }

    @Override
    public void addListener(AfterBuiltEventListener listener)
    {
        listenerList.add(AfterBuiltEventListener.class, listener);
    }

    @Override
    public void removeListener(AfterBuiltEventListener listener)
    {
        listenerList.remove(AfterBuiltEventListener.class, listener);
    }

    @Override
    public Component getComponent()
    {
        return component;
    }

    @Override
    public Map<String, FieldBuilder> getPropertiesMap()
    {
        return propertiesMap;
    }

    @Override
    public Set<String> getIgnoreSet()
    {
        return ignoreSet;
    }

    @Override
    public void build()
    {
        if (component.getProperties() == null)
            throw new IllegalStateException("Component hasn't properties model");

        propertiesMap.clear();

        for (Method method : component.getProperties().getClass().getDeclaredMethods())
        {
            for (Class<? extends Annotation> annoClazz : buildersMap.keySet())
            {
                if (method.isAnnotationPresent(annoClazz))
                {
                    FieldBuilder builder = makeBuilder(annoClazz, PropertyHelper.getPropertyName(method.getName()));
                    propertiesMap.put(PropertyHelper.getPropertyName(method.getName()), builder);
                    builder.build();

                    break;
                }
            }
        }

        propertiesMap.entrySet().stream().filter(entry -> !ignoreSet.contains(entry.getKey())).forEach(entry -> vBox.getChildren().add(entry.getValue().getRoot()));

        for (AfterBuiltEventListener listener : listenerList.getListeners(AfterBuiltEventListener.class))
            listener.handle();
    }

    private FieldBuilder makeBuilder(Class<? extends Annotation> annoClazz, String property)
    {
        if (!buildersMap.containsKey(annoClazz))
            throw new NoSuchElementException("No builder registered for annotation " + annoClazz.getSimpleName());

        try
        {
            FieldBuilder builder = buildersMap.get(annoClazz)
                    .getConstructor(annoClazz, Object.class, String.class, PropertiesBuilder.class)
                    .newInstance(getComponent().getProperties().getClass().getDeclaredMethod(PropertyHelper.getAccessorName(property)).getDeclaredAnnotation(annoClazz), component.getProperties(), property, this);

            return builder;
        }
        catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e)
        {
            throw new BuilderException(e);
        }
    }

    @Override
    public Parent getRoot()
    {
        return vBox;
    }
}

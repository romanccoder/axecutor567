package com.romanccoder.axecutor.core.component;

/**
 * Created by roman on 10/4/16.
 */
public class CoreComponentExistsException extends RuntimeException
{
    private Component component;

    public CoreComponentExistsException(String message)
    {
        super(message);
    }

    public CoreComponentExistsException(String message, Component component)
    {
        super(message);
        this.component = component;
    }

    public CoreComponentExistsException(Component component)
    {
        super();
        this.component = component;
    }

    public Component getComponent()
    {
        return component;
    }
}

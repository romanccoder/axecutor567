package com.romanccoder.axecutor.core.settings;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.locale.LocaleService;
import com.romanccoder.axecutor.core.locale.LocaleWrapper;
import javafx.beans.property.*;

/**
 * Created by Roman on 06.07.2016.
 */
public class SettingsImpl extends Settings
{
    private static final String  DEFAULT_LOCALE = "en";
    private static final int     DEFAULT_COMMAND_TIMEOUT = 60;
    private static final int     DEFAULT_FTP_OPERATION_TIMEOUT = 30;
    private static final boolean DEFAULT_CLEAR_LOG = false;
    private static final int     DEFAULT_TASK_MAX_RECORDS = -1;

    private LocaleService localeService;

    private ObjectProperty<LocaleWrapper> activeLocaleProperty;
    private IntegerProperty commandTimeoutProperty;
    private IntegerProperty ftpOperationTimeoutProperty;
    private BooleanProperty clearLogProperty;
    private IntegerProperty taskMaxRecordsProperty;


    @Inject
    public SettingsImpl(LocaleService localeService)
    {
        this.localeService = localeService;

        activeLocaleProperty = new SimpleObjectProperty<>();
        commandTimeoutProperty = new SimpleIntegerProperty();
        ftpOperationTimeoutProperty = new SimpleIntegerProperty();
        clearLogProperty = new SimpleBooleanProperty();
        taskMaxRecordsProperty = new SimpleIntegerProperty();

        setDefault();
    }

    public void setDefault()
    {
        setActiveLocale(localeService.getById(DEFAULT_LOCALE));
        setCommandTimeout(DEFAULT_COMMAND_TIMEOUT);
        setFtpOperationTimeout(DEFAULT_FTP_OPERATION_TIMEOUT);
        setClearLog(DEFAULT_CLEAR_LOG);
        setTaskMaxRecords(DEFAULT_TASK_MAX_RECORDS);
    }

    @Override
    public ObjectProperty<LocaleWrapper> activeLocaleProperty()
    {
        return activeLocaleProperty;
    }

    @Override
    public void setActiveLocale(LocaleWrapper wrapper)
    {
        activeLocaleProperty.set(wrapper);
    }

    @Override
    public LocaleWrapper getActiveLocale()
    {
        return activeLocaleProperty.get();
    }

    @Override
    public IntegerProperty commandTimeoutProperty()
    {
        return commandTimeoutProperty;
    }

    @Override
    public void setCommandTimeout(int timeout)
    {
        commandTimeoutProperty.set(timeout);
    }

    @Override
    public int getCommandTimeout()
    {
        return commandTimeoutProperty.get();
    }

    @Override
    public IntegerProperty ftpOperationTimeoutProperty()
    {
        return ftpOperationTimeoutProperty;
    }

    @Override
    public void setFtpOperationTimeout(int timeout)
    {
        ftpOperationTimeoutProperty.set(timeout);
    }

    @Override
    public int getFtpOperationTimeout()
    {
        return ftpOperationTimeoutProperty.get();
    }

    @Override
    public BooleanProperty clearLogProperty()
    {
        return clearLogProperty;
    }

    @Override
    public void setClearLog(boolean clear)
    {
        clearLogProperty.set(clear);
    }

    @Override
    public boolean getClearLog()
    {
        return clearLogProperty.get();
    }

    @Override
    public IntegerProperty taskMaxRecordsProperty()
    {
        return taskMaxRecordsProperty;
    }

    @Override
    public void setTaskMaxRecords(int max)
    {
        taskMaxRecordsProperty.set(max);
    }

    @Override
    public int getTaskMaxRecords()
    {
        return taskMaxRecordsProperty.get();
    }
}

package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 20.07.2016.
 */
public class VisualComponentDescriptorImpl extends ComponentDescriptorImpl implements VisualComponentDescriptor
{
    private double x;
    private double y;

    public VisualComponentDescriptorImpl(Component source)
    {
        super(source);
    }

    @Override
    public void setX(double x)
    {
        this.x = x;
    }

    @Override
    public double getX()
    {
        return x;
    }

    @Override
    public void setY(double y)
    {
        this.y = y;
    }

    @Override
    public double getY()
    {
        return y;
    }
}

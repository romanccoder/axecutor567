package com.romanccoder.axecutor.core.locale;

import com.romanccoder.axecutor.core.base.Identifiable;
import com.romanccoder.axecutor.core.base.Simplified;

import java.util.Locale;

/**
 * Created by Roman on 06.07.2016.
 */
public interface LocaleWrapper extends Identifiable<String>, Simplified
{
    public Locale getLocale();
}

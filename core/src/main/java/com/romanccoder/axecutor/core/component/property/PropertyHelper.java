package com.romanccoder.axecutor.core.component.property;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman on 13.07.2016.
 */
public class PropertyHelper
{
    public static String getPropertyName(String methodName)
    {
        String name = methodName.substring(3);

        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    public static String getAccessorName(String propertyName)
    {
        return "get" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
    }

    public static String getSetterName(String propertyName)
    {
        return "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
    }

    public static String[] getProperties(Object model)
    {
        List<String> propertiesList = new ArrayList<>();

        for (Method method : model.getClass().getDeclaredMethods())
        {
            if (!method.isAnnotationPresent(Property.class))
                continue;

            propertiesList.add(getPropertyName(method.getName()));
        }

        return propertiesList.toArray(new String[0]);
    }

    public static Object getValue(Object model, String name)
    {
        try
        {
            return model.getClass().getDeclaredMethod(getAccessorName(name)).invoke(model);
        }
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
        {
            throw new PropertyException(e);
        }
    }

    public static void setValue(Object model, String name, Object value)
    {
        try
        {
            boolean hasBeenSet = false;

            for (Method method : model.getClass().getDeclaredMethods())
            {
                if (!method.getName().equals(getSetterName(name)))
                    continue;

                method.invoke(model, value);

                hasBeenSet = true;

                break;
            }

            if (!hasBeenSet)
                throw new PropertyException("Setter for property" + name + " was not founded");
        }
        catch (IllegalAccessException | InvocationTargetException e)
        {
            throw new PropertyException(e);
        }
    }
}

package com.romanccoder.axecutor.core.component.builder;

import com.romanccoder.axecutor.core.component.Component;
import javafx.scene.Parent;

import java.util.EventListener;
import java.util.Map;
import java.util.Set;

/**
 * Created by Roman on 14.07.2016.
 */
public interface PropertiesBuilder
{
    interface AfterBuiltEventListener extends EventListener
    {
        void handle();
    }

    void addListener(AfterBuiltEventListener listener);
    void removeListener(AfterBuiltEventListener listener);

    Component getComponent();
    Map<String, FieldBuilder> getPropertiesMap();
    Set<String> getIgnoreSet();
    void build();
    Parent getRoot();
}

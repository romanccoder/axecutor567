package com.romanccoder.axecutor.core.settings;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.romanccoder.axecutor.core.base.App;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.locale.LocaleService;
import com.romanccoder.axecutor.core.util.validation.ValidationProvider;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.*;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;

/**
 * Created by Roman on 06.07.2016.
 */
public class SettingsServiceImpl implements SettingsService
{
    private File file;
    private Properties properties;
    @Inject
    private Settings model;

    @Inject
    private Provider<Settings> modelProvider;
    @Inject
    private LocaleService localeService;
    @Inject
    @Core
    private ValidationProvider validationProvider;

    @Inject
    public SettingsServiceImpl(App app)
    {
        properties = new Properties();
        file = Paths.get(app.getDocumentsRoot().getAbsolutePath(), FILE_NAME).toFile();
    }

    @Override
    public void setModel(Settings model)
    {
        this.model = model;
    }

    @Override
    public Settings getModel()
    {
        return model;
    }

    @Override
    public boolean refresh() throws SettingsException
    {
        if (!file.isFile() || !file.exists())
            return false;

        try(Reader reader = new FileReader(file))
        {
            properties.clear();
            properties.load(reader);
        }
        catch (IOException e)
        {
            throw new SettingsException("Can't open settings file", e);
        }
        catch (IllegalArgumentException e)
        {
            throw new SettingsException("Settings file is broken", e);
        }

        for (String key : new String[]{LOCALE_KEY, COMMAND_TIMEOUT_KEY, FTP_OPERATION_TIMEOUT_KEY, CLEAR_LOG_KEY, TASK_MAX_RECORDS_KEY})
            if (!properties.containsKey(key))
                throw new SettingsException("Settings file is broken");

        model = modelProvider.get();

        try
        {
            model.setActiveLocale(localeService.getById(properties.getProperty(LOCALE_KEY)));
            model.setCommandTimeout(Integer.parseInt(properties.getProperty(COMMAND_TIMEOUT_KEY)));
            model.setFtpOperationTimeout(Integer.parseInt(properties.getProperty(FTP_OPERATION_TIMEOUT_KEY)));
            model.setClearLog(Boolean.valueOf(properties.getProperty(CLEAR_LOG_KEY)));
            model.setTaskMaxRecords(Integer.parseInt(properties.getProperty(TASK_MAX_RECORDS_KEY)));
        }
        catch (NumberFormatException | NoSuchElementException e)
        {
            throw new SettingsException("Settings file is broken");
        }

        if (!validate())
            throw new SettingsException("Settings file is broken");

        return true;
    }

    @Override
    public void save() throws SettingsException
    {
        if (!validate())
            throw new SettingsException("Model validation failed");

        properties.clear();
        properties.put(LOCALE_KEY, getModel().getActiveLocale().getId());
        properties.put(COMMAND_TIMEOUT_KEY, String.valueOf(getModel().getCommandTimeout()));
        properties.put(FTP_OPERATION_TIMEOUT_KEY, String.valueOf(getModel().getFtpOperationTimeout()));
        properties.put(CLEAR_LOG_KEY, String.valueOf(getModel().getClearLog()));
        properties.put(TASK_MAX_RECORDS_KEY, String.valueOf(getModel().getTaskMaxRecords()));

        if (!file.exists())
            file.getParentFile().mkdirs();

        try(Writer writer = new PrintWriter(file))
        {
            properties.store(writer, "Settings");
        }
        catch (IOException e)
        {
            throw new SettingsException("Can't write to settings file", e);
        }
    }

    @Override
    public boolean validate()
    {
        Validator validator = validationProvider.getValidator();
        Set<ConstraintViolation<Settings>> constraintSet = validator.validate(model);

        return constraintSet.isEmpty();
    }

    @Override
    public File getFile()
    {
        return file;
    }
}

package com.romanccoder.axecutor.core.util;

import com.google.inject.AbstractModule;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.util.validation.CoreValidationProvider;
import com.romanccoder.axecutor.core.util.validation.ValidationProvider;
import com.romanccoder.axecutor.core.util.validation.ViewValidationUtil;
import com.romanccoder.axecutor.core.util.validation.ViewValidationUtilImpl;

/**
 * Created by Roman on 06.07.2016.
 */
public class UtilModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(TranslationProvider.class).annotatedWith(Core.class).to(CoreTranslationProvider.class);
        bind(ValidationProvider.class).annotatedWith(Core.class).to(CoreValidationProvider.class);
        bind(ResourceProvider.class).annotatedWith(Core.class).to(CoreResourceProvider.class);
        bind(ViewValidationUtil.class).to(ViewValidationUtilImpl.class);
    }
}

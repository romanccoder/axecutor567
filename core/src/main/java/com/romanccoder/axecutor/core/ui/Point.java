package com.romanccoder.axecutor.core.ui;

/**
 * Created by Roman on 18.07.2016.
 */
public class Point
{
    private double x;
    private double y;

    public Point(double x, double y)
    {
        setX(x);
        setY(y);
    }

    public Point()
    {}

    public Point(Point point)
    {
        setX(point.getX());
        setY(point.getY());
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getX()
    {
        return x;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public double getY()
    {
        return y;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Point))
            return false;

        Point point = (Point) obj;

        return point.getX() == getX() && point.getY() == getY();
    }
}

package com.romanccoder.axecutor.core.util;

import java.net.URL;

/**
 * Created by Roman on 06.07.2016.
 */
public interface ResourceProvider
{
    String BUNDLES_DIRECTORY = "bundles";
    String IMAGES_DIRECTORY  = "images";
    String CSS_DIRECTORY     = "css";
    String FONTS_DIRECTORY   = "fonts";
    String UI_DIRECTORY      = "ui";
    String DATA_DIRECTORY    = "data";

    String getBasePath();

    default URL getBundle(String name)
    {
        return getClass().getResource(getBasePath() + "/" + BUNDLES_DIRECTORY + "/" + name);
    }

    default URL getImage(String name)
    {
        return getClass().getResource(getBasePath() + "/" + IMAGES_DIRECTORY + "/" + name);
    }


    default URL getCSS(String name)
    {
        return getClass().getResource(getBasePath() + "/" + CSS_DIRECTORY + "/" + name);
    }

    default URL getFont(String name)
    {
        return getClass().getResource(getBasePath() + "/" + FONTS_DIRECTORY + "/" + name);
    }

    default URL getUI(String name)
    {
        return getClass().getResource(getBasePath() + "/" + UI_DIRECTORY + "/" + name);
    }

    default URL getData(String name)
    {
        return getClass().getResource(getBasePath() + "/" + DATA_DIRECTORY + "/" + name);
    }

}

package com.romanccoder.axecutor.core.component;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import javafx.scene.Parent;
import javafx.scene.paint.Color;

import java.net.URL;

/**
 * Created by Roman on 14.07.2016.
 */
public class VariableComponent implements Component
{
    @Inject
    @Core
    private ResourceProvider resourceProvider;

    private VariableProperties properties;

    public VariableComponent()
    {
        properties = new VariableProperties();
    }

    @Override
    public ComponentType getType()
    {
        return ComponentType.STANDALONE;
    }

    @Override
    public ComponentCategory getCategory()
    {
        return ComponentCategory.BASE;
    }

    @Override
    public Color getColor()
    {
        return Color.ORANGE;
    }

    @Override
    public Properties getProperties()
    {
        return properties;
    }

    @Override
    public Parent getView()
    {
        return null;
    }

    @Override
    public boolean execute()
    {
        return false;
    }

    @Override
    public String getId()
    {
        return "variable";
    }

    @Override
    public URL getIcon()
    {
        return resourceProvider.getImage("components/variable.png");
    }

    @Override
    public String getLabel()
    {
        return "Variable";
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    public String toString()
    {
        return getLabel();
    }
}

package com.romanccoder.axecutor.core.component;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.base.Core;
import com.romanccoder.axecutor.core.util.ResourceProvider;
import com.romanccoder.axecutor.core.util.TranslationProvider;
import javafx.scene.Parent;
import javafx.scene.paint.Color;

import java.net.URL;

/**
 * Created by Roman on 07.07.2016.
 */
public class StartComponent implements Component
{
    @Inject
    @Core
    private TranslationProvider translationProvider;

    @Inject
    @Core
    private ResourceProvider resourceProvider;

    private StartProperties properties;

    public StartComponent()
    {
        properties = new StartProperties();
    }

    @Override
    public ComponentType getType()
    {
        return ComponentType.CORE;
    }

    @Override
    public ComponentCategory getCategory()
    {
        return ComponentCategory.BASE;
    }

    @Override
    public Color getColor()
    {
        return Color.web("#75afd3");
    }

    @Override
    public String getLabel()
    {
        return translationProvider.get("com.romanccoder.axecutor.core.component.StartComponent.label");
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    public URL getIcon()
    {
        return null;
    }

    @Override
    public String getId()
    {
        return "start";
    }

    @Override
    public Properties getProperties()
    {
        return properties;
    }

    @Override
    public Parent getView()
    {
        return null;
    }

    @Override
    public boolean execute()
    {
        return true;
    }
}

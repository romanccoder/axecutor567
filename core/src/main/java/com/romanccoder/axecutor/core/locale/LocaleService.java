package com.romanccoder.axecutor.core.locale;

import java.util.List;

/**
 * Created by Roman on 06.07.2016.
 */
public interface LocaleService
{
    List<LocaleWrapper> getAll();
    LocaleWrapper getById(String id);
    boolean exists(String id);
}

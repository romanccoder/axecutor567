package com.romanccoder.axecutor.core.component;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Roman on 10.07.2016.
 */
public class ComponentDescriptorImpl implements ComponentDescriptor
{
    private Component source;
    private Component greenTarget;
    private Component redTarget;

    private List<DescriptorChangedListener> listenerList;

    @Inject
    public ComponentDescriptorImpl(@Assisted Component source)
    {
        this.source = source;
        listenerList = new CopyOnWriteArrayList<>();
    }

    @Override
    public Component getSource()
    {
        return source;
    }

    @Override
    public void setGreenTarget(Component target)
    {
        Component previousTarget = greenTarget;
        greenTarget = target;
        raiseChanged(ConnectionType.GREEN, previousTarget);
    }

    @Override
    public Component getGreenTarget()
    {
        return greenTarget;
    }

    @Override
    public void setRedTarget(Component target)
    {
        Component previousTarget = redTarget;
        redTarget = target;
        raiseChanged(ConnectionType.RED, previousTarget);
    }

    @Override
    public Component getRedTarget()
    {
        return redTarget;
    }

    @Override
    public boolean hasGreenTarget()
    {
        return getGreenTarget() != null;
    }

    @Override
    public boolean hasRedTarget()
    {
        return getRedTarget() != null;
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == null || !(other instanceof ComponentDescriptor))
            return false;

        return getSource() == ((ComponentDescriptor) other).getSource();
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getSource());
    }

    private void raiseChanged(ConnectionType type, Component previousTarget)
    {
        for (DescriptorChangedListener listener : listenerList)
            listener.onDescriptorChanged(this, type, previousTarget);
    }

    @Override
    public void addListener(DescriptorChangedListener listener)
    {
        listenerList.add(listener);
    }

    @Override
    public void removeListener(DescriptorChangedListener listener)
    {
        listenerList.remove(listener);
    }

}

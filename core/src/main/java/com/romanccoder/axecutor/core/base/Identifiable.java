package com.romanccoder.axecutor.core.base;

/**
 * Created by Roman on 06.07.2016.
 */
public interface Identifiable<T>
{
    T getId();
}

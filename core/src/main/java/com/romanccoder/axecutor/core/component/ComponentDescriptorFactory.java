package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 16.07.2016.
 */
public interface ComponentDescriptorFactory
{
    ComponentDescriptor create(Component source);
}

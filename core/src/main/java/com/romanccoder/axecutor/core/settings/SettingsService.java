package com.romanccoder.axecutor.core.settings;

import java.io.File;

public interface SettingsService
{
    String FILE_NAME = ".settings";
    String LOCALE_KEY = "language";
    String COMMAND_TIMEOUT_KEY = "command_execution_timeout";
    String FTP_OPERATION_TIMEOUT_KEY = "ftp_operation_timeout";
    String CLEAR_LOG_KEY = "clear_log";
    String TASK_MAX_RECORDS_KEY = "task_max_records";

    void setModel(Settings model);
    Settings getModel();

    boolean refresh() throws SettingsException;
    void save() throws SettingsException;
    boolean validate();

    File getFile();
}
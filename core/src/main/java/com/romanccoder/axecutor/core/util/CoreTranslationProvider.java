package com.romanccoder.axecutor.core.util;

import com.google.inject.Inject;
import com.romanccoder.axecutor.core.settings.SettingsService;

import java.util.ResourceBundle;

/**
 * Created by Roman on 06.07.2016.
 */
public class CoreTranslationProvider implements TranslationProvider
{
    private ResourceBundle bundle;

    @Inject
    public CoreTranslationProvider(SettingsService service)
    {
        bundle = ResourceBundle.getBundle("com.romanccoder.axecutor.core.bundles.Messages", service.getModel().getActiveLocale().getLocale());
    }

    @Override
    public ResourceBundle getBundle()
    {
        return bundle;
    }
}

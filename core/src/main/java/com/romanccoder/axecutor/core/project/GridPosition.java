package com.romanccoder.axecutor.core.project;

import java.util.Objects;
import java.util.Observable;

/**
 * Created by Roman on 31.07.2016.
 */
public class GridPosition extends Observable
{
    private int row;
    private int column;

    public GridPosition(int row, int column)
    {
        this.row = row;
        this.column = column;
    }

    public GridPosition()
    {}

    public int getRow()
    {
        return row;
    }

    public void setRow(int row)
    {
        this.row = row;
        setChanged();
        notifyObservers();
    }

    public int getColumn()
    {
        return column;
    }

    public void setColumn(int column)
    {
        this.column = column;
        setChanged();
        notifyObservers();
    }

    public void copy(GridPosition position)
    {
        setRow(position.getRow());
        setColumn(position.getColumn());
    }

    @Override
    public boolean equals(Object other)
    {
        if (!(other instanceof GridPosition))
            return false;

        if (getRow() == ((GridPosition) other).getRow() && getColumn() == ((GridPosition) other).getColumn())
            return true;
        else
            return false;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getRow(), getColumn());
    }

    @Override
    public String toString()
    {
        return "row=" + getRow() + ", column=" + getColumn();
    }
}

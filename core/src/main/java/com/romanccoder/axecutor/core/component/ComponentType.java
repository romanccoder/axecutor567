package com.romanccoder.axecutor.core.component;

/**
 * Created by Roman on 06.07.2016.
 */
public enum ComponentType
{
    CORE,
    STANDALONE
}
